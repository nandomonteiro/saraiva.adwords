﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Services.Interfaces
{
    public interface IFilaService
    {
        bool InserirFilaSQS(string mensagem);
        List<String> ProcessarFilaSQS();
    }
}
