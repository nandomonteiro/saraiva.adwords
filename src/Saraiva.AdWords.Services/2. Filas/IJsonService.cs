﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Services.Interfaces
{
    public interface IJsonService
    {
        string Serialize(object obj);
        T Deserialize<T>(string strObjeto);
    }
}
