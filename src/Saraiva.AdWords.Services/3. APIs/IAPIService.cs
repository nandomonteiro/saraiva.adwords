﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Services.Interfaces
{
    public interface IAPIService
    {
        Task<T> ExecutaAcao<T>(string action, object request);
        Task<T> ExecutaAcao<T>(string action);
        Task<T> ExecutaAcao<T>(string action, string messenge);
    }
}
