﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Services.Mensagens.Sku
{
    public class InsertRequest
    {
        public string OfferId { get; set; }
        public long AdGroupId { get; set; }
        public long ParentId { get; set; }
        public long CpcBid { get; set; }
    }
}
