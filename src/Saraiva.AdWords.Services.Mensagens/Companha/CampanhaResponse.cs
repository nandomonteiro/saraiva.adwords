﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Services.Mensagens.Campanha
{
    public class CampanhaResponse
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public long Orcamento { get; set; }
        public string DataInicio { get; set; }
        public string DataFim { get; set; }
        public string Status { get; set; }
    }
}
