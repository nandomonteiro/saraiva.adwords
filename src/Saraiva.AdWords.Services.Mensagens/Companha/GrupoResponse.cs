﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Services.Mensagens.Campanha
{
    public class GrupoResponse
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public long CampanhaId { get; set; }
        public long CpcBid { get; set; }
        public string Status { get; set; }
    }
}
