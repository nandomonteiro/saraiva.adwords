﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Services.Mensagens.Performance
{
    public class PerformanceResponse
    {
        public string OfferId { get; set; }
        public string Date { get; set; }
        public long CampaignId { get; set; }
        public long AdGroupId { get; set; }
        public long Clicks { get; set; }
        public long Impressions { get; set; }
        public decimal Cost { get; set; }
        public double Conversions { get; set; }
        public decimal CostPerConversion { get; set; }
        public double ValuePerConversion { get; set; }
        public double ValuePerAllConversion { get; set; }
    }
}
