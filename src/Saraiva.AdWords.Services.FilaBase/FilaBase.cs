﻿using Saraiva.AdWords.Services.Filas;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Services.FilaBase
{
    public abstract class FilaBase : IFilaService
    {
        public string AccessKeyId { get; set; }
        public string SecretAccessKey { get; set; }
        public string Proxy
        {
            get
            {
                return "http://" + ProxyHost + ":" + ProxyPort;
            }
        }
        public string ProxyHost { get; set; }
        public int ProxyPort { get; set; }
        public string SQSHost { get; set; }

        public string Region { get; set; }

        public int MaxNumberOfMessages { get; set; }

        public abstract Task<bool> InserirFilaSQS<T>(T obj);

    }
}
