﻿using Saraiva.AdWords.Services.FilaService;
using Saraiva.AdWords.Services.Interfaces;

namespace Saraiva.AdWords.DI.Factory
{
    public class AdWordsFactory
    {
        public static IFilaService CriarFila()
        {
            return new FilaService();
        }

    }
}
