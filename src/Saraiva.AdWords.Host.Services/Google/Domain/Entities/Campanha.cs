﻿namespace Saraiva.AdWords.Host.Domain
{
    public class Campanha : ICampanhaGoogle
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public long Orcamento { get; set; }
        public string DataInicio { get; set; }
        public string DataFim { get; set; }
        public string Status { get; set; }
    }
}
