﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Domain
{
    public interface IClearingResponse : IBaseResponse
    {
        string Acao { get; set; }
        IEnumerable<IResponseSku> Skus { get; set; }
        void AddSkus(IEnumerable<IProdutoGoogle> produtos);
    }
}
