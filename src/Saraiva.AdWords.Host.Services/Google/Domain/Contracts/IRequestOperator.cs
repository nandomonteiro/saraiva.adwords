﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Domain
{
    public interface IRequestOperator
    {
        string Acao { get; set; }
        bool FlExecucao { get; set; }
        string MsgExecucao { get; set; }
        void AddSkus(IEnumerable<IProdutoGoogle> produtos);
        IEnumerable<IRequestSku> Skus { get; set; }
    }
}
