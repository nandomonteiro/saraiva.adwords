﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{
    public interface IGoogleResponse : IBaseResponse
    {
        long GrupoId { get; set; }
        IDictionary<string, IProdutoGoogle> Produtos { get; set; }
        IDictionary<long, IProdutoGoogle> Categorias { get; set;  }
        IDictionary<long, IProdutoGoogle> Simulacoes { get; }
        void AddSimulacao(long googleId, long cpc, long custo, long conversoes, long clicks);
    }

}
