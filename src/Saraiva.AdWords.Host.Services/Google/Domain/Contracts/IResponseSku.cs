﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Domain
{
    public interface IResponseSku
    {
        string Sku { get; set; }
        long GoogleId { get; set; }
        long Cpc { get; set; }
    }
}
