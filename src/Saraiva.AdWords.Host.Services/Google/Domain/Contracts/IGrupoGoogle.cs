﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Domain
{
    public interface IGrupoGoogle
    {
        long Id { get; set; }
        string Nome { get; set; }
        long CampanhaId { get; set; }
        long CpcBid { get; set; }
        string Status { get; set; }
    }
}
