﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{
    public interface IDimensaoResponse
    {
        long Id { get; set; }
        string Descricao { get; set; }
        long ParentId { get; set; }
    }

}
