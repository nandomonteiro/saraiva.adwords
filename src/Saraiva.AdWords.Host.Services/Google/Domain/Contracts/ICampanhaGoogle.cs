﻿namespace Saraiva.AdWords.Host.Domain
{
    public interface ICampanhaGoogle
    {
        long Id { get; set; }
        string Nome { get; set; }
        long Orcamento { get; set; }
        string DataInicio { get; set; }
        string DataFim { get; set; }
        string Status { get; set; }
    }
}
