﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Domain
{
    public interface IRequestSku
    {
        string Sku { get; set; }
        long Id { get; set; }
        long Cpc { get; set; }
        string Cat { get; set; }
    }
}
