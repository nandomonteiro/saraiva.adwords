﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Domain
{
    public interface IProdutoGoogle
    {
        long GoogleId { get; set; }
        long GrupoId { get; set; }
        string Sku { get; set; }
        long Cpc { get; set; }
        long NewCpc { get; set; }
        decimal Cpa { get; set; }
        decimal Custo { get; set; }
        double Conversoes { get; set; }
        bool IsCategoria { get; set; }
        bool IsMarca { get; set; }
        long Clicks { get; set; }
        string Type { get; set; }
        long ParentGoogleId { get; set; }
        string Categoria { get; set; }
        long Dimensao { get; set; }
        bool IsExcluded { get; set; }
        string Status { get; set; }
    }
}
