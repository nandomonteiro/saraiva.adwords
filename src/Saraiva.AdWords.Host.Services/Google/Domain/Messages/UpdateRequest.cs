﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Saraiva.AdWords.Host.Domain
{
    public class UpdateRequest : ICrudRequest
    {
        public long GrupoId { get; set; }
        public IEnumerable<RequestSku> Skus { get; set; }
    }

}
