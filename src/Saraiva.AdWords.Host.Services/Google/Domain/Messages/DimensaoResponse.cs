﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Domain
{
    public class DimensaoResponse : IDimensaoResponse
    {
        public long Id { get; set; }
        public string Descricao { get; set; }
        public long ParentId { get; set; }
    }
}
