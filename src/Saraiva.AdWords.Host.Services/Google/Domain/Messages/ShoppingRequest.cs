﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Saraiva.AdWords.Host.Domain
{
    public class ShoppingRequest
    {
        public string Time { get; set; }
        public string Interval { get; set; }
        public int[] skus { get; set; }
    }

}
