﻿namespace Saraiva.AdWords.Host.Domain
{
    public class RequestSku : IRequestSku
    {
        public long Id { get; set; }
        public string Sku { get; set; }
        public long Cpc { get; set; }
        public string Cat { get; set; }

    }
}
