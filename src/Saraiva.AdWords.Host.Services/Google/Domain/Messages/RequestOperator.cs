﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{
    public class RequestOperator : IRequestOperator
    {

        public string Acao { get; set; }
        public bool FlExecucao { get; set; }
        public string MsgExecucao { get; set; }
        public IEnumerable<IRequestSku> Skus { get; set; }

        public void AddSkus(IEnumerable<IProdutoGoogle> produtos)
        {
            if (produtos == null)
                return;

            var skus = new List<IRequestSku>();

            foreach (var item in produtos)
                skus.Add(new RequestSku() { Id = item.GoogleId, Sku = item.Sku, Cpc = item.Cpc });

            this.Skus = skus;
        }


    }
}