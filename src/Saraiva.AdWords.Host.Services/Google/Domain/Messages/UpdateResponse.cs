﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{
    public class UpdateResponse : IBaseResponse
    {
        public bool FlErro { get; set; }
        public string MsgErro { get; set; }

    }
}