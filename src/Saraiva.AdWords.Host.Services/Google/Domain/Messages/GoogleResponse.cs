﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{
    public class GoogleResponse : IGoogleResponse
    {
        public long GrupoId { get; set; }
        public bool FlErro { get; set; }
        public string MsgErro { get; set; }
        public IDictionary<string, IProdutoGoogle> Produtos { get; set; }
        public IDictionary<long, IProdutoGoogle> Categorias { get; set;  }
        public IDictionary<long, IProdutoGoogle> Simulacoes { get; }

        public GoogleResponse()
        {
            this.Produtos = new Dictionary<string, IProdutoGoogle>();
            this.Categorias = new Dictionary<long, IProdutoGoogle>();
            this.Simulacoes = new Dictionary<long, IProdutoGoogle>();
        }

        public void AddSimulacao(long googleId, long cpc, long custo, long conversoes, long clicks)
        {
            if (this.Simulacoes.ContainsKey(googleId) )
            {
                if (this.Simulacoes[googleId].Cpc < cpc)
                { 
                    this.Simulacoes[googleId].Cpc = cpc;
                    this.Simulacoes[googleId].Custo = custo;
                    this.Simulacoes[googleId].Conversoes = conversoes;
                    this.Simulacoes[googleId].Clicks = clicks;
                }
            }
            else
            {
                this.Simulacoes.Add(googleId, 
                    new Produto {   GoogleId = googleId,
                                    Cpc = cpc,
                                    Custo = custo,
                                    Conversoes = conversoes,
                                    Clicks = clicks }  );
            }


        }

    }
}
