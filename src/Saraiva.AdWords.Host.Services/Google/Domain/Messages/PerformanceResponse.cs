﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{
    public class PerformanceResponse
    {
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public long AdGroupId { get; set; }
        public string AdGroupName { get; set; }
        public string OfferId { get; set; }
        public long Id { get; set; }
        public long Cpc { get; set; }
        public List<EstatisticaResponse> Estatisticas { get; set; }
        public List<SimulacaoResponse> Simulacoes { get; set; }
    }

    public class EstatisticaResponse
    {
        public string Date { get; set; }
        public long Clicks { get; set; }
        public long Impressions { get; set; }
        public decimal Cost { get; set; }
        public double Conversions { get; set; }
        public double ConversionValue { get; set; }
        public double AllConversionValue { get; set; }
        public decimal CostPerConversion { get; set; }
        public double ValuePerConversion { get; set; }
        public double ValuePerAllConversion { get; set; }
    }

}
