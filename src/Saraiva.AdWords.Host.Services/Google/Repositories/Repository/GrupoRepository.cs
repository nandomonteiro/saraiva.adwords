﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201705;
using Saraiva.AdWords.Host.Domain;
using System;
using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Repositories
{
    public class GrupoRepository : BaseRepository, IGrupoRepository
    {
        #region Buscar grupos de campanha
        /// <summary>
        /// Buscar todos os Grupos de Campanha de todas as Campanhas cadastradas.
        /// </summary>
        /// <param name="Id">Identificador google opcional para o grupo.</param>
        public IEnumerable<T> BuscarGruposAd<T>() where T : IGrupoGoogle, new()
        {
            return BuscarGruposAd<T>(new Predicate[] { Predicate.Equals(AdGroup.Fields.AdGroupType, "SHOPPING_PRODUCT_ADS" )
            });
        }

        /// <summary>
        /// Buscar Grupo de Campanha.
        /// </summary>
        /// <param name="id">Identificador google do grupo.</param>
        public IEnumerable<T> BuscarGruposAd<T>(long id) where T : IGrupoGoogle, new()
        {
            return BuscarGruposAd<T>(new Predicate[] { Predicate.Equals(AdGroupCriterion.Fields.AdGroupId, id) });
        }

        /// <summary>
        /// Buscar os Grupos de Campanha por campanha informada.
        /// </summary>
        /// <param name="CampanhaId">Identificador google da campanha.</param>
        public IEnumerable<T> BuscarGruposPorCampanha<T>(long CampanhaId) where T : IGrupoGoogle, new()
        {
            return BuscarGruposAd<T>(new Predicate[] { Predicate.Equals(AdGroup.Fields.CampaignId, CampanhaId),
                Predicate.Equals(AdGroup.Fields.AdGroupType.ToString(), AdGroupType.SHOPPING_PRODUCT_ADS.ToString())
            });
        }
        
        /// <summary>
        /// Buscar informações de critérios do tipo partição de um produto definidos por um predicado de busca.
        /// </summary>
        /// <param name="predicates">Critérios google a serem encapsulados no seletor.</param>
        private IEnumerable<T> BuscarGruposAd<T>(Predicate[] predicates) where T : IGrupoGoogle, new()
        {
            AdGroupService adGroupService = (AdGroupService)User.GetService(AdWordsService.v201705.AdGroupService);

            Selector selector = new Selector()
            {
                fields = new string[] {
                    AdGroup.Fields.Id,
                    AdGroup.Fields.Name,
                    AdGroup.Fields.CampaignId,
                    AdGroup.Fields.CampaignName,
                    AdGroup.Fields.AdGroupType,
                    AdGroup.Fields.Status,
                    CpcBid.Fields.CpcBid
            },
                paging = Paging.Default
            };

            selector.predicates = predicates;

            AdGroupPage page = new AdGroupPage();

            var result = new List<T>();

            try
            {
                do
                {
                    page = adGroupService.get(selector);

                    if (page != null && page.entries != null)
                    {
                        foreach (AdGroup adGroup in page.entries)
                        {
                            var item = new T
                            {
                                Id = adGroup.id,
                                Nome = adGroup.name,
                                CampanhaId = adGroup.campaignId,
                                Status = adGroup.status.ToString(),
                                CpcBid = 0
                            };

                            if (adGroup.biddingStrategyConfiguration.bids != null)
                            {
                                foreach (var bid in adGroup.biddingStrategyConfiguration.bids)
                                {
                                    if (bid.BidsType == "CpcBid" && (bid as CpcBid).bid != null)
                                        item.CpcBid += (bid as CpcBid).bid.microAmount;
                                }
                            }

                            result.Add(item);

                        }
                    }
                    selector.paging.IncreaseOffset();
                } while (selector.paging.startIndex < page.totalNumEntries);
            }
            catch (Exception e)
            {
                throw new Exception("Falha ao receber grupos", e);
            }

            return result;
        }
        #endregion
    }

}
