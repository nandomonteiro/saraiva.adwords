﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.Util.Reports.v201705;
using Google.Api.Ads.AdWords.v201705;
using Google.Api.Ads.AdWords.Util.Shopping.v201705;
using System.Linq;
using Google.Api.Ads.Common.Util.Reports;
using Saraiva.AdWords.Host.Domain;
using System;
using System.Collections.Generic;
using System.IO.Compression;

namespace Saraiva.AdWords.Host.Repositories
{
    public class RelatorioRepository : BaseRepository
    {
        private const string cte_versao = "v201705";

        public struct OfferGrupo
        {
            public long AdGrupo { set; get; }
            public string OfferId { set; get; }
        }

        #region Obter Estatistica Por Grupo/Produto
        /// <summary>
        /// Relatório responsável em buscar estatísticas de cliques, impressões, custo, conversões e cpc
        /// em um grupo de campanha com seus respectivos custos por clique
        /// </summary>
        /// <param name="adGroupId">Identificador google para grupo.</param>
        public Dictionary<long, T> RelProdutosPorGrupo<T>(long adGrupoId) where T : IProdutoGoogle, new()
        {
            return RelProdutosParticionados<T>(String.Format(@"AdGroupId = {0}", adGrupoId));
        }
        /// <summary>
        /// Relatório responsável em buscar estatísticas de cliques, impressões, custo, conversões e cpc
        /// em um grupo de campanha com seus respectivos custos por clique
        /// </summary>
        /// <param name="adGroupId">Identificador google para grupo.</param>
        /// <param name="id">Identificador google para o produto particionado.</param>
        public Dictionary<long, T> RelProdutosPorGrupoProduto<T>(long adGrupoId, long id) where T : IProdutoGoogle, new()
        {
            return RelProdutosParticionados<T>(String.Format(@"AdGroupId = {0} and Id = {1}", adGrupoId, id));
        }
        /// <summary>
        /// Relatório responsável em buscar estatísticas de cliques, impressões, custo, conversões e cpc
        /// em um grupo de campanha com seus respectivos custos por clique
        /// </summary>
        /// <param name="criterios">Condição "Where" a ser concatenada com o comando em AWQL responsável pelo relatório</param>
        private Dictionary<long, T> RelProdutosParticionados<T>(string criterios) where T : IProdutoGoogle, new()
        {
            var sql = String.Format(@"SELECT Id, AdGroupId, Clicks, Impressions, Cost, Conversions, CostPerConversion
                                     FROM   PRODUCT_PARTITION_REPORT
                                     WHERE  {0}", criterios);

            var result = new Dictionary<long, T>();

            (User.Config as AdWordsAppConfig).UseRawEnumValues = true;

            ReportUtilities reportUtilities = new ReportUtilities(User, cte_versao, sql, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (ReportResponse response = reportUtilities.GetResponse())
                {
                    using (GZipStream gzipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var report = new AwReport<ProductPartitionReportReportRow>(new AwXmlTextReader(gzipStream), "RelProdutosParticionados"))
                        {
                            while (report.MoveNext())
                            {
                                result.Add(report.Current.id , new T()
                                {
                                    GoogleId = report.Current.id,
                                    GrupoId = report.Current.adGroupId,
                                    Clicks = report.Current.clicks,
                                    Custo = report.Current.cost,
                                    Conversoes = report.Current.conversions,
                                    Cpa = report.Current.costPerConversion
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Falha ao buscar produtos. Criterios: {0} - {1}. Erro: {2}", cte_versao, sql, e.Message));
            }

            return result;
        }

        private ProductPartitionNode FindNoteByOffer(IEnumerable<ProductPartitionNode> nodes, string value)
        {
            ProductPartitionNode result = null;
            foreach (var node in nodes)
            {
                if (node.Dimension.ProductDimensionType == "ProductOfferId")
                    if ((node.Dimension as ProductOfferId).value == Convert.ToString(value))
                        return node;

                if (node.Children.Count() > 0)
                    result = FindNoteByOffer(node.Children, value);

                if (result != null)
                    return result;
            }

            return null;
        }

        public List<PerformanceResponse> CarregarPerformances(string[] offersId, string time, string intervalo)
        {
            List<PerformanceResponse> performances = new List<PerformanceResponse>();

            var offersPerformance = RelShoppingPerformance(offersId, time, intervalo);

            var productPartitionNodes = new Dictionary<long, IEnumerable<ProductPartitionNode>>();

            foreach (var performance in offersPerformance.Values)
            {
                var adGroupId = performance.AdGroupId;
                var offerId = performance.OfferId;

                if (!productPartitionNodes.ContainsKey(adGroupId))
                    productPartitionNodes.Add(adGroupId, ProductPartitionTree.DownloadAdGroupTree(User, adGroupId).Root.Children);

                var partitionOffer = FindNoteByOffer(productPartitionNodes[adGroupId], offerId);

                if (partitionOffer != null)
                {
                    performance.Id = partitionOffer.ProductPartitionId;
                    performance.Cpc = partitionOffer.CpcBid;

                    performance.Simulacoes = BuscarSimulacoes(adGroupId, performance.Id);
                }

                performances.Add(performance);
            }

            return performances;
        }

        public List<ShoppingResponse> RelShopping(long adGroupId, string intervalo)
        {
            var sql = String.Format(@"SELECT OfferId,
                                            Date,    
                                            CampaignId,
                                            AdGroupId,
                                            Clicks,
                                            Impressions,    
                                            Cost,
                                            Conversions,
                                            CostPerConversion,
                                            ValuePerConversion,
                                            ValuePerAllConversion
                                     FROM   SHOPPING_PERFORMANCE_REPORT
                                     WHERE  AdGroupId = {0} DURING {1}", adGroupId, intervalo);

            var result = new List<ShoppingResponse>();

            (User.Config as AdWordsAppConfig).UseRawEnumValues = true;

            ReportUtilities reportUtilities = new ReportUtilities(User, cte_versao, sql, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (ReportResponse response = reportUtilities.GetResponse())
                {
                    using (GZipStream gzipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var report = new AwReport<ShoppingPerformanceReportReportRow>(new AwXmlTextReader(gzipStream), "RelShoppingPerformanceReport"))
                        {
                            while (report.MoveNext())
                            {
                                result.Add(new ShoppingResponse()
                                {
                                    OfferId = report.Current.offerId,
                                    Date = report.Current.date,
                                    CampaignId = report.Current.campaignId,
                                    AdGroupId = report.Current.adGroupId,
                                    Clicks = report.Current.clicks, 
                                    Impressions = report.Current.impressions,
                                    Cost = report.Current.cost,
                                    Conversions = report.Current.conversions,
                                    CostPerConversion = report.Current.costPerConversion,
                                    ValuePerConversion = report.Current.valuePerConversion,
                                    ValuePerAllConversion = report.Current.valuePerAllConversion
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Falha ao buscar produtos. Criterios: {0} - {1}. Erro: {2}", cte_versao, sql, e.Message));
            }

            return result;
        }

        public Dictionary<OfferGrupo, PerformanceResponse> RelShoppingPerformance(string[] offersId, string time, string intervalo)
        {
            var sql = String.Format(@"SELECT OfferId,
                                             CampaignId,
                                             {2}
                                             CampaignName,   
                                             AdGroupId,
                                             AdGroupName,
                                             Clicks,
                                             Impressions,    
                                             Cost,
                                             Conversions,
                                             ConversionValue,                
                                             AllConversionValue,                
                                             CostPerConversion,
                                             ValuePerConversion,
                                             ValuePerAllConversion
                                     FROM    SHOPPING_PERFORMANCE_REPORT
                                     WHERE   CampaignStatus = {0} AND OfferId IN [{1}]{3}", 
                                     CampaignStatus.ENABLED, 
                                     String.Join(",", offersId),
                                     time == null ? String.Empty : time + ",",
                                     intervalo == null ? String.Empty : " DURING " + intervalo
                                     );

            var result = new Dictionary<OfferGrupo, PerformanceResponse>();

            (User.Config as AdWordsAppConfig).UseRawEnumValues = true;

            ReportUtilities reportUtilities = new ReportUtilities(User, cte_versao, sql, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (ReportResponse response = reportUtilities.GetResponse())
                {
                    using (GZipStream gzipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var report = new AwReport<ShoppingPerformanceReportReportRow>(new AwXmlTextReader(gzipStream), "RelShoppingPerformance"))
                        {
                            while (report.MoveNext())
                            {
                                var estatistica = new EstatisticaResponse()
                                {
                                    Date = report.Current.date,
                                    Clicks = report.Current.clicks,
                                    Impressions = report.Current.impressions,
                                    Cost = report.Current.cost,
                                    Conversions = report.Current.conversions,
                                    CostPerConversion = report.Current.costPerConversion,
                                    ValuePerConversion = report.Current.valuePerConversion,
                                    ValuePerAllConversion = report.Current.valuePerAllConversion,
                                    AllConversionValue = report.Current.allConversionValue,
                                    ConversionValue = report.Current.conversionValue
                                };

                                long adGroupId = report.Current.adGroupId;
                                string adGroupName = report.Current.adGroupName;
                                long campaignId = report.Current.campaignId;
                                string campaignName = report.Current.campaignName;
                                string offerId = report.Current.offerId;

                                var chave = new OfferGrupo() { AdGrupo = adGroupId, OfferId = offerId };

                                if (!result.ContainsKey(chave))
                                { 
                                    result.Add(chave, new PerformanceResponse() {
                                        AdGroupId = adGroupId,
                                        CampaignId = campaignId,
                                        AdGroupName = adGroupName,
                                        CampaignName = campaignName,
                                        OfferId = offerId,
                                        Estatisticas = new List<EstatisticaResponse>()
                                    });
                                }

                                result[chave].Estatisticas.Add(estatistica);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Falha ao buscar produtos. Criterios: {0} - {1}. Erro: {2}", cte_versao, sql, e.Message));
            }

            return result;
        }

        public List<SimulacaoResponse> BuscarSimulacoes(long groupId, long googleId)
        {
            var result = new List<SimulacaoResponse>();

            DataService dataService = (DataService)User.GetService(AdWordsService.v201705.DataService);

            // Create selector.
            Selector selector = new Selector()
            {
                fields = new string[] {
                                            CriterionBidLandscape.Fields.CriterionId,
                                            CriterionBidLandscape.Fields.StartDate,
                                            CriterionBidLandscape.Fields.EndDate,
                                            CriterionBidLandscape.Fields.CampaignId,
                                            CriterionBidLandscape.Fields.AdGroupId,
                                            BidLandscapeLandscapePoint.Fields.Bid,
                                            BidLandscapeLandscapePoint.Fields.LocalClicks,
                                            BidLandscapeLandscapePoint.Fields.LocalCost,
                                            BidLandscapeLandscapePoint.Fields.LocalImpressions
            },
                predicates = new Predicate[] {
                    Predicate.Equals(CriterionBidLandscape.Fields.AdGroupId, groupId),
                    Predicate.Equals(CriterionBidLandscape.Fields.CriterionId, googleId)
            },
                ordering = new OrderBy[] { OrderBy.Asc(CriterionBidLandscape.Fields.CriterionId),
                                           OrderBy.Desc(BidLandscapeLandscapePoint.Fields.LocalImpressions)
                },
                paging = Paging.Default
            };

            int landscapePointsInLastResponse = 0;

            try
            {
                CriterionBidLandscapePage page = null;

                do
                {
                    page = dataService.getCriterionBidLandscape(selector);
                    landscapePointsInLastResponse = 0;

                    if (page != null && page.entries != null)
                    {
                        foreach (CriterionBidLandscape bidLandscape in page.entries)
                        {
                            foreach (BidLandscapeLandscapePoint point in bidLandscape.landscapePoints)
                            {
                                result.Add(new SimulacaoResponse()
                                {
                                    CriterionId = bidLandscape.criterionId,
                                    StartDate = bidLandscape.startDate,
                                    EndDate = bidLandscape.endDate,
                                    CampaignId = bidLandscape.campaignId,
                                    AdGroupId = bidLandscape.adGroupId,
                                    Bid = point.bid.microAmount,
                                    Clicks = point.clicks,
                                    Impressions = point.impressions,
                                    Cost = point.cost.microAmount
                                });

                                landscapePointsInLastResponse++;
                            }
                        }
                    }
                    selector.paging.IncreaseOffsetBy(landscapePointsInLastResponse);
                } while (landscapePointsInLastResponse > 0);
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Falha ao atualizar simulacoes. Erro: {0}.", e.Message));
            }

            return result;
        }

    }

    #endregion
}




