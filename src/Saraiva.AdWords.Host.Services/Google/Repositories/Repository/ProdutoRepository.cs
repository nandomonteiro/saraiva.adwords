﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201705;
using Google.Api.Ads.AdWords.Util.Shopping.v201705;
using Saraiva.AdWords.Host.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Saraiva.AdWords.Host.Repositories
{
    public class ProdutoRepository : BaseRepository
    {
        
        #region Buscar produtos particionados
        /// <summary>
        /// Buscar informações de critérios do tipo partição de todos os produto de um grupo.
        /// </summary>
        /// <param name="adGroupId">Identificador google para o grupo.</param>
        public void CarregarGrupoProduto<T>(IGoogleResponse response)
            where T : IProdutoGoogle, new()
        {
            BuscarProdutos<T>(ProductDimensionType.OFFER_ID, new Predicate[] {
                Predicate.Equals(AdGroupCriterion.Fields.AdGroupId, response.GrupoId),
                Predicate.Equals(ProductPartition.Fields.CriteriaType, "PRODUCT_PARTITION")
            }, response);
        }

        public void CarregarGrupoProdutoPorId<T>(long id, IGoogleResponse response)
        where T : IProdutoGoogle, new()
        {
            BuscarProdutos<T>(ProductDimensionType.OFFER_ID, new Predicate[] {
                Predicate.Equals(AdGroupCriterion.Fields.AdGroupId, response.GrupoId),
                Predicate.Equals(ProductPartition.Fields.Id, id),
                Predicate.Equals(ProductPartition.Fields.CriteriaType, "PRODUCT_PARTITION")
            }, response);
        }

        public void CarregarProdutoPorSku<T>(string sku, IGoogleResponse response)
        where T : IProdutoGoogle, new()
        {
            BuscarProdutosSku<T>(sku, new Predicate[] {
                Predicate.Equals(ProductPartition.Fields.CriteriaType, "PRODUCT_PARTITION"),
                Predicate.Equals(AdGroupCriterion.Fields.AdGroupId, response.GrupoId),
            }, response);
        }
        // Predicate.Equals(ProductPartition.Fields.PartitionType, ProductPartitionType.UNIT)
        public void CarregarMarcas<T>(IGoogleResponse response)
            where T : IProdutoGoogle, new()
        {
            BuscarProdutos<T>(ProductDimensionType.BRAND, new Predicate[] {
                Predicate.Equals(AdGroupCriterion.Fields.AdGroupId, response.GrupoId),
                Predicate.Equals(ProductPartition.Fields.CriteriaType, "PRODUCT_PARTITION")
            }, response);
        }

        public void CarregarCategorias<T>(IGoogleResponse response)
            where T : IProdutoGoogle, new()
        {
            BuscarProdutos<T>(ProductDimensionType.PRODUCT_TYPE_L1, new Predicate[] {
                Predicate.Equals(AdGroupCriterion.Fields.AdGroupId, response.GrupoId),
                Predicate.Equals(ProductPartition.Fields.CriteriaType, "PRODUCT_PARTITION")
            }, response);
        }

        public void CarregarMarcaCategorias<T>(IGoogleResponse response)
            where T : IProdutoGoogle, new()
        {
            BuscarProdutos<T>(ProductDimensionType.UNKNOWN, new Predicate[] {
                Predicate.Equals(AdGroupCriterion.Fields.AdGroupId, response.GrupoId),
                Predicate.Equals(ProductPartition.Fields.CriteriaType, "PRODUCT_PARTITION")}, response);
        }
 
        /// <summary>
        /// Buscar informações de critérios do tipo partição de um produto definidos por um predicado de busca.
        /// </summary>
        /// <param name="predicates">Critérios google a serem encapsulados no seletor.</param>
        private void BuscarProdutos<T>(ProductDimensionType? dimensao, Predicate[] predicates, IGoogleResponse response)
            where T : IProdutoGoogle, new()
        {
            #region Parametrizacao Basica

            AdGroupCriterionService adGroupCriterionService = (AdGroupCriterionService)User.GetService(AdWordsService.v201705.AdGroupCriterionService);

            Selector selector = new Selector()
            {
                fields = new string[] {
                ProductPartition.Fields.Id, ProductPartition.Fields.CaseValue,
                AdGroupCriterion.Fields.CriterionUse,
                ProductPartition.Fields.CriteriaType,
                BiddingStrategyConfiguration.Fields.BiddingStrategyId,
                ProductPartition.Fields.ParentCriterionId,
                CpcBid.Fields.CpcBid
            },
            };

            selector.predicates = predicates;

            AdGroupCriterionPage page = new AdGroupCriterionPage();

            #endregion

            try
            {
                page = adGroupCriterionService.get(selector);

                // Display the results.
                if (page != null && page.entries != null)
                {
                    #region Laço
                    foreach (AdGroupCriterion adGroupCriterion in page.entries)
                    {
                        if (adGroupCriterion is BiddableAdGroupCriterion && adGroupCriterion.criterion is ProductPartition)
                        {
                            var biddingStrategyConfiguration = (adGroupCriterion as BiddableAdGroupCriterion).biddingStrategyConfiguration;

                            var productPartition = (adGroupCriterion.criterion as ProductPartition);

                            if (productPartition.caseValue != null)
                            {
                                #region Produto
                                if (productPartition.caseValue is ProductOfferId &&
                                    (productPartition.caseValue as ProductOfferId).value != null &&
                                    (dimensao == ProductDimensionType.OFFER_ID || dimensao == null)
                                    )
                                {
                                    var productOffer = (productPartition.caseValue as ProductOfferId);

                                    var produto = new T()
                                    {
                                        Sku = productOffer.value,
                                        GoogleId = productPartition.id,
                                        ParentGoogleId = productPartition.parentCriterionId,
                                        GrupoId = response.GrupoId
                                    };

                                    if (biddingStrategyConfiguration.bids != null)
                                    { 
                                        foreach (var bid in biddingStrategyConfiguration.bids)
                                        {
                                            if (bid.BidsType == "CpcBid" && (bid as CpcBid).bid != null)
                                                produto.Cpc += (bid as CpcBid).bid.microAmount;
                                        }
                                    }

                                    response.Produtos.Add(produto.Sku, produto);
                                }
                                #endregion
                                else
                                #region Marca
                                    if (productPartition.caseValue is ProductBrand &&
                                       (productPartition.caseValue as ProductBrand).value != null &&
                                       (dimensao == ProductDimensionType.BRAND || 
                                         dimensao == null || 
                                         dimensao == ProductDimensionType.UNKNOWN))
                                    {
                                        var productBrand = (productPartition.caseValue as ProductBrand);

                                        var marca = new T() {
                                            Categoria = productBrand.value,
                                            GoogleId = productPartition.id,
                                            ParentGoogleId = productPartition.parentCriterionId,
                                            GrupoId = response.GrupoId
                                        };

                                        if (biddingStrategyConfiguration.bids != null)
                                        {
                                            foreach (var bid in biddingStrategyConfiguration.bids)
                                            {
                                                if (bid.BidsType == "CpcBid" && (bid as CpcBid).bid != null)
                                                marca.Cpc += (bid as CpcBid).bid.microAmount;
                                            }
                                        }

                                        response.Categorias.Add(marca.GoogleId, marca);
                                    }
                                    #endregion
                                else
                                #region Tipo de Produto
                                        if (productPartition.caseValue is ProductType &&
                                           (productPartition.caseValue as ProductType).value != null &&
                                             (
                                                dimensao == null ||
                                                dimensao == ProductDimensionType.UNKNOWN ||
                                                dimensao == ProductDimensionType.PRODUCT_TYPE_L1
                                              )
                                           )
                                        {
                                            var productType = (productPartition.caseValue as ProductType);

                                            var categoria = new T()
                                            {
                                                Categoria = productType.value,
                                                GoogleId = productPartition.id,
                                                ParentGoogleId = productPartition.parentCriterionId,
                                                GrupoId = response.GrupoId
                                            };

                                            if (biddingStrategyConfiguration.bids != null)
                                            {
                                                foreach (var bid in biddingStrategyConfiguration.bids)
                                                {
                                                    if (bid.BidsType == "CpcBid" && (bid as CpcBid).bid != null)
                                                      categoria.Cpc += (bid as CpcBid).bid.microAmount;
                                                }
                                            }

                                            response.Categorias.Add(categoria.GoogleId, categoria);
                                        }
                                #endregion
                            }

                        }
                    }
                    #endregion
                }
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Falha ao buscar produtos. Criterios: {0}. Erro: {1}", predicates, e));
            }
        }
        #endregion

        public string ConfigurarCategorias(IConfigRequest request)
        {
            try
            { 
                ProductPartitionTree partitionTree = ProductPartitionTree.DownloadAdGroupTree(User, request.AdGroupId);

                var node = FindNoteById(partitionTree.Root.Children, request.GoogleId);

                if (node.IsExcludedUnit)
                {
                    node.AsBiddableUnit().CpcBid = 10000L;
                }

                if (!node.IsSubdivision)
                {
                    node.AsSubdivision();
                    node.AddChild(ProductDimensions.CreateOfferId(null)).AsExcludedUnit();
                }

                AdGroupCriterionOperation[] mutateOperations = partitionTree.GetMutateOperations();

                if (mutateOperations.Length == 0)
                    return "EMPTY";

                AdGroupCriterionService adGroupCriterionService = (AdGroupCriterionService)User.GetService(AdWordsService.v201705.AdGroupCriterionService);

                adGroupCriterionService.mutate(mutateOperations);

                return "OK";

            }
            catch (Exception e)
            {
                return e.Message;
            }

        }

        private ProductPartitionNode FindNoteById(IEnumerable<ProductPartitionNode> nodes, long id)
        {
            ProductPartitionNode result = null;

            foreach (var node in nodes)
            {
                if (node.ProductPartitionId == id)
                    return node;

                if (node.Children.Count() > 0)
                    result = FindNoteById(node.Children, id);

                if (result != null)
                    return result;
            }

            return null;
        }

        public IEnumerable<T> BuscarGrupoProdutos<T>(long grupoId) where T : IProdutoGoogle, new()
        {
            
            #region Parametrizacao Basica

            AdGroupCriterionService adGroupCriterionService = (AdGroupCriterionService)User.GetService(AdWordsService.v201705.AdGroupCriterionService);

            Selector selector = new Selector()
            {
                fields = new string[] {
                ProductPartition.Fields.Id, ProductPartition.Fields.CaseValue,
                AdGroupCriterion.Fields.CriterionUse,
                ProductPartition.Fields.CriteriaType,
                BiddingStrategyConfiguration.Fields.BiddingStrategyId,
                ProductPartition.Fields.ParentCriterionId,
                CpcBid.Fields.CpcBid
            },
            };

            selector.predicates = new Predicate[] { Predicate.Equals(AdGroupCriterion.Fields.AdGroupId, grupoId) };

            AdGroupCriterionPage page = new AdGroupCriterionPage();

            var result = new List<T>();

            #endregion

            try
            {
                int i = 0;
                page = adGroupCriterionService.get(selector);

                // Display the results.
                if (page != null && page.entries != null)
                {
                    #region Laço
                    foreach (AdGroupCriterion adGroupCriterion in page.entries)
                    {
                        //if (adGroupCriterion.criterion.id == 342436517402)
                          //  i++;

                        if (adGroupCriterion.criterion is ProductPartition)
                        {
                            var productPartition = (adGroupCriterion.criterion as ProductPartition);

                            var produto = new T()
                            {
                                GoogleId = productPartition.id,
                                ParentGoogleId = productPartition.parentCriterionId,
                                Type = productPartition.partitionType.ToString(),
                                GrupoId = adGroupCriterion.adGroupId,
                                IsCategoria = true,
                                IsMarca = false
                            };

                            if (productPartition.caseValue == null)
                                produto.Categoria = productPartition.CriterionType;
                            else
                                if (!CarregarValores(productPartition.caseValue, produto))
                                continue;

                            produto.IsExcluded = adGroupCriterion is NegativeAdGroupCriterion;

                            if (!produto.IsExcluded && adGroupCriterion is BiddableAdGroupCriterion)
                            { 
                                var biddingStrategyConfiguration = (adGroupCriterion as BiddableAdGroupCriterion).biddingStrategyConfiguration;

                                if (biddingStrategyConfiguration.bids != null)
                                {
                                    foreach (var bid in biddingStrategyConfiguration.bids)
                                    {
                                        if (bid.BidsType == "CpcBid" && (bid as CpcBid).bid != null)
                                            produto.Cpc += (bid as CpcBid).bid.microAmount;
                                    }
                                }
                            }

                            result.Add(produto);
                        }
                    }
                    #endregion
                }
                return result;
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Falha ao buscar produtos. GrupoId: {0}. Erro: {1}", grupoId, e));
            }
        }

        public T BuscarGrupoOfferId<T>(long grupoId, string offerId) where T : IProdutoGoogle, new()
        {
            T result = default(T);

            #region Parametrizacao Basica

            AdGroupCriterionService adGroupCriterionService = (AdGroupCriterionService)User.GetService(AdWordsService.v201705.AdGroupCriterionService);

            Selector selector = new Selector()
            {
                fields = new string[] {
                ProductPartition.Fields.Id, ProductPartition.Fields.CaseValue,
                AdGroupCriterion.Fields.CriterionUse,
                ProductPartition.Fields.CriteriaType,
                BiddingStrategyConfiguration.Fields.BiddingStrategyId,
                ProductPartition.Fields.ParentCriterionId,
                CpcBid.Fields.CpcBid
            },
            };

            selector.predicates = new Predicate[] { Predicate.Equals(AdGroupCriterion.Fields.AdGroupId, grupoId) };

            AdGroupCriterionPage page = new AdGroupCriterionPage();

            #endregion

            try
            {
                page = adGroupCriterionService.get(selector);

                // Display the results.
                if (page != null && page.entries != null)
                {
                    #region Laço
                    foreach (AdGroupCriterion adGroupCriterion in page.entries)
                    {
                        if (adGroupCriterion.criterion is ProductPartition && (adGroupCriterion.criterion as ProductPartition).caseValue != null )
                        {
                            var productPartition = (adGroupCriterion.criterion as ProductPartition);

                            result = new T()
                            {
                                GoogleId = productPartition.id,
                                ParentGoogleId = productPartition.parentCriterionId,
                                Type = productPartition.partitionType.ToString(),
                                GrupoId = adGroupCriterion.adGroupId,
                                IsCategoria = true,
                                IsMarca = false
                            };

                            if (!(productPartition.caseValue.ProductDimensionType == "ProductOfferId" && ((ProductOfferId)productPartition.caseValue).value == offerId))
                                continue;

                            result.IsExcluded = adGroupCriterion is NegativeAdGroupCriterion;

                            if (!result.IsExcluded && adGroupCriterion is BiddableAdGroupCriterion)
                            {
                                var biddingStrategyConfiguration = (adGroupCriterion as BiddableAdGroupCriterion).biddingStrategyConfiguration;

                                if (biddingStrategyConfiguration.bids != null)
                                {
                                    foreach (var bid in biddingStrategyConfiguration.bids)
                                    {
                                        if (bid.BidsType == "CpcBid" && (bid as CpcBid).bid != null)
                                            result.Cpc += (bid as CpcBid).bid.microAmount;
                                    }
                                }
                            }

                            return result;
                        }
                    }
                    #endregion
                }

                return result;
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Falha ao buscar produtos. GrupoId: {0}. Erro: {1}", grupoId, e));
            }
        }

        private bool CarregarValores(ProductDimension productDimension, IProdutoGoogle produto)
        {
            try
            { 
                switch (productDimension.ProductDimensionType)
                {
                    case "ProductOfferId":
                        return CarregarValores((ProductOfferId)productDimension, produto);
                    case "ProductAdwordsGrouping":
                        return CarregarValores((ProductAdwordsGrouping)productDimension, produto);
                    case "ProductBiddingCategory":
                        return CarregarValores((ProductBiddingCategory)productDimension, produto);
                    case "ProductTypeFull":
                        return CarregarValores((ProductTypeFull)productDimension, produto);
                    case "ProductLegacyCondition":
                        return CarregarValores((ProductLegacyCondition)productDimension, produto);
                    case "ProductCustomAttribute":
                        return CarregarValores((ProductCustomAttribute)productDimension, produto);
                    case "ProductType":
                        return CarregarValores((ProductType)productDimension, produto);
                    case "ProductBrand":
                        return CarregarValores((ProductBrand)productDimension, produto);
                    default:
                        produto.Categoria = productDimension.ProductDimensionType ?? "Todos os outros";
                        produto.IsCategoria = true;
                        produto.IsMarca = false;
                        return true;
                }
            }
            catch
            {
                return false;
            }

        }

        private bool CarregarValores(ProductOfferId productOffer, IProdutoGoogle produto)
        {
            produto.Sku = productOffer.value;
            produto.IsCategoria = false;
            produto.IsMarca = false;

            return (produto.Sku != null);
                
        }

        private bool CarregarValores(ProductAdwordsGrouping productAdwordsGrouping, IProdutoGoogle produto)
        {
            produto.Categoria = productAdwordsGrouping.value;
            produto.IsCategoria = true;
            produto.IsMarca = false;

            return (produto.Categoria != null);
        }
        private bool CarregarValores(ProductBiddingCategory productBiddingCategory, IProdutoGoogle produto)
        {
            produto.Categoria = productBiddingCategory.type.ToString();
            produto.Dimensao = productBiddingCategory.value;
            produto.IsCategoria = true;
            produto.IsMarca = false;

            return (produto.Categoria != null);
        }


        private bool CarregarValores(ProductTypeFull productTypeFull, IProdutoGoogle produto)
        {
            produto.Categoria = productTypeFull.value;
            produto.IsCategoria = true;
            produto.IsMarca = false;

            return (produto.Categoria != null);
        }
        private bool CarregarValores(ProductLegacyCondition productLegacyCondition, IProdutoGoogle produto)
        {
            produto.Categoria = productLegacyCondition.value;
            produto.IsCategoria = true;
            produto.IsMarca = false;

            return (produto.Categoria != null);
        }
        private bool CarregarValores(ProductCustomAttribute productCustomAttribute, IProdutoGoogle produto)
        {
            produto.Categoria = productCustomAttribute.value;
            produto.IsCategoria = true;
            produto.IsMarca = false;

            return (produto.Categoria != null);
        }
        private bool CarregarValores(ProductType productType, IProdutoGoogle produto)
        {
            produto.Categoria = productType.value;
            produto.IsCategoria = true;
            produto.IsMarca = false;

            return (produto.Categoria != null);
        }

        private bool CarregarValores(ProductBrand productBrand, IProdutoGoogle produto)
        {
            produto.Categoria = productBrand.value;
            produto.IsCategoria = true;
            produto.IsMarca = true;

            return (produto.Categoria != null);
        }

        /// <summary>
        /// Buscar informações de critérios do tipo partição de um produto definidos por um predicado de busca.
        /// </summary>
        /// <param name="predicates">Critérios google a serem encapsulados no seletor.</param>
        private void BuscarProdutosSku<T>(string sku, Predicate[] predicates, IGoogleResponse response)
            where T : IProdutoGoogle, new()
        {

            AdGroupCriterionService adGroupCriterionService = (AdGroupCriterionService)User.GetService(AdWordsService.v201705.AdGroupCriterionService);

            Selector selector = new Selector()
            {
                fields = new string[] {
                ProductPartition.Fields.Id, ProductPartition.Fields.CaseValue,
                AdGroupCriterion.Fields.CriterionUse,
                ProductPartition.Fields.CriteriaType,
                BiddingStrategyConfiguration.Fields.BiddingStrategyId,
                ProductPartition.Fields.ParentCriterionId,
                CpcBid.Fields.CpcBid
            },
                paging = Paging.Default,
            };

            selector.predicates = predicates;

            AdGroupCriterionPage page = new AdGroupCriterionPage();

            try
            {
                do
                {
                    page = adGroupCriterionService.get(selector);

                    // Display the results.
                    if (page != null && page.entries != null)
                    {
                        foreach (AdGroupCriterion adGroupCriterion in page.entries)
                        {
                            if (adGroupCriterion is BiddableAdGroupCriterion && adGroupCriterion.criterion is ProductPartition)
                            {
                                var biddingStrategyConfiguration = (adGroupCriterion as BiddableAdGroupCriterion).biddingStrategyConfiguration;

                                var productPartition = (adGroupCriterion.criterion as ProductPartition);

                                if (productPartition.caseValue != null)
                                {
                                    if (productPartition.caseValue is ProductOfferId &&
                                        (productPartition.caseValue as ProductOfferId).value == sku)
                                    {
                                        var productOffer = (productPartition.caseValue as ProductOfferId);

                                        var produto = new T()
                                        {
                                            Sku = productOffer.value,
                                            GoogleId = productPartition.id,
                                            ParentGoogleId = productPartition.parentCriterionId,
                                            GrupoId = response.GrupoId
                                        };

                                        if (biddingStrategyConfiguration.bids != null)
                                        {
                                            foreach (var bid in biddingStrategyConfiguration.bids)
                                            {
                                                if (bid.BidsType == "CpcBid" && (bid as CpcBid).bid != null)
                                                    produto.Cpc += (bid as CpcBid).bid.microAmount;
                                            }
                                        }

                                        response.Produtos.Add(produto.Sku, produto);
                                    }
                                    else
                                        continue;

                                }

                            }
                        }
                    }

                    selector.paging.IncreaseOffset();

                } while(selector.paging.startIndex < page.totalNumEntries);

            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Falha ao buscar produtos. Criterios: {0}. Erro: {1}", predicates, e));
            }
        }

        public List<T> BuscarSimulacoes<T>(long groupId, long googleId) where T : ISimulacaoResponse, new()
        {
            var result = new List<T>();

            DataService dataService = (DataService)User.GetService(AdWordsService.v201705.DataService);

            // Create selector.
            Selector selector = new Selector()
            {
                fields = new string[] {
                                            CriterionBidLandscape.Fields.CriterionId,
                                            CriterionBidLandscape.Fields.StartDate,
                                            CriterionBidLandscape.Fields.EndDate,
                                            CriterionBidLandscape.Fields.CampaignId,
                                            CriterionBidLandscape.Fields.AdGroupId,
                                            BidLandscapeLandscapePoint.Fields.Bid,
                                            BidLandscapeLandscapePoint.Fields.LocalClicks,
                                            BidLandscapeLandscapePoint.Fields.LocalCost,
                                            BidLandscapeLandscapePoint.Fields.LocalImpressions
            },
                predicates = new Predicate[] {
                    Predicate.Equals(CriterionBidLandscape.Fields.AdGroupId, groupId),
                    Predicate.Equals(CriterionBidLandscape.Fields.CriterionId, googleId)
            },
                ordering = new OrderBy[] { OrderBy.Asc(CriterionBidLandscape.Fields.CriterionId),
                                           OrderBy.Desc(BidLandscapeLandscapePoint.Fields.LocalImpressions)
                },
                paging = Paging.Default
            };

            int landscapePointsInLastResponse = 0;

            try
            {
                CriterionBidLandscapePage page = null;

                do
                {
                    page = dataService.getCriterionBidLandscape(selector);
                    landscapePointsInLastResponse = 0;

                    if (page != null && page.entries != null)
                    {
                        foreach (CriterionBidLandscape bidLandscape in page.entries)
                        {
                            foreach (BidLandscapeLandscapePoint point in bidLandscape.landscapePoints)
                            {
                                result.Add(new T(){
                                    CriterionId = bidLandscape.criterionId,
                                    StartDate = bidLandscape.startDate,
                                    EndDate = bidLandscape.endDate,
                                    CampaignId = bidLandscape.campaignId,
                                    AdGroupId = bidLandscape.adGroupId,
                                    Bid = point.bid.microAmount,
                                    Clicks = point.clicks,
                                    Impressions = point.impressions,
                                    Cost = point.cost.microAmount
                                });

                                landscapePointsInLastResponse++;
                            }
                        }
                    }
                    selector.paging.IncreaseOffsetBy(landscapePointsInLastResponse);
                } while (landscapePointsInLastResponse > 0);
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Falha ao atualizar simulacoes. Erro: {0}.", e.Message));
            }

            return result;
        }


        #region Obter atributos google para inserção de Bid
        /// <summary>
        /// Adicionar um SKU a um grupo de campanha específico.
        /// </summary>
        /// <param name="GoogleIdRef">Código google referente a sub-grupo cadastrado. Exemplo: Livros (Grupo) > Religião (Sub-Grupo)</param>
        /// <param name="produtoGoogle">Objeto contendo identificador do produto com o CPC alvo.</param>
        private AdGroupCriterionOperation GetOperationForInsertBid(IInsertUpdateBidRequest produtoGoogle)
        {
            AdGroupCriterionService adGroupCriterionService = (AdGroupCriterionService)User.GetService(AdWordsService.v201705.AdGroupCriterionService);

            ProductPartition product = new ProductPartition();
            product.type = CriterionType.PRODUCT_PARTITION;
            product.partitionType = ProductPartitionType.UNIT;

            ProductOfferId offer = new ProductOfferId();
            offer.value = produtoGoogle.OfferId;
            product.caseValue = offer;
            product.parentCriterionId = produtoGoogle.ParentId;

            BiddingStrategyConfiguration biddingConfig = new BiddingStrategyConfiguration();
            CpcBid cpcBid = new CpcBid();
            cpcBid.bid = new Money();
            cpcBid.bid.microAmount = produtoGoogle.CpcBid < 10000 ? 10000 : produtoGoogle.CpcBid;
            biddingConfig.bids = new Bids[] { cpcBid };

            BiddableAdGroupCriterion keywordCriterion = new BiddableAdGroupCriterion();
            //keywordCriterion.baseCampaignId = 
            keywordCriterion.adGroupId = produtoGoogle.AdGroupId;
            keywordCriterion.criterion = product;
            keywordCriterion.biddingStrategyConfiguration = biddingConfig;

            keywordCriterion.userStatus = UserStatus.ENABLED;

            AdGroupCriterionOperation operation = new AdGroupCriterionOperation();
            operation.@operator = Operator.ADD;
            operation.operand = keywordCriterion;

            return operation;

        }
        #endregion

        /*public bool AtualizarBids(IEnumerable<IInsertBidRequest> produtosGoogle)
        {
            var lista = new List<AdGroupCriterionOperation>();

            foreach (var produtoGoogle in produtosGoogle)
                lista.Add(GetOperationForUpdateBid(produtoGoogle));

            return MudateAdGroupCriterion(lista);
        }*/

        public string AdicionarBid(IInsertUpdateBidRequest produtoGoogle)
        {
            var lista = new List<AdGroupCriterionOperation>();

            if (produtoGoogle.GoogleId == 0)
               lista.Add(GetOperationForInsertBid(produtoGoogle));
            else
               lista.Add(GetOperationForUpdateBid(produtoGoogle));
            
            try
            {
                var afetados = MudateAdGroupCriterion(lista);

                if (afetados != null && afetados.Length != 0 && afetados[0].criterion != null)
                    return Convert.ToString(afetados[0].criterion.id);
                else
                    return "Sku nao incluido.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string ExcluirBids(IEnumerable<IConfigRequest> produtosGoogle)
        {
            var lista = new List<AdGroupCriterionOperation>();

            foreach (var produtoGoogle in produtosGoogle)
                lista.Add(GetOperationForExcludeBid(produtoGoogle));

            try
            {
                var excluidos = MudateAdGroupCriterion(lista);

                if (excluidos != null && excluidos.Length == lista.Count)
                   return "OK";
                else
                    return "NOK";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        #region Obter critérios para atualização de bid
        /// <summary>
        /// Obtendo objeto de operação preparado para atualização de BID.
        /// </summary>
        /// <param name="produtoGoogle">Objeto contendo identificador do produto com o CPC alvo.</param>
        /// <param name="operacao">Numerador de operação: ADD, SET, REMOVE.</param>
        private AdGroupCriterionOperation GetOperationForUpdateBid(IInsertUpdateBidRequest produtoGoogle)
        {
            Criterion criterion = new Criterion();

            criterion.id = produtoGoogle.GoogleId;

            BiddableAdGroupCriterion biddableAdGroupCriterion = new BiddableAdGroupCriterion();
            biddableAdGroupCriterion.adGroupId = produtoGoogle.AdGroupId;
            biddableAdGroupCriterion.criterion = criterion;

            BiddingStrategyConfiguration biddingConfig = new BiddingStrategyConfiguration();
            CpcBid cpcBid = new CpcBid();
            cpcBid.bid = new Money();

            cpcBid.bid.microAmount = produtoGoogle.CpcBid < 10000 ? 10000 : produtoGoogle.CpcBid;
            biddingConfig.bids = new Bids[] { cpcBid };

            biddableAdGroupCriterion.biddingStrategyConfiguration = biddingConfig;

            AdGroupCriterionOperation operation = new AdGroupCriterionOperation();
            operation.@operator = Operator.SET;
            operation.operand = biddableAdGroupCriterion;

            return operation;
        }

        #endregion

        private AdGroupCriterionOperation GetOperationForConvertBid(IConfigRequest produtoGoogle)
        {
            ProductPartition criterion = new ProductPartition();

            criterion.parentCriterionId = produtoGoogle.GoogleId;
            criterion.partitionType = ProductPartitionType.SUBDIVISION;

            ProductCustomAttribute attribute = new ProductCustomAttribute();
            attribute.value = "religiao";
            attribute.type = ProductDimensionType.CUSTOM_ATTRIBUTE_1;
            criterion.caseValue = attribute;

            BiddableAdGroupCriterion biddableAdGroupCriterion = new BiddableAdGroupCriterion();
            biddableAdGroupCriterion.adGroupId = produtoGoogle.AdGroupId;
            biddableAdGroupCriterion.criterion = criterion;

            BiddingStrategyConfiguration biddingConfig = new BiddingStrategyConfiguration();
            biddingConfig.bids = null;

            biddableAdGroupCriterion.biddingStrategyConfiguration = biddingConfig;

            AdGroupCriterionOperation operation = new AdGroupCriterionOperation();
            operation.@operator = Operator.ADD;
            operation.operand = biddableAdGroupCriterion;

            return operation;
        }


        private AdGroupCriterionOperation GetOperationForExcludeBid(IConfigRequest produtoGoogle)
        {
            Criterion criterion = new Criterion();

            criterion.id = produtoGoogle.GoogleId;

            BiddableAdGroupCriterion biddableAdGroupCriterion = new BiddableAdGroupCriterion();
            biddableAdGroupCriterion.adGroupId = produtoGoogle.AdGroupId;
            biddableAdGroupCriterion.criterion = criterion;

            AdGroupCriterionOperation operation = new AdGroupCriterionOperation();
            operation.@operator = Operator.REMOVE;
            operation.operand = biddableAdGroupCriterion;

            return operation;
        }


        private long MudateAddAdGroupCriterion(List<AdGroupCriterionOperation> operations)
        {
            AdGroupCriterionService adGroupCriterionService = (AdGroupCriterionService)User.GetService(AdWordsService.v201705.AdGroupCriterionService);

            try
            {
                var retVal = adGroupCriterionService.mutate(operations.ToArray());

                if (retVal != null && retVal.value != null && retVal.value.Length > 0)

                    return (retVal.value[0].criterion as ProductPartition).id;

                else

                    return 0;

            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Falha ao atualizar produto. Erro: {0}.", e.Message));
            }
        }

        public IEnumerable<T> CarregarDimensoes<T>() where T : IDimensaoResponse, new()
        {
            var result = new List<T>();

            ConstantDataService constantDataService = (ConstantDataService)User.GetService(AdWordsService.v201705.ConstantDataService);

            Selector selector = new Selector()
            {
                predicates = new Predicate[] { Predicate.In(ProductBiddingCategoryData.Fields.Country, new string[] { "BR" }),
                Predicate.Equals(ProductBiddingCategoryData.Fields.BiddingCategoryStatus, ShoppingBiddingDimensionStatus.ACTIVE.ToString())
                }
            };

            ProductBiddingCategoryData[] results = constantDataService.getProductBiddingCategoryData(selector);
            foreach (ProductBiddingCategoryData productBiddingCategory in results)
            {
                foreach (var item in productBiddingCategory.displayValue)
                { 
                    result.Add( new T() {Id = productBiddingCategory.dimensionValue.value, Descricao = item.value, ParentId = productBiddingCategory.parentDimensionValue.value });
                }
            }

            return result;
        }

        private AdGroupCriterion[] MudateAdGroupCriterion(List<AdGroupCriterionOperation> operations)
        {
            AdGroupCriterionService adGroupCriterionService = (AdGroupCriterionService)User.GetService(AdWordsService.v201705.AdGroupCriterionService);

            try
            {
                var retVal = adGroupCriterionService.mutate(operations.ToArray());

                if (retVal != null)
                    return retVal.value;
                else
                    return null;
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Falha na tentativa de incluir novo bid. Erro: {0}.", e.Message));
            }
        }
    }

}
