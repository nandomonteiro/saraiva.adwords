﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201705;
using Saraiva.AdWords.Host.Domain;
using System;
using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Repositories
{
    public class CampanhaRepository : BaseRepository, ICampanhaRepository
    {
        #region Buscar campanhas
        /// <summary>
        /// Buscar todas Campanhas especificadas ou não pelo identificador
        /// </summary>
        public IEnumerable<T> BuscarCampanhas<T>() where T : ICampanhaGoogle, new()
        {
            return BuscarCampanhas<T>(null);
        }

        /// <summary>
        /// Buscar todas Campanhas especificadas pelo identificador
        /// </summary>
        /// <param name="Id">Identificador google para a companha.</param>
        public IEnumerable<T> BuscarCampanhas<T>(long id) where T : ICampanhaGoogle, new()
        {
            return BuscarCampanhas<T>(new Predicate[] { Predicate.Equals(Campaign.Fields.Id, id) });
        }

        public IEnumerable<T> BuscarCampanhasPorLike<T>(string descricao) where T : ICampanhaGoogle, new()
        {
            return BuscarCampanhas<T>(new Predicate[] { Predicate.Contains(Campaign.Fields.Name, descricao) });
        }

        /// <summary>
        /// Buscar informações de critérios do tipo partição de um produto definidos por um predicado de busca.
        /// </summary>
        /// <param name="predicates">Critérios google a serem encapsulados no seletor.</param>
        private IEnumerable<T> BuscarCampanhas<T>(Predicate[] predicates) where T : ICampanhaGoogle, new()
        {
            CampaignService campaignService = (CampaignService)User.GetService(AdWordsService.v201705.CampaignService);

            Selector selector = new Selector()
            {
                fields = new string[] {
                    Campaign.Fields.Id,
                    Campaign.Fields.Name,
                    Campaign.Fields.Status,
                    Budget.Fields.Amount,
                    Campaign.Fields.StartDate,
                    Campaign.Fields.EndDate

            },
                paging = Paging.Default
            };

            selector.predicates = predicates;

            CampaignPage page = new CampaignPage();

            var result = new List<T>();
            try
            {
                do
                {
                    // Get the campaigns.
                    page = campaignService.get(selector);

                    // Display the results.
                    if (page != null && page.entries != null)
                    {
                        int i = selector.paging.startIndex;
                        foreach (Campaign campaign in page.entries)
                        {
                            result.Add(new T {
                                Id = campaign.id,
                                Nome = campaign.name,
                                Status = campaign.status.ToString(),
                                DataInicio = campaign.startDate,
                                DataFim = campaign.endDate,
                                Orcamento = campaign.budget.amount.microAmount
                            });

                        }
                    }
                    selector.paging.IncreaseOffset();
                } while (selector.paging.startIndex < page.totalNumEntries);
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Falha ao receber campanhas", e);
            }

            return result;
        }
        #endregion
    }

}
