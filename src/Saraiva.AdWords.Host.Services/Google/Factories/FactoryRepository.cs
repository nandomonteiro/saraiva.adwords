﻿using Saraiva.AdWords.Host.Repositories;

namespace Saraiva.AdWords.Host.Repositories
{
    public class FactoryRepository
    {
        public static ICampanhaRepository Campanha
        {
            get
            {
                return new CampanhaRepository();
            }
        }

        public static IGrupoRepository Grupo
        {
            get
            {
                return new GrupoRepository();
            }
        }

        public static ProdutoRepository Produto
        {
            get
            {
                return new ProdutoRepository();
            }
        }

        public static RelatorioRepository Relatorios
        {
            get
            {
                return new RelatorioRepository();
            }
        }

    }
}
