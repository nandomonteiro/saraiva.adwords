﻿using Newtonsoft.Json;
using Saraiva.AdWords.DI.Factory;
using Saraiva.AdWords.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using Saraiva.AdWords.Service.PlugInAdicionarBid;


namespace Saraiva.AdWords.Host.ExecutarPlugins
{
    class Program
    {

        static int idtransaction = 0;

        static Dictionary<string, Func<List<MessageQueue>>> _acoes =
            new Dictionary<string, Func<List<MessageQueue>>>()
            {
                {"1", ObterMaisVendidos },
                {"2", RetirarDeCampanha },
                {"3", ObterMensagensPendentes },
                {"4", RecebeDaFila }
            };

        static void Main(string[] args)
        {
            //Reprocessar();
            //Console.ReadLine();

            //return;

            if (args.Length == 0 || !_acoes.ContainsKey(args[0]))
                return;

            var lacao = _acoes[args[0]];

            try
            {
                ProcessarFila(lacao);
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao processar fila: " + e.Message);
            }
        }

        public static void Reprocessar()
        {
            idtransaction = 0;

            var list = new List<AdicionarBidRequest>();

            list.Add(new AdicionarBidRequest() { sku = 7900511, itens = 160, qtde = 184 });
            list.Add(new AdicionarBidRequest() { sku = 9726323, itens = 150, qtde = 70 });
            list.Add(new AdicionarBidRequest() { sku = 9417597, itens = 93, qtde = 75 });

            var message = new MessageQueue();

            message.Acao = "Insert";
            message.Mensagem = JsonConvert.SerializeObject(list);

            var result = new List<MessageQueue>() { message };

            var plugin = new PlugInAdicionarBid(message.Acao, message.Mensagem, new ServicoGoogle(), new ServicoJson(), idtransaction);

            plugin.ExecutarServico();

        }

        public static string GetConnection(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        public static List<MessageQueue> RetirarDeCampanha()
        {
            idtransaction = 0;

            return new List<MessageQueue>() { new MessageQueue() { Acao = "Delete", Mensagem = " " } };
        }

        public static void ProcessarFila(Func<List<MessageQueue>> acao)
        {
            var messages = acao();

            var plugins = new List<IPlugInService>();

            foreach (var msg in messages.ToArray())
            {
                var nomePlugin = ConfigurationManager.AppSettings[msg.Acao];

                if (nomePlugin == null)
                    continue; 

                var plugin = GetPlugin(nomePlugin, msg.Acao, msg.Mensagem);

                if (plugin != null)
                {
                    messages.Remove(msg);
                    plugins.Add(plugin);
                }
            }

            if (messages.Count > 0)
                SendMessage(messages);

            if (plugins.Count > 0)
                ExecutarPlugins(plugins);
        }

        #region Execução de Plugins

        public static void ExecutarPlugins(List<IPlugInService> plugins)
        {
            var tasks = new List<Task>();

            foreach (var plugin in plugins)
            {
                plugin.ExecutarServico();
            }
        }

        static public IPlugInService GetPlugin(string nomePlugin, string acao, string mensagem)
        {
            try
            {
                var assembly = Assembly.LoadFile(nomePlugin);

                Type[] types = assembly.GetTypes();

                var argumentos = new object[] { acao, mensagem, new ServicoGoogle(), new ServicoJson(), idtransaction };

                foreach (Type t in types)
                {
                    if (t.GetInterface("IPlugInService") != null)
                        return (IPlugInService)Activator.CreateInstance(t, argumentos);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("{0} - Erro ao carregar PlugIn {1} Erro - {2}", "GetPlugin", ex.Message, nomePlugin));
                return null;
            }

            return null;
        }

        #endregion

        #region Métodos da Fila
        public static void SendMessage(MessageQueue messenge)
        {
            try
            {
                EnviaParaFila(messenge);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Erro ao tentar inserir na fila. Acao {0} Mensagem {1} Erro {2}", messenge.Acao, messenge.Mensagem, e.Message));
            }
        }

        public static void SendMessage(IEnumerable<MessageQueue> messenges)
        {
            foreach (var msg in messenges)
            {
                try
                {
                    EnviaParaFila(msg);
                }
                catch (Exception e)
                {
                    Console.WriteLine(string.Format("Erro ao tentar inserir na fila. Acao {0} Mensagem {1} Erro {2}", msg.Acao, msg.Mensagem, e.Message));
                }
            }
        }

        public static void EnviarParaFila(string action, object messenge)
        {
            var msg = JsonConvert.SerializeObject(messenge);
            var request = new MessageQueue { Acao = action, Mensagem = msg };

            try
            {
                EnviaParaFila(request);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Erro ao tentar inserir na fila. Acao {0} Mensagem {1} Erro {2}", action, msg, e.Message));
            }
        }

        public static void SendMessage(string action, string messenge)
        {
            var request = new MessageQueue { Acao = action, Mensagem = messenge };

            try
            {
                EnviaParaFila(request);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Erro ao tentar inserir na fila. Acao {0} Mensagem {1} Erro {2}", action, messenge, e.Message));
            }
        }

        public static void EnviaParaFila(MessageQueue request)
        {
            var srv = AdWordsFactory.CriarFila();
            try
            { 
            if (srv.InserirFilaSQS(JsonConvert.SerializeObject(request)))
                Console.WriteLine("Inserido com sucesso em fila");
            else
                Console.WriteLine("Fila não adicionado");
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("Fila não adicionado. Erro: {0}", e.Message));
            }
        }

        public static List<MessageQueue> RecebeDaFila()
        {
            idtransaction = 0;

            var srv = AdWordsFactory.CriarFila();

            var posts = srv.ProcessarFilaSQS();

            var acoes = new Dictionary<string, MessageQueue>();

            var result = new List<MessageQueue>();

            foreach (var post in posts)
            {
                try
                {
                    var message = JsonConvert.DeserializeObject<MessageQueue>(post);

                    if (acoes.ContainsKey(message.Acao))
                        IncluirTransacao(message.Acao, post, "AE");
                    else
                    {
                        acoes.Add(message.Acao, message);
                        result.Add(message);
                    }
                }
                catch
                {
                    IncluirTransacao("Erro", post, "MP");
                }

            }

            return result;
        }

        public static void IncluirTransacao(string acao, string mensagem, string status)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(GetConnection("DBINTERNET")))
                {
                    string sql = @"INSERT INTO GoogleAdwordsTransaction (GAN_Acao, GAN_DtInicio, GAN_Response, GAN_Status) VALUES (@acao, GETDATE(), @mensagem, @status)";
                    
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("@acao", acao);
                    cmd.Parameters.AddWithValue("@mensagem", mensagem);
                    cmd.Parameters.AddWithValue("@status", status);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("InsertTransaction: {0}", e.Message);
            }
        }

        public static List<MessageQueue> ObterMensagensPendentes()
        {
            var result = new List<MessageQueue>();

            try
            {
                using (SqlConnection conn = new SqlConnection(GetConnection("DBINTERNET") ))
                {
                    string sql = @" select	TOP 1 GAN_Id, GAN_Response 
                                    from	GoogleAdwordsTransaction 
                                    where	GAN_Status = 'AE'
                                    order by GAN_DtInicio DESC";

                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    var reader = cmd.ExecuteReader();

                    if (reader == null || !reader.HasRows || !reader.Read() )
                        return result;

                    idtransaction = reader.GetInt32(0);

                    var mensagem = reader.GetString(1);

                    var obj = JsonConvert.DeserializeObject<MessageQueue>(mensagem);

                    if (obj != null)
                        result.Add(obj);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("InsertTransaction: {0}", e.Message);
            }
            return result;
        }

        public static string ObterParametro(string type, string key, string def)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(GetConnection("DBINTERNET")))
                {
                    string sql = @" SELECT	ISNULL(MAX(GAR_Value),@default)
                                    FROM	GoogleAdwordsParams 
                                    WHERE	GAR_TYPE = @type
	                                    AND	GAR_Key  = @Key";

                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("@default", def);
                    cmd.Parameters.AddWithValue("@type", type);
                    cmd.Parameters.AddWithValue("@key", key);

                    return (string)cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Erro na tentativa de obter parametro type {0}, key {1}. Erro: {2}", type, key, e.Message));
            }
        }

        public static int ObterMenorQtdeVendida()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(GetConnection("DBINTERNET")))
                {
                    string sql = @" SELECT	ISNULL(MIN(BRA_VlrPrecoDe),70)
                                    FROM	BuscadorDescrRegra
                                    WHERE	BRA_Descricao LIKE '%AUT]'
	                                    AND	BRA_LgAtivo = 'S'";

                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    var result = cmd.ExecuteScalar();

                    return Convert.ToInt32(result);
                }
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Erro na tentativa de obter menor parametro de menor quantidade vendida. Erro {0}", e.Message));
            }
        }



        public static List<MessageQueue> ObterMaisVendidos()
        {
            var lintervalo = int.Parse(ObterParametro("MAIS_VENDIDOS", "MAIS_VENDIDOS_HORAS", "3"));
            var llimite = ObterMenorQtdeVendida();

            var result = new List<MessageQueue>();

            try
            {
                using (var conn = new MySqlConnection(GetConnection("DBMAGENTO")))
                {
                    string sql = @" select
                                            sales_flat_order_item.sku,
                                            count(1),
                                            sum(sales_flat_order_item.qty_ordered)
                                    from    sales_flat_order s
                                      inner join sales_flat_order_item 
                                            on s.entity_id = sales_flat_order_item.order_id
                                      inner join  catalog_category_product 
                                            on catalog_category_product.product_id = sales_flat_order_item.product_id
                                    where s.created_at >= date(now() - interval @intervalo HOUR)
                                    GROUP by sku
                                    HAVING sum(sales_flat_order_item.qty_ordered) >= @limite";
                    conn.Open();

                    var cmd = new MySqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@intervalo", lintervalo);
                    cmd.Parameters.AddWithValue("@limite", llimite);

                    var reader = cmd.ExecuteReader();

                    if (reader == null || !reader.HasRows)
                        return result;

                    var list = new List<AdicionarBidRequest>();

                    while (reader.Read())
                    {
                        list.Add(new AdicionarBidRequest() { sku = Convert.ToInt32(reader.GetString(0)), itens = reader.GetInt32(1), qtde = reader.GetInt32(2)});
                    }

                    var message = new MessageQueue();

                    message.Acao = "Insert";
                    message.Mensagem = JsonConvert.SerializeObject(list);

                    result.Add(message);

                    return result;
                }
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Erro na tentativa de obter mais vendidos. Erro {0}", e.Message));
            }
        }

        #endregion
    }

    public class AdicionarBidRequest
    {
        public int sku { get; set; }
        public int qtde { get; set; }
        public int itens { get; set; }
    }

    public class MessageQueue
    {
        public string Acao { get; set; }
        public string Mensagem { get; set; }
    }

    public class ServicoJson : IJsonService
    {
        public T Deserialize<T>(string strObjeto)
        {
            return JsonConvert.DeserializeObject<T>(strObjeto);
        }

        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }

    public class ServicoGoogle : IAPIService
    {
        private HttpClient client = new HttpClient();

        public ServicoGoogle()
        {
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["GoogleService"]);
        }

        public async Task<T> ExecutaAcao<T>(string action, object request)
        {
            var requestSerializado = JsonConvert.SerializeObject(request);

            StringContent content = new StringContent(requestSerializado);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = await client.PostAsync(action, content);

            if (response.IsSuccessStatusCode)
            {
                var obj = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<T>(obj);
            }
            else
            {
                throw new Exception(String.Format("Erro detectado. Codigo: {0} Mensagem: {1}", response.StatusCode, response.RequestMessage));
            }
        }

        public async Task<T> ExecutaAcao<T>(string action)
        {
            HttpResponseMessage response = await client.GetAsync(action);

            if (response.IsSuccessStatusCode)
            {
                var obj = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<T>(obj);
            }
            else
            {
                throw new Exception(String.Format("Erro detectado. Codigo: {0} Mensagem: {1}", response.StatusCode, response.RequestMessage));
            }
        }

        public async Task<T> ExecutaAcao<T>(string action, string messenge)
        {
            StringContent content = new StringContent(messenge);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = await client.PostAsync(action, content);

            if (response.IsSuccessStatusCode)
            {
                var obj = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<T>(obj);
            }
            else
            {
                throw new Exception(String.Format("Erro detectado. Codigo: {0} Mensagem: {1}", response.StatusCode, response.RequestMessage));
            }
        }


    }


}
