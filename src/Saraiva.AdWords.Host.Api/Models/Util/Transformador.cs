﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Saraiva.AdWords.Host.Domain
{
    public static class Transformador
    {
        private const string SEPARADOR_ARRAY = " < ";
        private static string[] GetProperties<T>()
        {
            Type myType = typeof(T);
            var myProperties = myType.GetProperties();
            var lista = new List<string>();

            foreach (var property in myProperties)
            {
                lista.Add(property.Name);
            }

            return lista.ToArray();
        }

        public static string Executar<T>(string _separador, List<T> list, Func<string, object, bool> condicao = null)
        {
            try
            {
                return GerarArquivo(_separador, list, condicao);
            }
            catch
            {
                return null;
            }
        }

        private static string GerarArquivo<T>(string _separador, List<T> list)
        {
            var properties = GetProperties<T>();

            return GerarArquivo<T>(properties, properties, _separador, list, null);
        }
        private static string GerarArquivo<T>(string _separador, List<T> list, Func<string, object, bool> condicao)
        {
            var properties = GetProperties<T>();

            return GerarArquivo<T>(properties, properties, _separador, list, condicao);
        }

        private static string GerarArquivo<T>(object[] cabecalho, string _separador, List<T> list, Func<string, object, bool> condicao = null)
        {
            var properties = GetProperties<T>();

            return GerarArquivo<T>(cabecalho, properties, _separador, list, condicao);
        }
        private static string GerarArquivo<T>(object[] cabecalho, string[] properties, string _separador, List<T> list, Func<string, object, bool> condicao = null)
        {
            if (list.Count == 0)
                return null;

            var builder = new StringBuilder();

            string linha = string.Join(_separador, cabecalho);

            builder.AppendLine(linha);

            foreach (var item in list)
            {
                try
                {
                    var values = CarregarValores<T>(properties, item, condicao);

                    if (values == null)
                        continue;

                    linha = String.Join(_separador, values);

                    builder.AppendLine(linha);
                }
                catch
                {
                    return null;
                }
            }

            return builder.ToString();
        }

        private static object[] CarregarValores<T>(string[] properties, T obj, Func<string, object, bool> condicao = null)
        {
            var values = new List<object>();
            var linhas = 0;
            foreach (var property in properties)
            {
                linhas++;
                try
                {
                    object value = null;

                    value = obj.GetType().GetProperty(property).GetValue(obj, null);

                    if (condicao != null && !condicao(property, value))
                        return null;

                    if (value is IEnumerable<string>)
                    {
                        var tmp = String.Empty;
                        foreach (var item in value as IEnumerable<string>)
                        {
                            if (tmp == String.Empty)
                                tmp = item;
                            else
                                tmp += SEPARADOR_ARRAY + item;
                        }
                        values.Add(tmp == null ? String.Empty : tmp);
                    }
                    else
                    {
                            values.Add(value == null ? String.Empty : value);
                    }
                }
                catch (Exception e)
                {
                    values.Add(string.Format("Erro: {0} - {1}", property, e.Message));
                }
            }

            return values.ToArray();
        }
    }
}