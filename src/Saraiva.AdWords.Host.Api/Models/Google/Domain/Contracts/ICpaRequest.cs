﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{
    public interface ICpaRequest
    {
        string OfferId { get; set; }
        string Date { get; set; }
        long CampaignId { get; set; }
        long AdGroupId { get; set; }
        long Clicks { get; set; }
        long Impressions { get; set; }    
        decimal Cost { get; set; }
        double Conversions { get; set; }
        decimal CostPerConversion { get; set; }
        double ValuePerConversion { get; set; }
        double ValuePerAllConversion { get; set; }
    }

}
