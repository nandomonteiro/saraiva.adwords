﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{
    public interface ITopClicks
    {
        long CriterionId { get; set; }
        string StartDate { get; set; }
        string EndDate { get; set; }
        long CampaignId { get; set; }
        long AdGroupId { get; set; }
        long Bid { get; set; }
        long Clicks { get; set; }
        long Cost { get; set; }
        long Impressions { get; set; }
    }

}
