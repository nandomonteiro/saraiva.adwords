﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Domain
{
    public interface IConfigRequest
    {
        long AdGroupId { get; set; }
        long GoogleId { get; set; }
    }

}
