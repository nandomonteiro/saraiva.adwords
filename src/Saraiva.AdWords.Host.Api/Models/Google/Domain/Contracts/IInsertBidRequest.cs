﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Domain
{
    public interface IInsertUpdateBidRequest
    {
        string OfferId { get; set; }
        long AdGroupId { get; set; }
        long ParentId { get; set; }
        long GoogleId { get; set; }
        long CpcBid { get; set; }
    }

}
