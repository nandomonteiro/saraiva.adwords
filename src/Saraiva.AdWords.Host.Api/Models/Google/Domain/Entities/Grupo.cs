﻿namespace Saraiva.AdWords.Host.Domain
{
    public class Grupo : IGrupoGoogle
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public long CampanhaId { get; set; }
        public long CpcBid { get; set; }
        public string Status { get; set; }
    }
}
