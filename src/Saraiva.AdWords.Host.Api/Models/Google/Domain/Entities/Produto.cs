﻿using System;

namespace Saraiva.AdWords.Host.Domain
{
    public class Produto : IProdutoGoogle
    {
        public long GoogleId { get; set; }
        public long GrupoId { get; set; }
        public string Sku { get; set; }
        public long Cpc { get; set; }
        public long NewCpc { get; set; }
        public decimal Cpa { get; set; }
        public decimal Custo { get; set; }
        public double Conversoes { get; set; }
        public long Clicks { get; set; }
        public string Type { get; set; }
        public bool IsMarca { get; set; }
        public bool IsCategoria { get; set; }
        public long ParentGoogleId { get; set; }
        public string Categoria { get; set; }
        public long Dimensao { get; set; }
        public bool IsExcluded { get; set; }
        public string Status { get; set; }

  
    }
}
