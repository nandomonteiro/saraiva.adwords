﻿using Saraiva.AdWords.Host.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Domain
{
    public class Bid
    {
        private const string cte_Categoria = "ProductType";
        private const string cte_Marca = "Brand";
        private const string cte_GrupoProduto = "Group";
        public enum Acao {
            Inclusao,
            Alteracao,
            Categorizar,
            Marcar,
            Simulacao,
            Validacao
        };

        private IGoogleResponse response;
        private IProdutoRepository repository;
        private IRelatoriosRepository report;

        private List<IProdutoGoogle> listaTarefaAtualizacaoBid;
        private List<IProdutoGoogle> listaTarefaInclusaoBid;

        private Status stsAtualizar = new Status() { Acao = Acao.Alteracao, FlExecucao = true };
        private Status stsIncluir = new Status() { Acao = Acao.Inclusao, FlExecucao = true };
        private Status stsCategoria = new Status() { Acao = Acao.Categorizar, FlExecucao = true };
        private Status stsSimulacao = new Status() { Acao = Acao.Simulacao, FlExecucao = true };


        public struct Saneamento
        {
            public bool flAtualizar, flIncluir;
        }
        public struct Status
        {
            public bool FlExecucao;
            public string MsgExecucao;
            public long Id;
            public Acao Acao;
        }
        public Bid(IProdutoRepository repository, IRelatoriosRepository report): this(repository)
        {
            this.report = report;
        }

        public Bid(IProdutoRepository repository)
        {
            this.repository = repository;
            this.listaTarefaAtualizacaoBid = new List<IProdutoGoogle>();
            this.listaTarefaInclusaoBid = new List<IProdutoGoogle>();
        }


        public IEnumerable<IClearingResponse> Consultar(IClearingRequest request)
        {
            if (!ValidarRequisicao(request))
                return null;

            this.response = new GoogleResponse { GrupoId = request.GrupoId };

            var operacoes = new List<Status>();

            BuscarProdutosNoGoogle(request.GrupoId);

            var saneamento = SanearSKUs(request.GrupoId, request.Skus);

            if (saneamento.flAtualizar)
            {
                operacoes.Add(stsAtualizar);
            }

            if (saneamento.flIncluir)
                operacoes.Add(stsIncluir);

            return TrataRetornoSaneamento(operacoes);
        }

        public IBaseResponse Update(ICrudRequest request)
        {
            var response = new UpdateResponse();

            try
            {
                AtualizarBidsNoGoogle(request);
                response.FlErro = false;
            }
            catch (Exception e)
            {
                response.FlErro = true;
                response.MsgErro = e.Message;
            }

            return response;
        }

        public IEnumerable<IRequestSku> GetCategories(ICrudRequest request)
        {
            var categorias = CarregarCategoria(new GoogleResponse { GrupoId = request.GrupoId });

            foreach (var sku in request.Skus)
            {
                if (categorias.ContainsKey(sku.Cat))
                    sku.Id = categorias[sku.Cat];
            }

            return request.Skus;
        }

        private Dictionary<string, long> CarregarCategoria(IGoogleResponse response )
        {
            repository.CarregarMarcaCategorias<Produto>(response);

            var categorias = new Dictionary<string, long>();

            foreach (var cat in response.Categorias)
            {
                if (response.Categorias.ContainsKey(cat.Value.ParentGoogleId))
                    categorias.Add(String.Format("{0}|{1}", response.Categorias[cat.Value.ParentGoogleId].Categoria, cat.Value.Categoria), cat.Key);
            }

            return categorias;
        }
       
        private IEnumerable<IClearingResponse> TrataRetornoSaneamento(IEnumerable<Status> operacoes)
        {
            var result = new List<IClearingResponse>();

            var dic = new Dictionary<Acao, IEnumerable<IProdutoGoogle>>();

            dic.Add(Acao.Inclusao, listaTarefaInclusaoBid);
            dic.Add(Acao.Alteracao, listaTarefaAtualizacaoBid);
            dic.Add(Acao.Simulacao, response.Simulacoes.Values);

            foreach (var item in operacoes)
            {
                var response = new ClearingResponse
                {
                    MsgErro = item.MsgExecucao,
                    FlErro =  false,
                    Acao = item.Acao.ToString()
                };
                response.AddSkus(dic[item.Acao]);

                result.Add(response);
            }

            return result;
        }

        private bool ValidarRequisicao(IBaseRequest request)
        {
            return (request != null && repository != null && request.GrupoId != 0);
        }

        private void ValidarRequisicao(IBaseRequest request, IBaseResponse response)
        {
            response.FlErro = !ValidarRequisicao(request);
            if (response.FlErro)
                response.MsgErro = "Parametros invalidos.";
        }
        private void BuscarProdutosNoGoogle(long GrupoId)
        {
            repository.CarregarGrupoProduto<Produto>(response);
        }

        private Saneamento SanearSKUs(long GroupId, IEnumerable<string> skus)
        {
            Saneamento result = new Saneamento();

            foreach (var sku in skus)
            {
                if (response.Produtos.ContainsKey(sku))
                {
                    listaTarefaAtualizacaoBid.Add(response.Produtos[sku]);
                    if (!result.flAtualizar)
                        result.flAtualizar = true;
                }
                else
                {
                    listaTarefaInclusaoBid.Add(new Produto { Sku = sku, GrupoId = GroupId });
                    if (!result.flIncluir)
                        result.flIncluir = true;
                }
            }

            return result;
        }

        private IDictionary<long, Produto> BuscarCpaNoGoogle(ICrudRequest request)
        {
            try
            {
                return report.RelProdutosPorGrupo<Produto>(request.GrupoId);
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Erro ao Buscar CPAs no Google. Erro: {0}.", e.Message));
            }
        }

        private void AtualizarBidsNoGoogle(ICrudRequest request)
        {
            try
            {
                var listaAtualizacao = new List<IProdutoGoogle>();

                foreach (var item in request.Skus)
                {
                    IProdutoGoogle p = new Produto { Sku = item.Sku, GoogleId = item.Id, Cpc = item.Cpc };
                    listaAtualizacao.Add(p);
                }

                repository.AtualizarBids(listaAtualizacao);
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Erro ao Atualizar Bids. Erro: {0}.", e.Message));
            }
        }

        private void AdicionarBidsNoGoogle(IInsertBidRequest request)
        {
            try
            {
                foreach (var item in request.Skus)
                {
                    IProdutoGoogle p = new Produto { GrupoId = request.GrupoId, Sku = item.Sku, ParentGoogleId = item.Id, Cpc = item.Cpc };

                    item.Id = repository.AdicionarBid(p);
                }
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Erro ao adicionar Bids no Google Erro: {0}.", e.Message));
            }
        }
        /*
        private void CarregarAtributosGoogle()
        {
            if (String.IsNullOrWhiteSpace(request.Marca))
                repository.CarregarCategorias<Produto>(request.GrupoId, response);
            else
            { 
                repository.CarregarMarcaCategorias<Produto>(request.GrupoId, response);

                foreach (var marca in response.Marcas.Values.Where(x => x.Marca == request.Marca))
                {
                    if (response.Categorias.ContainsKey(marca.ParentGoogleId) && response.Categorias[marca.ParentGoogleId].Categoria  )
                        marca.Value.Categoria = response.Categorias[Parentid].Categoria;
                }
            }
        }

        private void BuscarMarcaOuCategNoGoogle()
        {
            IDictionary<string, IProdutoGoogle> atributos;

            try
            { 


                atributos = repository.BuscarPorGrupoMarcaCategoria<GoogleResponse, Produto>(request.GrupoId).Marcas;
                    response.Marcas = atributos;
                }
                else
                { 
                    atributos = repository.BuscarPorGrupoCategoria<GoogleResponse, Produto>(request.GrupoId);
                    response.Categorias = atributos;
                }

                stsCategoria.FlExecucao = atributos.ContainsKey(request.CategoriaNome);

                if (stsCategoria.FlExecucao)
                {
                    stsCategoria.Id = atributos[request.CategoriaNome].GoogleId;
                    stsCategoria.MsgExecucao = String.Format("{0} - {1} encontrada. GoogleId: {1}", request.Categorizacao, request.CategoriaNome, stsCategoria.Id);
                }
                else
                    stsCategoria.MsgExecucao = String.Format("{0} - {1} NÃO encontrada.", request.Categorizacao, request.CategoriaNome);

            }
            catch (Exception e)
            {
                stsCategoria.FlExecucao = false;
                stsCategoria.MsgExecucao = e.Message;
            }

        }
        */

    }

}
