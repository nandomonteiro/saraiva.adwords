﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{
    public class ClearingResponse : IClearingResponse
    {
        public string Acao { get; set; }
        public bool FlErro { get; set; }
        public string MsgErro { get; set; }
        public IEnumerable<IResponseSku> Skus { get; set; }

        public void AddSkus(IEnumerable<IProdutoGoogle> produtos)
        {
            if (produtos == null)
                return;

            var skus = new List<IResponseSku>();

            foreach (var item in produtos)
                skus.Add(new ResponseSku() { GoogleId = item.GoogleId, Sku = item.Sku, Cpc = item.Cpc, NewCpc = item.NewCpc });

            this.Skus = skus;
        }


    }
}