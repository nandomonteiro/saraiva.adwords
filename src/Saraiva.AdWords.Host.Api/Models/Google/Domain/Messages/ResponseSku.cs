﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Domain
{
    public class ResponseSku : IResponseSku
    {
        public string Sku { get; set; }
        public long GoogleId { get; set; }
        public long Cpc { get; set; }
        public long NewCpc { get; set; }
    }
}
