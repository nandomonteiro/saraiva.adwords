﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Saraiva.AdWords.Host.Domain
{
    public class ConfigRequest : IConfigRequest
    {
        public long AdGroupId { get; set; }
        public long GoogleId { get; set; }
    }

}
