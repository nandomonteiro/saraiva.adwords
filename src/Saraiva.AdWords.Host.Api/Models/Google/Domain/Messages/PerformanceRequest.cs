﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Saraiva.AdWords.Host.Domain
{
    public class PerformanceRequest
    {
        public long AdGroupId { get; set; }
        public string[] OfferIds { get; set; }
    }

}
