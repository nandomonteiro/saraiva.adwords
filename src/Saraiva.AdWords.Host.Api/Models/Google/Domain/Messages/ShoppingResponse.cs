﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{

    public class ShoppingResponse
    {
        public string OfferId { get; set; }
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public long AdGroupId { get; set; }
        public string AdGroupName { get; set; }
        public string Date { get; set; }
        public long Clicks { get; set; }
        public long Impressions { get; set; }
        public decimal Cost { get; set; }
        public double Conversions { get; set; }
        public decimal CostPerConversion { get; set; }
        public double ValuePerConversion { get; set; }
        public double ValuePerAllConversion { get; set; }
        public double ConversionValue { get; set; }
        public double AllConversionValue { get; set; }
    }

}
