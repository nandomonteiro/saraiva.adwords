﻿using System.Runtime.Serialization;

namespace Saraiva.AdWords.Host.Domain
{
    public class ClearingRequest : IClearingRequest
    {
        public long GrupoId { get; set; }
        public string[] Skus { get; set; }
    }

}
