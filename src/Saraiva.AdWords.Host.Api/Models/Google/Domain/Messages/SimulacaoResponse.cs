﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{
    public class SimulacaoResponse : ISimulacaoResponse
    {
        public long CriterionId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public long CampaignId { get; set; }
        public long AdGroupId { get; set; }
        public long Bid { get; set; }
        public long Clicks { get; set; }
        public long Cost { get; set; }
        public long Impressions { get; set; }
    }

}
