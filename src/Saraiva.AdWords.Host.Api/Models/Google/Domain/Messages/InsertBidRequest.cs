﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Saraiva.AdWords.Host.Domain
{
    public class InsertUpdateBidRequest : IInsertUpdateBidRequest
    {
        public string OfferId { get; set; }
        public long AdGroupId { get; set; }
        public long ParentId { get; set; }
        public long GoogleId { get; set; }
        public long CpcBid { get; set; }
    }

}
