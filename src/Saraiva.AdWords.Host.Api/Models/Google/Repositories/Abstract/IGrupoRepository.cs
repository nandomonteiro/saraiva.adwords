﻿using Saraiva.AdWords.Host.Domain;
using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Repositories
{
    public interface IGrupoRepository
    {
        IEnumerable<T> BuscarGruposAd<T>() where T : IGrupoGoogle, new();
        IEnumerable<T> BuscarGruposAd<T>(long id) where T : IGrupoGoogle, new();
        IEnumerable<T> BuscarGruposPorCampanha<T>(long CampanhaId) where T : IGrupoGoogle, new();
    }
}
