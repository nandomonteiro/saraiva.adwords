﻿using Saraiva.AdWords.Host.Domain;
using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Repositories
{
    public interface IProdutoRepository
    {
        IEnumerable<T> BuscarGrupoProdutos<T>(long grupoId) where T : IProdutoGoogle, new();
        void CarregarProdutoPorSku<T>(string sku, IGoogleResponse response) where T : IProdutoGoogle, new();
        void CarregarGrupoProduto<T>(IGoogleResponse response) where T : IProdutoGoogle, new();
        void CarregarMarcas<T>(IGoogleResponse response) where T : IProdutoGoogle, new();
        void CarregarCategorias<T>(IGoogleResponse response) where T : IProdutoGoogle, new();
        void CarregarMarcaCategorias<T>(IGoogleResponse response) where T : IProdutoGoogle, new();
        string AdicionarBids(IEnumerable<IInsertUpdateBidRequest> produtosGoogle);
        string ExcluirBids(IEnumerable<IConfigRequest> produtosGoogle);
        List<T> BuscarSimulacoes<T>(long groupId, long googleId) where T : ISimulacaoResponse, new();
        IEnumerable<T> CarregarDimensoes<T>() where T : IDimensaoResponse, new();

        T BuscarGrupoOfferId<T>(long grupoId, string offerId) where T : IProdutoGoogle, new();

        string ConfigurarCategorias(IConfigRequest request);
        //bool AtualizarBids(IEnumerable<IProdutoGoogle> produtosGoogle);

        //IDictionary<long, IProdutoGoogle> ObterSimulacoes(long groupId);
        //List<T> CarregarSimulacoes<T>(IGoogleResponse googleResponse) where T : ISimulacaoResponse, new();
    }
}
