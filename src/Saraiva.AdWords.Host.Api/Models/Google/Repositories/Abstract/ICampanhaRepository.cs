﻿using Saraiva.AdWords.Host.Domain;
using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Repositories
{
    public interface ICampanhaRepository
    {
        IEnumerable<T> BuscarCampanhas<T>(long id) where T : ICampanhaGoogle, new();
        IEnumerable<T> BuscarCampanhas<T>() where T : ICampanhaGoogle, new();
        IEnumerable<T> BuscarCampanhasPorLike<T>(string descricao) where T : ICampanhaGoogle, new();
    }
}
