﻿using Google.Api.Ads.AdWords.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Repositories
{
    public abstract class BaseRepository
    {
        private AdWordsUser user;

        public AdWordsUser User
        {
            get
            {
                if (user == null)
                    user = new AdWordsUser();

                return user;
            }
        }

    }
}
