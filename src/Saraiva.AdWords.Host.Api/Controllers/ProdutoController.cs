﻿using Saraiva.AdWords.Host.Domain;
using Saraiva.AdWords.Host.Repositories;
using System;
using System.Web.Http;
using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Api.Controllers
{
    [RoutePrefix("api/Produto")]
    public class ProdutoController : ApiController
    {
        [Route("Grupo/{grupoId:long}"), HttpGet]
        public IHttpActionResult GetPorGrupo(long grupoId)
        {
            try
            {
                var repository = FactoryRepository.Produto;

                return Ok(repository.BuscarGrupoProdutos<Produto>(grupoId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("{grupoId:long}/{offerId:int}"), HttpGet]
        public IHttpActionResult GetPorGrupoSku(long grupoId, int offerId)
        {
            try
            {
                var repository = FactoryRepository.Produto;

                return Ok(repository.BuscarGrupoOfferId<Produto>(grupoId, offerId.ToString()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [Route("Dimensoes"), HttpGet]
        public IHttpActionResult GetDimensoes()
        {
            try
            {
                var repository = FactoryRepository.Produto;

                return Ok(repository.CarregarDimensoes<DimensaoResponse>());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("Simulacoes/{grupoId:long}/{sku:int}"), HttpGet]
        public IHttpActionResult ObterSimulacoes(long grupoId, long googleId)
        {
            try
            {
                var repository = FactoryRepository.Produto;

                return Ok(repository.BuscarSimulacoes<SimulacaoResponse>(grupoId, googleId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /*[Route("Update"), HttpPost]
        public IHttpActionResult Update([FromBody]UpdateRequest request)
        {
            try
            {
                var bid = new Bid(FactoryRepository.Produto, FactoryRepository.Relatorios);

                return Ok(bid.Update(request));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }*/

        [Route("Bid"), HttpPost]
        public IHttpActionResult InsertUpdateBid([FromBody]InsertUpdateBidRequest request)
        {
            try
            {
                var repository = FactoryRepository.Produto;

                return Ok(repository.AdicionarBid(request));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("DeleteBid"), HttpPost]
        public IHttpActionResult DeleteBid([FromBody]ConfigRequest request)
        {
            try
            {
                var repository = FactoryRepository.Produto;

                return Ok(repository.ExcluirBid(request));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("Config"), HttpPost]
        public IHttpActionResult ConvertToSubdivision([FromBody]ConfigRequest request)
        {
            try
            {
                var repository = FactoryRepository.Produto;

                return Ok(repository.ConfigurarCategorias(request));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //ExcluirBids(IEnumerable<IDeleteBidRequest> produtosGoogle)
    }
}
