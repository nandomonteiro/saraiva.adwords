﻿using Saraiva.AdWords.Host.Domain;
using Saraiva.AdWords.Host.Repositories;
using System;
using System.Web.Http;

namespace Saraiva.AdWords.Host.Api.Controllers
{
    [RoutePrefix("api/Grupo")]
    public class GrupoController : ApiController
    {
        public IHttpActionResult Get()
        {
            try
            {
                var repository = FactoryRepository.Grupo;

                return Ok(repository.BuscarGruposAd<Grupo>());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("{id:long}"), HttpGet]
        public IHttpActionResult GetId(long id)
        {
            try
            {
                var repository = FactoryRepository.Grupo;

                return Ok(repository.BuscarGruposAd<Grupo>(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("Campanha/{CampanhaId:long}")]
        public IHttpActionResult Get(long CampanhaId)
        {
            try
            {
                var repository = FactoryRepository.Grupo;

                return Ok(repository.BuscarGruposPorCampanha<Grupo>(CampanhaId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
