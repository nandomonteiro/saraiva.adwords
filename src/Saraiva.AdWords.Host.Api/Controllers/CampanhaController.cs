﻿using Saraiva.AdWords.Host.Domain;
using Saraiva.AdWords.Host.Repositories;
using System;
using System.Web.Http;

namespace Saraiva.AdWords.Host.Api.Controllers
{
    [RoutePrefix("api/Campanha")]
    public class CampanhaController : ApiController
    {
        public IHttpActionResult Get()
        {
            try
            {
                var repository = FactoryRepository.Campanha;

                return Ok(repository.BuscarCampanhas<Campanha>());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("{id:long}"), HttpGet]
        public IHttpActionResult GetId(long id)
        {
            try
            {
                var repository = FactoryRepository.Campanha;

                return Ok(repository.BuscarCampanhas<Campanha>(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("{Automaticas}"), HttpGet]
        public IHttpActionResult GetAutomatica()
        {
            try
            {
                var repository = FactoryRepository.Campanha;

                return Ok(repository.BuscarCampanhasPorLike<Campanha>("[AUT]"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
