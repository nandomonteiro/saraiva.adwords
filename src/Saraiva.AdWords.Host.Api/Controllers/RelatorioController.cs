﻿using Saraiva.AdWords.Host.Domain;
using Saraiva.AdWords.Host.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Saraiva.AdWords.Host.Api.Controllers
{
    [RoutePrefix("api/Relatorio")]
    public class RelatorioController : ApiController
    {

        [Route("Produto/{grupoId:long}/{id:long}"), HttpGet]
        public IHttpActionResult GetRelPorGrupoProduto(long grupoId, long id)
        {
            try
            {
                var repository = FactoryRepository.Relatorios;

                return Ok(repository.RelProdutosPorGrupoProduto<Produto>(grupoId, id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("Produto/Grupo/{grupoId:long}"), HttpGet]
        public IHttpActionResult GetRelPorGrupo(long grupoId)
        {
            try
            {
                var repository = FactoryRepository.Relatorios;

                return Ok(repository.RelProdutosPorGrupo<Produto>(grupoId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("RelShoppingToday/{adGroupId:long}"), HttpGet]
        public IHttpActionResult RelShoppingToday(long adGroupId)
        {
            try
            {
                var repository = FactoryRepository.Relatorios;

                return Ok(repository.RelShopping(adGroupId, "TODAY"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("ShoppingLastWeek/{adGroupId:long}"), HttpGet]
        public IHttpActionResult RelShoppingLastWeek(long adGroupId)
        {
            try
            {
                var repository = FactoryRepository.Relatorios;

                return Ok(repository.RelShopping(adGroupId, "LAST_7_DAYS"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("ShoppingLastWeekExcel"), HttpGet]
        public HttpResponseMessage ShoppingLastWeekExcel()
        {
            var repository = FactoryRepository.Relatorios;

            var campanhas = FactoryRepository.Campanha;

            var dadosCampanha = campanhas.BuscarCampanhasPorLike<Campanha>("[AUT]");

            var dados = new List<ShoppingResponse>();

            foreach (var campanha in dadosCampanha)
            {
                dados.AddRange(repository.RelShoppingCampanhaDetalhada(campanha.Id, "LAST_7_DAYS"));
            }

            foreach (var item in dados)
            {
                item.Cost = item.Cost / 1000000;
                item.Conversions = item.Conversions / 100;
                item.CostPerConversion = item.CostPerConversion / 1000000;
                item.ValuePerConversion = item.ValuePerConversion / 100;
                item.ConversionValue = item.ConversionValue / 100;
                item.AllConversionValue = item.AllConversionValue / 100;
                item.ValuePerAllConversion = item.ValuePerAllConversion / 100;
                item.ValuePerConversion = item.ValuePerConversion / 100;
            }

            var excel = Transformador.Executar(";", dados);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(excel, Encoding.ASCII);

            return response;
        }

        [Route("OutrasLastWeekExcel"), HttpGet]
        public HttpResponseMessage OutrasLastWeekExcel()
        {
            var repository = FactoryRepository.Relatorios;

            var campanhas = FactoryRepository.Campanha;

            var dadosCampanha = campanhas.BuscarCampanhasNotLike<Campanha>("[AUT]");

            var dados = new List<ShoppingResponse>();

            foreach (var campanha in dadosCampanha)
            {
                dados.AddRange(repository.RelShoppingCampanha(campanha.Id, "LAST_7_DAYS"));
            }

            foreach (var item in dados)
            {
                item.Cost = item.Cost / 1000000;
                item.Conversions = item.Conversions / 100;
                item.CostPerConversion = item.CostPerConversion / 1000000;
                item.ValuePerConversion = item.ValuePerConversion / 100;
                item.ConversionValue = item.ConversionValue / 100;
                item.AllConversionValue = item.AllConversionValue / 100;
                item.ValuePerAllConversion = item.ValuePerAllConversion / 100;
                item.ValuePerConversion = item.ValuePerConversion / 100;
            }

            var excel = Transformador.Executar(";", dados);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(excel, Encoding.ASCII);

            return response;
        }

        [Route("PerformanceLastWeek"), HttpPost]
        public IHttpActionResult PerformanceLastWeek([FromBody]ShoppingRequest offerIds)
        {
            try
            {
                var relatorio = FactoryRepository.Relatorios;

                string[] offers = offerIds.skus.Select(n => Convert.ToString(n)).ToArray();

                return Ok(relatorio.CarregarPerformances(offers, offerIds.Time, offerIds.Interval));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("PerformanceALLTime"), HttpPost]
        public IHttpActionResult PerformanceALLTime([FromBody]int[] skus)
        {
            try
            {
                var relatorio = FactoryRepository.Relatorios;

                string[] offers = skus.Select(n => Convert.ToString(n)).ToArray();

                return Ok(relatorio.CarregarPerformances(offers, null, null));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}
