﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Services.Mensageria.Configuracao
{
    public class Hosts
    {
        public string awsSQSHost { get; set; }

        public string awsRegion { get; set; }

        public Amazon.RegionEndpoint RegionEndpoint
        {
            get
            {
                if (string.IsNullOrEmpty(awsRegion))
                    throw new Exception("awsRegion não encontrado no arquivo de configuração.");
                else
                    return Amazon.RegionEndpoint.GetBySystemName(awsRegion);
            }
        }

        public int MaxNumberOfMessages { get; set; }

        public string awsSQSHostPedidoPago { get; set; }

        public string ClientHost { get; set; }
    }
}
