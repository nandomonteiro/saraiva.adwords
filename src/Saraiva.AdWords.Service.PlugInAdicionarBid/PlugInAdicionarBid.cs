﻿using Microsoft.SqlServer.Server;
using Saraiva.AdWords.Services.Interfaces;
using Saraiva.AdWords.Services.Plugins;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using System.Linq;

namespace Saraiva.AdWords.Service.PlugInAdicionarBid
{
    public class PlugInAdicionarBid : PlugInBaseBid
    {

        public PlugInAdicionarBid(string acao, string mensagem) : base(acao, mensagem)
        {
            Acoes.Add("Insert", ActionInsertAut);
            Acoes.Add("InsertM", ActionInsertManual);
            Acoes.Add("Delete", Delete);
            Acoes.Add("CargaDimensoes", ActionLoadDimensoes);
            Acoes.Add("CargaProduto", ActionLoadProdutoCategorias);
        }

        public PlugInAdicionarBid(string acao, string mensagem, IAPIService service, IJsonService json) : this(acao, mensagem)
        {
            Service = service;
            Json = json;
        }

        public PlugInAdicionarBid(string acao, string mensagem, IAPIService service, IJsonService json, int idtransaction) : this(acao, mensagem, service, json)
        {
            IdTransaction = idtransaction;
        }

        protected IEnumerable<InsertUpdateDTO> ObterDTOs(AdicionarBidRequest[] skus)
        {
            foreach (var sku in skus)
               yield return new InsertUpdateDTO() { Sku = sku.sku, Itens = sku.itens, Qtde = sku.qtde, FlValido = true };
        }

        protected IEnumerable<InsertUpdateDTO> ObterDTOs(int[] skus)
        {
            foreach (var sku in skus)
                yield return new InsertUpdateDTO() { Sku = sku, FlValido = true };
        }

        private void ActionInsertAut(string mensagem)
        {
            AdicionarBidRequest[] skus = null;

            try
            {
                skus = Json.Deserialize<AdicionarBidRequest[]>(mensagem);
                Insert(skus, true);
            }
            catch (Exception e)
            {
                UpdateTransactionError(e.Message);
                return;
            }
        }

        private void ActionInsertManual(string mensagem)
        {
            var skusRequest = new List<AdicionarBidRequest>();

            try
            {
                var skus = Json.Deserialize<int[]>(mensagem);

                foreach (var sku in skus)
                    skusRequest.Add(new AdicionarBidRequest() { sku = sku });

                Insert(skusRequest.ToArray(), false);
            }
            catch (Exception e)
            {
                UpdateTransactionError(e.Message);
                return;
            }
        }

        private void Insert(AdicionarBidRequest[] skus, bool validaQtde)
        {
            try
            {
                if (ExistsStatus(Acao, StatusTransaction.EM_EXECUCAO))
                {
                    UpdateTransaction(false, "Ja existe tarefa em andamento", StatusTransaction.AGUARDANDO_EXECUCAO);
                    return;
                }

                UpdateTransactionStatus("Baixando dados das campanhas automaticas");

                BaixarCampanhasAutomaticas();

                UpdateTransactionStatus("Validando skus elegiveis para bid");

                var skusAlvo = InsertTransactionProcess(ObterDTOs(skus), validaQtde);

                if (skusAlvo == null || skusAlvo.Count() == 0)
                {
                    UpdateTransaction(false, "Nao houve sku elegivel para bid", StatusTransaction.CONCLUIDO_SEM_ATIVIDADE);
                    return;
                }

                if (skusAlvo.Count(x => x.Fltratamento) > 0)
                {
                    UpdateTransactionStatus("Configurando categorias encontradas");

                    skusAlvo = ConfigurarCategorias(skusAlvo);

                    if (skusAlvo.Count(x=>x.FlValido) == 0)
                    {
                        UpdateTransactionError("Problemas ao configurar categorias");
                        UpdateTransactionProcess(skusAlvo);
                        return;
                    }
                }

                UpdateTransactionStatus("Baixando performances");

                var performance = BaixarPerformance(skusAlvo);

                PersistirPerformances(performance);

                UpdateTransactionStatus("Obtendo skus elegiveis para bid");

                var insertsDTOs = CarregarInsertUpdate(IdTransaction) ?? new List<InsertUpdateDTO>();

                var totalAIncluir = insertsDTOs.Count();

                if (totalAIncluir == 0)
                {
                    UpdateTransaction(false, "Nao encontrado SKUs eleitos para bid", StatusTransaction.CONCLUIDO_SEM_ATIVIDADE);
                    return;
                }

                insertsDTOs = ActionInsertBid(insertsDTOs);

                var totalEnviado = skus.Length;
                var totalSucesso = insertsDTOs.Count(x => x.FlValido);
                var lsucesso = totalSucesso == totalAIncluir;
                var lresultado = string.Empty;
                var lstatus = string.Empty;

                if (totalSucesso == 0)
                {
                    lresultado = "Nenhum bid foi efetuado";
                    lstatus = StatusTransaction.CONCLUIDO_SEM_ATIVIDADE;
                }
                else
                    if (!lsucesso)
                    {
                        lresultado = "Alguns bids nao foram atualizados";
                        lstatus = StatusTransaction.CONCLUIDO_COM_RESSALVAS;
                    }
                    else
                        {
                            lresultado = "Bids alterados com sucesso";
                            lstatus = StatusTransaction.CONCLUIDO_COM_SUCESSO;
                        };

                UpdateTransaction(!lsucesso, lresultado, lstatus);
                UpdateTransactionProcess(insertsDTOs);
            }
            catch (Exception e)
            {
                UpdateTransactionError(string.Format("{0}: {1}", e.Message, e.InnerException));
            }
        }

        private void Delete(string mensagem)
        {
            try
            {
                if (ExistsStatus(Acao, StatusTransaction.EM_EXECUCAO))
                {
                    UpdateTransaction(false, "Ja existe tarefa em andamento", StatusTransaction.AGUARDANDO_EXECUCAO);
                    return;
                }

                UpdateTransactionStatus("Baixando dados das campanhas automaticas");

                BaixarCampanhasAutomaticas();

                var skusAlvo = DeleteTransactionProcess();

                if (skusAlvo == null || skusAlvo.Count() == 0)
                {
                    UpdateTransaction(false, "Nao houve sku elegivel para exclusao do bid", StatusTransaction.CONCLUIDO_SEM_ATIVIDADE);
                    return;
                }

                UpdateTransactionStatus("Baixando performances");

                var performance = BaixarPerformance(skusAlvo);

                PersistirPerformances(performance);

                UpdateTransactionStatus("Obtendo skus elegiveis para exclusao do bid");

                var deleteDTOs = CarregarDelete(IdTransaction) ?? new List<DeleteDTO>();

                var totalAExcluir = deleteDTOs.Count();

                if (totalAExcluir == 0)
                {
                    UpdateTransaction(false, "Nao houve sku elegivel para exclusao de bid", StatusTransaction.CONCLUIDO_SEM_ATIVIDADE);
                    return;
                }

                deleteDTOs = ActionExcluirBid(deleteDTOs);

                var totalSucesso = deleteDTOs.Count(x => x.FlValido);
                var lsucesso = totalSucesso == totalAExcluir;
                var lresultado = string.Empty;
                var lstatus = string.Empty;

                if (totalSucesso == 0)
                {
                    lresultado = "Nenhuma exclusão de bid foi efetuado";
                    lstatus = StatusTransaction.CONCLUIDO_SEM_ATIVIDADE;
                }
                else
                    if (!lsucesso)
                {
                    lresultado = "Alguns bids nao foram excluidos";
                    lstatus = StatusTransaction.CONCLUIDO_COM_RESSALVAS;
                }
                else
                {
                    lresultado = "Bids excluidos com sucesso";
                    lstatus = StatusTransaction.CONCLUIDO_COM_SUCESSO;
                };

                UpdateTransaction(!lsucesso, lresultado, lstatus);
                UpdateTransactionProcess(deleteDTOs);
            }
            catch (Exception e)
            {
                UpdateTransactionError(string.Format("{0}: {1}", e.Message, e.InnerException));
            }
        }

        private object BaixarEstatistica(IEnumerable<DeleteDTO> skusAlvo)
        {
            throw new NotImplementedException();
        }

        private void PersistirPerformances(List<PerformanceDTO> performances)
        {
            foreach (var performance in performances)
            {
                if (performance.Estatisticas != null && performance.Estatisticas.Count > 0)
                    PersistirEstatisticas(performance.Estatisticas);

                if (performance.Simulacoes != null && performance.Simulacoes.Count > 0)
                    PersistirSimulacoes(performance.Simulacoes);
            }
        }

        private List<PerformanceDTO> BaixarPerformance(IEnumerable<InsertUpdateDTO> skusAlvo)
        {
            var result = new List<PerformanceDTO>();

            var request = new ShoppingRequest();
            var response = new List<PerformanceResponse>();

            foreach (var sku in skusAlvo)
            { 

                request.Interval = "LAST_7_DAYS";
                request.Time = null;
                request.skus = new int[] { sku.Sku };

                
                string action = "Relatorio/PerformanceLastWeek";
                try
                {
                    var r = Task.Run(() => Service.ExecutaAcao<List<PerformanceResponse>>(action, request));
                    r.Wait();
                    response.AddRange(r.Result);
                }
                catch (Exception e)
                {
                    continue;
                }
            }

            List<PerformanceEstatisticaDTO> estatisticas = null;
            List<PerformanceSimulacaoDTO> simulacoes = null;

            foreach (var item in response)
            {
                if (item.Estatisticas != null)
                {
                    estatisticas = new List<PerformanceEstatisticaDTO>();

                    foreach (var e in item.Estatisticas)
                    {
                        estatisticas.Add(new PerformanceEstatisticaDTO() {
                            CampaignId = item.CampaignId,
                            CampaignName = item.CampaignName,
                            AdGroupId = item.AdGroupId,
                            AdGroupName = item.AdGroupName,
                            Id = item.Id,
                            OfferId = item.OfferId,
                            Date = e.Date,
                            Clicks = e.Clicks,
                            Cost = e.Cost,
                            Conversions = e.Conversions,
                            CostPerConversion = e.CostPerConversion,
                            ValuePerConversion = e.ValuePerConversion,
                            ValuePerAllConversion = e.ValuePerAllConversion,
                            AllConversionValue = e.AllConversionValue,
                            ConversionValue = e.ConversionValue
                        });
                    }
                }

                if (item.Simulacoes != null)
                { 
                    simulacoes = new List<PerformanceSimulacaoDTO>();

                    foreach (var s in item.Simulacoes)
                    {
                        simulacoes.Add(new PerformanceSimulacaoDTO()
                        {
                            CampaignId = item.CampaignId,
                            CampaignName = item.CampaignName,
                            AdGroupId = item.AdGroupId,
                            AdGroupName = item.AdGroupName,
                            Id = item.Id,
                            OfferId = item.OfferId,
                            StartDate = s.StartDate,
                            EndDate = s.EndDate,
                            Clicks = s.Clicks,
                            Cost = s.Cost,
                            Impressions = s.Impressions,
                            Bid = s.Bid}
                        );
                    }
                }

                result.Add(new PerformanceDTO() { Estatisticas = estatisticas, Simulacoes = simulacoes });
            }

            return result;

        }

        private List<PerformanceDTO> BaixarPerformance(Dictionary<string, DeleteDTO> skusAlvo)
        {
            var result = new List<PerformanceDTO>();

            var response = new List<ShoppingResponse>();

            foreach (var grupoId in skusAlvo.Values.Select(x => x.GrupoId).Distinct().ToArray())
            {
                string action = string.Format("Relatorio/RelShoppingToday/{0}", grupoId);

                try
                {
                    var r = Task.Run(() => Service.ExecutaAcao<List<ShoppingResponse>>(action));
                    r.Wait();
                    response.AddRange(r.Result);
                }
                catch (Exception e)
                {
                    continue;
                }
            }

            var estatisticas = new List<PerformanceEstatisticaDTO>();

            foreach (var item in response)
            {
                if (!skusAlvo.ContainsKey(item.OfferId))
                    continue;

                estatisticas.Add(new PerformanceEstatisticaDTO()
                {
                    CampaignId = item.CampaignId,
                    CampaignName = item.CampaignName,
                    AdGroupId = item.AdGroupId,
                    AdGroupName = item.AdGroupName,
                    Id = skusAlvo[item.OfferId].GoogleId,
                    OfferId = item.OfferId,
                    Date = item.Date,
                    Clicks = item.Clicks,
                    Cost = item.Cost,
                    Conversions = item.Conversions,
                    CostPerConversion = item.CostPerConversion,
                    ValuePerConversion = item.ValuePerConversion,
                    ValuePerAllConversion = item.ValuePerAllConversion,
                    AllConversionValue = item.AllConversionValue,
                    ConversionValue = item.ConversionValue
                });
            }

            result.Add(new PerformanceDTO() { Estatisticas = estatisticas });

            return result;

        }

        private async void ActionLoadDimensoes(string mensagem)
        {
            var dimensoes = await GetDimensoes();

            await Task.Run(() => PersistirDimensoes(dimensoes));
        }

        private async void ActionLoadProdutoCategorias(string mensagem)
        {
            foreach (var strGrupo in mensagem.Split(','))
            {
                var idGrupo = Convert.ToInt64(strGrupo);

                var produtoCategorias = await GetProdutoCategorias(idGrupo);

                await Task.Run(() => AtualizarProduto(idGrupo, produtoCategorias.Where(x => !x.IsCategoria).ToList()));

                await Task.Run(() => AtualizarCategoria(idGrupo, produtoCategorias.Where(x => x.IsCategoria).ToList()));

                Console.WriteLine("Grupo {0} atualizado.", idGrupo);
            }
        }

        public async Task<List<DimensaoResponse>> GetDimensoes()
        {
            List<DimensaoResponse> dimensoes = null;

            try
            {
                dimensoes = await Service.ExecutaAcao<List<DimensaoResponse>>("Produto/Dimensoes");

                return dimensoes;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao obter dimensoes: {0}", ex.Message);
                return null;
            }
        }


        public async Task<List<CategoriaProdutoResponse>> GetProdutoCategorias(long grupoId)
        {
            List<CategoriaProdutoResponse> categorias = null;

            try
            {
                categorias = await Service.ExecutaAcao<List<CategoriaProdutoResponse>>(string.Format("Produto/Grupo/{0}", grupoId));

                return categorias;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao obter categorias: {0}", ex.Message);
                return null;
            }
        }


        private void ActionExcluirBid(string mensagem)
        {
            /*var skusAlvo = InsertTransactionProcess(mensagem.Split(','), Evento.Exclusao);

            if (skusAlvo.Count() == 0)
                return;

            await ActionExcluirBid(skusAlvo);*/
        }

        private IEnumerable<InsertUpdateDTO> ConfigurarCategorias(IEnumerable<InsertUpdateDTO> skusAlvo)
        {
            List<InsertUpdateDTO> result = new List<InsertUpdateDTO>();

            try
            {
                foreach (var sku in skusAlvo )
                {
                    if (sku.Fltratamento)
                    { 
                        var request = new ConfigRequest() { AdGroupId = sku.GrupoId, GoogleId = sku.CategoriaId };

                        var response = Task.Run<string>(() => Service.ExecutaAcao<string>("Produto/Config", request));

                        if (response.Result != "OK")
                        {
                            sku.FlValido = false;
                            sku.MsgErro = string.Format("Erro ao tentar configurar categoria: {0}", response.Result);
                        }
                    }

                    result.Add(sku);
                }

                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao configurar skus: " + e.Message);
            }
        }

        private IEnumerable<InsertUpdateDTO> ActionInsertBid(IEnumerable<InsertUpdateDTO> list)
        {
            var lst = new List<InsertUpdateDTO>();
            
            long lgoogleId = 0;
            try
            { 
                foreach (var item in list)
                {
                    try
                    {
                        var request = new InsertUpdateRequest()
                        {
                            OfferId = Convert.ToString(item.Sku),
                            AdGroupId = item.GrupoId,
                            GoogleId = 0,
                            ParentId = item.CategoriaId,
                            CpcBid = Convert.ToInt64(item.VlCpc * 1000000)
                        };

                        var response = Task.Run(() => Service.ExecutaAcao<string>("Produto/Bid", request));

                        response.Wait();

                        item.FlValido = long.TryParse(response.Result, out lgoogleId);

                        if (item.FlValido)
                            item.GoogleId = lgoogleId;
                        else
                            item.MsgErro = response.Result;
                    }
                    catch (Exception e)
                    {
                        item.FlValido = false;
                        item.MsgErro = e.Message;
                    }
                    lst.Add(item);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro na tentativa de incluir bids", e);
            }
            return lst;
        }

        private IEnumerable<DeleteDTO> ActionExcluirBid(IEnumerable<DeleteDTO> list)
        {
            var lst = new List<DeleteDTO>();

            long lgoogleId = 0;
            try
            {
                foreach (var item in list)
                {
                    try
                    {
                        var request = new DeleteBidRequest()
                        {
                            AdGroupId = item.GrupoId,
                            GoogleId = item.GoogleId
                        };

                        var response = Task.Run(() => Service.ExecutaAcao<string>("Produto/DeleteBid", request));

                        response.Wait();

                        item.FlValido = long.TryParse(response.Result, out lgoogleId);

                        if (item.FlValido)
                            item.GoogleId = lgoogleId;
                        else
                            item.MsgErro = response.Result;
                    }
                    catch (Exception e)
                    {
                        item.FlValido = false;
                        item.MsgErro = e.Message;
                    }

                    lst.Add(item);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro na tentativa de excluir bids", e);
            }
            return lst;
        }

        private IEnumerable<InsertUpdateDTO> InsertTransactionProcess(IEnumerable<InsertUpdateDTO> skus, bool isValidaQtde)
        {
            var result = new List<InsertUpdateDTO>();

            try
            {
                using (SqlConnection conn = new SqlConnection(DBINTERNET))
                {
                    string sql = @"EXECUTE [dbo].[STP_GoogleAdWordsClearInsert] @IdTransaction, @pValidaQuantidade, @pStsAplicacao, @SkuList";

                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Parameters.Add("@IdTransaction", SqlDbType.Int).Value = IdTransaction;
                    cmd.Parameters.Add("@pValidaQuantidade", SqlDbType.Int).Value = isValidaQtde;
                    cmd.Parameters.Add("@pStsAplicacao", SqlDbType.Bit).Value = true;

                    var record = CreateSkuList(skus);

                    var paramTable = cmd.Parameters.AddWithValue("@SkuList", record);
                    paramTable.SqlDbType = SqlDbType.Structured;
                    paramTable.TypeName = "dbo.SkuList";

                    var dr = cmd.ExecuteReader();

                    if (!dr.HasRows)
                        return result;

                    while (dr.Read())
                    {
                        result.Add(new InsertUpdateDTO()
                        {
                            Id = dr.GetInt32(0),
                            Sku = dr.GetInt32(1),
                            CampanhaId = dr.GetInt64(2),
                            GrupoId = dr.GetInt64(3),
                            CategoriaId = dr.GetInt64(4),
                            Fltratamento = dr.GetBoolean(5),
                            FlValido = true
                        });
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao executar clearing de SKUs [STP_GoogleAdWordsClearInsert]", e);
            }

            return result;
        }

        private Dictionary<string, DeleteDTO> DeleteTransactionProcess()
        {
            var result = new Dictionary<string, DeleteDTO>();

            try
            {
                using (SqlConnection conn = new SqlConnection(DBINTERNET))
                {
                    string sql = @"EXECUTE [dbo].[STP_GoogleAdWordsClearDelete] @IdTransaction, @pStsAplicacao";

                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Parameters.Add("@IdTransaction", SqlDbType.Int).Value = IdTransaction;
                    cmd.Parameters.Add("@pStsAplicacao", SqlDbType.Bit).Value = true;

                    var dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        result.Add(Convert.ToString(dr.GetInt32(0)), new DeleteDTO()
                        {
                            Id = dr.GetInt32(1),
                            GoogleId = dr.GetInt64(2),
                            GrupoId = dr.GetInt64(3),
                            FlValido = true
                        });
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao executar clearing de SKUs [STP_GoogleAdWordsClearDelete]", e);
            }

            return result;
        }

        public IEnumerable<DeleteDTO> CarregarDelete(long idTransaction)
        {
            using (SqlConnection conn = new SqlConnection(DBINTERNET))
            {
                string sql = @"EXECUTE [dbo].[STP_GoogleAdWordsDelete] @IdTransaction";

                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.CommandTimeout = int.MaxValue;

                cmd.Parameters.AddWithValue("@IdTransaction", idTransaction);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var item = new DeleteDTO()
                    {
                        Id = reader.GetInt32(0),
                        GoogleId = reader.GetInt64(1),
                        GrupoId = reader.GetInt64(2)
                    };

                    yield return item;
                }
            }

        }

        private void UpdateTransactionProcess(IEnumerable<ICrudDTO> skus)
        {
            var result = new List<SimulacaoRequest>();

            SqlCommand cmd = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(DBINTERNET))
                {
                    string sql = @" UPDATE GoogleAdwordsTransactionProcess 
                                SET GTP_GAP_Id = @GoogleId,
	                                GTP_Status = @Status,
                                    GTP_Mensagem = @MsgErro
                                WHERE GTP_GAN_Id = @idtransaction and GTP_Id = @Id";

                    conn.Open();

                    cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.Add("@idtransaction", SqlDbType.Int);
                    cmd.Parameters.Add("@GoogleId", SqlDbType.BigInt);
                    cmd.Parameters.Add("@Status", SqlDbType.Char, 1);
                    cmd.Parameters.Add("@Id", SqlDbType.Int);
                    cmd.Parameters.Add("@MsgErro", SqlDbType.VarChar, 255);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Prepare();

                    foreach (var sku in skus)
                    {
                        cmd.Parameters["@idtransaction"].Value = IdTransaction;
                        cmd.Parameters["@GoogleId"].Value = sku.GoogleId;
                        cmd.Parameters["@Status"].Value = sku.FlValido ? "S" : "N";
                        cmd.Parameters["@Id"].Value = sku.Id;
                        cmd.Parameters["@MsgErro"].Value = sku.FlValido ? "Realizado" : sku.MsgErro ?? "Não realizado";
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao atualizar processo [GoogleAdwordsTransactionProcess]", e);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public IEnumerable<InsertUpdateDTO> CarregarInsertUpdate(long idTransaction)
        {
            var result = new List<InsertUpdateDTO>();

            SqlCommand cmd = null;
            try
            { 
                using (SqlConnection conn = new SqlConnection(DBINTERNET))
                {
                    string sql = @"EXECUTE [dbo].[STP_GoogleAdWordsInsert] @IdTransaction";

                    conn.Open();

                    cmd = new SqlCommand(sql, conn);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Parameters.AddWithValue("@IdTransaction", idTransaction);

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var item = new InsertUpdateDTO()
                        {
                            Id = reader.GetInt32(0),
                            Sku = reader.GetInt32(1),
                            CampanhaId = reader.GetInt64(2),
                            GrupoId = reader.GetInt64(3),
                            CategoriaId = reader.GetInt64(4),
                            VlCpc = reader.GetDecimal(5)
                        };

                        result.Add(item);
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao obter skus para inclusao [STP_GoogleAdWordsInsert]", e);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        private void ActionUpdateBid(List<InsertUpdateDTO> list)
        {
            if (list == null || list.Count == 0)
                return;
        }

        private void BaixarCampanhasReferenciais(IEnumerable<GrupoDTO> gruposTarget)
        {
            foreach (var g in gruposTarget)
            {
                BaixarProdutoCategorias(g.CampanhaId, g.Id);
            }
        }

    private void PersistirSimulacoes(List<PerformanceSimulacaoDTO> simulacoes)
    {
        if (simulacoes.Count == 0)
            return;

        SqlCommand cmd = null;

        try
        {
            using (var conn = new SqlConnection(DBINTERNET))
            {
                conn.Open();

                string sql = @" INSERT INTO dbo.GoogleAdwordsSimulacao (GAS_GAN_Id, GAS_GAC_Id, GAS_GAG_Id, GAS_GAP_Id, GAS_DtInicio, GAS_DtFim ,GAS_VlBid, GAS_Clicks, GAS_Impressoes, GAS_VlCusto, GAS_PRO_Id)
                                VALUES (@IdTransaction, @GAS_GAC_Id, @GAS_GAG_Id, @GAS_GAP_Id, @GAS_DtInicio, @GAS_DtFim, @GAS_VlBid, @GAS_Clicks, @GAS_Impressoes, @GAS_VlCusto, @GAS_PRO_Id)";

                cmd = new SqlCommand(sql, conn);

                cmd.Parameters.Add("@IdTransaction", SqlDbType.Int);
                cmd.Parameters.Add("@GAS_GAC_Id", SqlDbType.BigInt);
                cmd.Parameters.Add("@GAS_GAG_Id", SqlDbType.BigInt);
                cmd.Parameters.Add("@GAS_GAP_Id", SqlDbType.BigInt);
                cmd.Parameters.Add("@GAS_DtInicio", SqlDbType.Date);
                cmd.Parameters.Add("@GAS_DtFim", SqlDbType.Date);
                cmd.Parameters.Add("@GAS_VlBid", SqlDbType.Money);
                cmd.Parameters.Add("@GAS_Clicks", SqlDbType.BigInt);
                cmd.Parameters.Add("@GAS_Impressoes", SqlDbType.BigInt);
                cmd.Parameters.Add("@GAS_VlCusto", SqlDbType.Money);
                cmd.Parameters.Add("@GAS_PRO_Id", SqlDbType.Int);

                cmd.CommandTimeout = int.MaxValue;

                cmd.Prepare();

                foreach (var simulacao in simulacoes)
                {
                    cmd.Parameters["@IdTransaction"].Value = IdTransaction;
                    cmd.Parameters["@GAS_GAC_Id"].Value = simulacao.CampaignId;
                    cmd.Parameters["@GAS_GAG_Id"].Value = simulacao.AdGroupId;
                    cmd.Parameters["@GAS_GAP_Id"].Value = simulacao.Id;
                    cmd.Parameters["@GAS_DtInicio"].Value = DateTime.ParseExact(simulacao.StartDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                    cmd.Parameters["@GAS_DtFim"].Value = DateTime.ParseExact(simulacao.EndDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                    cmd.Parameters["@GAS_VlBid"].Value = Convert.ToDecimal(simulacao.Bid) / 1000000;
                    cmd.Parameters["@GAS_Clicks"].Value = simulacao.Clicks;
                    cmd.Parameters["@GAS_Impressoes"].Value = simulacao.Impressions;
                    cmd.Parameters["@GAS_VlCusto"].Value = Convert.ToDecimal(simulacao.Cost) / 1000000;
                    cmd.Parameters["@GAS_PRO_Id"].Value = Convert.ToInt32(simulacao.OfferId);

                    cmd.ExecuteNonQuery();
                }

            }
        }
        catch (Exception e)
        {
            throw new Exception("Erro ao persistir simulacoes [GoogleAdwordsSimulacao]", e);
        }
        finally
        {
            if (cmd != null)
                cmd.Dispose();
        }
    }

    private void PersistirDimensoes(List<DimensaoResponse> dimensoes)
        {
            if (dimensoes.Count == 0)
                return;

            SqlCommand cmd = null;

            try
            {
                using (var conn = new SqlConnection(DBINTERNET))
                {
                    conn.Open();

                    string sql = @" INSERT INTO dbo.GoogleAdwordsDimensao (GAD_Id, GAD_Descricao, GAD_GAD_Id)
                                    SELECT @GAD_Id, @GAD_Descricao, @GAD_GAD_Id
                                    WHERE @GAD_Id NOT IN (SELECT GAD_Id FROM dbo.GoogleAdwordsDimensao)";      

                    cmd = new SqlCommand(sql, conn);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Parameters.Add("@GAD_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAD_Descricao", SqlDbType.VarChar, 255);
                    cmd.Parameters.Add("@GAD_GAD_Id", SqlDbType.BigInt);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Prepare();

                    foreach (var dim in dimensoes)
                    {
                        cmd.Parameters["@GAD_Id"].Value = dim.Id;
                        cmd.Parameters["@GAD_Descricao"].Value = dim.Descricao;
                        cmd.Parameters["@GAD_GAD_Id"].Value = dim.ParentId;

                        cmd.ExecuteNonQuery();
                    }

                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao persistir dimensoes [GoogleAdwordsDimensao]", e);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

        }

        private void PersistirEstatisticas(List<PerformanceEstatisticaDTO> estatisticas)
        {
            SqlCommand cmd = null;
            try
            {
                using (var conn = new SqlConnection(DBINTERNET))
                {
                    conn.Open();

                    string sql = @" INSERT INTO [dbo].[GoogleAdwordsPerformance]
                                    ([GAF_GAN_Id],[GAF_Data],[GAF_GAC_Id],[GAF_GAC_Descricao],[GAF_GAG_Id],[GAF_GAG_Descricao],[GAF_GAP_Id],[GAF_PRO_Id]
                                    ,[GAF_Clicks],[GAF_Impressions],[GAF_Cost],[GAF_Conversions],[GAF_CostPerConversion]
                                    ,[GAF_ValuePerConversion],[GAF_ConversionValue],[GAF_AllConversionValue],[GAF_ValuePerAllConversion],[GAF_DtCadastro])     
                                    VALUES (@IdTransaction, @GAF_Data,@GAF_GAC_Id,@GAF_GAC_Descricao,@GAF_GAG_Id,@GAF_GAG_Descricao,@GAF_GAP_Id,@GAF_PRO_Id,@GAF_Clicks,
                                    @GAF_Impressions,@GAF_Cost,@GAF_Conversions,@GAF_CostPerConversion,@GAF_ValuePerConversion, 
                                    @GAF_ConversionValue, @GAF_AllConversionValue, @GAF_ValuePerAllConversion, Getdate())";

                    cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.Add("@IdTransaction", SqlDbType.Int);
                    cmd.Parameters.Add("@GAF_GAC_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_GAC_Descricao", SqlDbType.VarChar, 255);
                    cmd.Parameters.Add("@GAF_GAG_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_GAG_Descricao", SqlDbType.VarChar, 255);
                    cmd.Parameters.Add("@GAF_Data", SqlDbType.Date);
                    cmd.Parameters.Add("@GAF_PRO_Id", SqlDbType.Int);
                    cmd.Parameters.Add("@GAF_GAP_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_Clicks", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_Impressions", SqlDbType.Int);
                    cmd.Parameters.Add("@GAF_Cost", SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_Conversions", SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_CostPerConversion", SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_ValuePerConversion", SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_ConversionValue", SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_AllConversionValue", SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_ValuePerAllConversion", SqlDbType.Money);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Prepare();

                    foreach (var estatistica in estatisticas)
                    {
                        cmd.Parameters["@idtransaction"].Value = IdTransaction;
                        cmd.Parameters["@GAF_GAC_Id"].Value = estatistica.CampaignId;
                        cmd.Parameters["@GAF_GAC_Descricao"].Value = estatistica.CampaignName;
                        cmd.Parameters["@GAF_GAG_Id"].Value = estatistica.AdGroupId;
                        cmd.Parameters["@GAF_GAG_Descricao"].Value = estatistica.AdGroupName;
                        cmd.Parameters["@GAF_Data"].Value = estatistica.Date == null ? DateTime.Now : DateTime.ParseExact(estatistica.Date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        cmd.Parameters["@GAF_GAP_Id"].Value = estatistica.Id;
                        cmd.Parameters["@GAF_PRO_Id"].Value = Convert.ToInt32(estatistica.OfferId);
                        cmd.Parameters["@GAF_Clicks"].Value = estatistica.Clicks;
                        cmd.Parameters["@GAF_Impressions"].Value = estatistica.Impressions;
                        cmd.Parameters["@GAF_Cost"].Value = Convert.ToDecimal(estatistica.Cost) / 1000000;
                        cmd.Parameters["@GAF_Conversions"].Value = estatistica.Conversions / 100;
                        cmd.Parameters["@GAF_CostPerConversion"].Value = Convert.ToDecimal(estatistica.CostPerConversion) / 1000000;
                        cmd.Parameters["@GAF_ValuePerConversion"].Value = Convert.ToDecimal(estatistica.ValuePerConversion) / 100;
                        cmd.Parameters["@GAF_ConversionValue"].Value = Convert.ToDecimal(estatistica.ConversionValue) / 100;
                        cmd.Parameters["@GAF_AllConversionValue"].Value = Convert.ToDecimal(estatistica.AllConversionValue) / 100;
                        cmd.Parameters["@GAF_ValuePerAllConversion"].Value = Convert.ToDecimal(estatistica.ValuePerAllConversion) / 100;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao persistir estatisticas [GoogleAdwordsPerformance]", e);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

        }

        public async Task<List<SimulacaoResponse>> GetSimulacoesAlvo(SimulacaoRequest request)
        {
            List<SimulacaoResponse> result = null;

            try
            {
                result = await Service.ExecutaAcao<List<SimulacaoResponse>>(string.Format("Produto/Simulacoes/{0}/{1}", request.AdGrupoId, request.GoogleId));

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao obter simulacoes: {0}", ex.Message);
                return null;
            }
        }

        public async Task<List<PerformanceResponse>> GetPerformancesAlvo(string[] skus)
        {
            List<PerformanceResponse> result = null;

            try
            {
                result = await Service.ExecutaAcao<List<PerformanceResponse>>("Relatorio/Shopping", skus);

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao obter dados de performance: {0}", ex.Message);
                return null;
            }
        }

        public async Task<long> GetGoogleId(long grupoId, int sku)
        {
            CategoriaProdutoResponse result = null;

            try
            {
                result = await Service.ExecutaAcao<CategoriaProdutoResponse>(string.Format("Produto/{0}/{1}", grupoId, sku));

                if (result == null)
                    return 0;
                else
                    return result.GoogleId;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao obter dados de performance: {0}", ex.Message);
                return 0;
            }
        }

        private async Task<List<GrupoResponse>> GetGrupos(long CampanhaId)
        {
            List<GrupoResponse> grupos = null;

            try
            {
                grupos = await Service.ExecutaAcao<List<GrupoResponse>>(string.Format("Grupo/Campanha/{0}", CampanhaId));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao atualizar grupo: {0}", ex.Message);
            }

            return grupos;
        }

        public void BaixarProdutoCategorias(long campanhaId , long grupoId = 0)
        {
            List<GrupoResponse> grupos;
            if (grupoId == 0)
                grupos = Task.Run(() => GetGrupos(campanhaId)).Result;
            else
                grupos = new List<GrupoResponse>() { new GrupoResponse() { Id = grupoId, CampanhaId = campanhaId, Status = "ENABLED" } };

            if (grupos == null)
                return;

            AtualizarGrupos(campanhaId, grupos);

            List<CategoriaProdutoResponse> produtosCategorias;

            foreach (var grupo in grupos)
            {
                try
                {
                    if (grupo.Status != "ENABLED")
                        continue;

                    produtosCategorias = Task.Run(() => Service.ExecutaAcao<List<CategoriaProdutoResponse>>(String.Format("Produto/Grupo/{0}", grupo.Id))).Result;

                    if (produtosCategorias == null)
                        continue;

                    AtualizarCategoria(grupo.Id, produtosCategorias.Where(x => x.IsCategoria).ToList());

                    AtualizarProduto(grupo.Id, produtosCategorias.Where(x => !x.IsCategoria).ToList());

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Erro ao atualizar grupo {0} - {1}: {2}", grupo.Id, grupo.Nome, ex.Message);
                }
            }
        }

        public void AtualizarGrupos(long CampanhaId, List<GrupoResponse> grupos)
        {
            if (grupos.Count == 0)
                return;

            SqlCommand cmd = null;
            try
            {
                using (var conn = new SqlConnection(DBINTERNET))
                {
                    conn.Open();

                    string sql = @" BEGIN
	                                    INSERT INTO GoogleAdwordsGrupo (GAG_Id, GAG_GAC_Id, GAG_Descricao, GAG_VlBidMaxPadrao, GAG_Status)
	                                    SELECT	@GAG_Id, @GAG_GAC_Id, @GAG_Descricao, @GAG_VlBidMaxPadrao, @GAG_Status
	                                    WHERE	@GAG_Id NOT IN (SELECT GAG_Id FROM GoogleAdwordsGrupo)

	                                    IF @@ROWCOUNT = 0
	                                    BEGIN
		                                    UPDATE	GoogleAdwordsGrupo
		                                    SET		GAG_GAC_Id = @GAG_GAC_Id,
				                                    GAG_Descricao = @GAG_Descricao,
				                                    GAG_VlBidMaxPadrao = @GAG_VlBidMaxPadrao,
				                                    GAG_Status = @GAG_Status
		                                    WHERE	GAG_Id = @GAG_Id
                                            AND (       GAG_Descricao != @GAG_Descricao
                                                    OR  GAG_GAC_Id != @GAG_GAC_Id        
                                                    OR  GAG_VlBidMaxPadrao != @GAG_VlBidMaxPadrao
                                                    OR  GAG_Status != @GAG_Status
                                                )
	                                    END
                                    END";

                    cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.Add("@GAG_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAG_GAC_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAG_Descricao", SqlDbType.VarChar, 255);
                    cmd.Parameters.Add("@GAG_VlBidMaxPadrao", SqlDbType.Money);
                    cmd.Parameters.Add("@GAG_Status", SqlDbType.VarChar, 30);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Prepare();

                    foreach (var grupo in grupos)
                    {
                        cmd.Parameters["@GAG_Id"].Value = grupo.Id;
                        cmd.Parameters["@GAG_GAC_Id"].Value = grupo.CampanhaId;
                        cmd.Parameters["@GAG_Descricao"].Value = grupo.Nome;
                        cmd.Parameters["@GAG_VlBidMaxPadrao"].Value = Convert.ToDecimal(grupo.CpcBid) / Convert.ToDecimal(1000000);
                        cmd.Parameters["@GAG_Status"].Value = grupo.Status;

                        cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro incluir/alterar grupo: {0}", ex.Message);
                throw;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

        }

        private void AtualizarCategoria(long grupoId, List<CategoriaProdutoResponse> categorias)
        {
            if (categorias.Count == 0)
                return;

            SqlCommand cmd = null;
            try
            {
                using (var conn = new SqlConnection(DBINTERNET))
                {
                    conn.Open();

                    cmd = new SqlCommand(string.Format(" DELETE FROM GoogleAdwordsCategoria WHERE GAT_GAG_Id = {0}", grupoId), conn);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.ExecuteNonQuery();

                    cmd.CommandText = @"INSERT INTO GoogleAdwordsCategoria (GAT_Id, GAT_GAG_Id, GAT_GAT_Id, GAT_Descricao, GAT_Type, GAT_GAD_Id, GAT_VlCpc, GAT_StsAtivo)
	                                    VALUES (@GAT_Id, @GAT_GAG_Id, @GAT_GAT_Id, @GAT_Descricao, @GAT_Type, @GAT_GAD_Id, @GAT_VlCpc, @GAT_StsAtivo)";

                    cmd.Parameters.Add("@GAT_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAT_GAG_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAT_GAT_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAT_Descricao", SqlDbType.VarChar, 255);
                    cmd.Parameters.Add("@GAT_Type", SqlDbType.VarChar, 255);
                    cmd.Parameters.Add("@GAT_GAD_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAT_StsAtivo", SqlDbType.Bit);
                    cmd.Parameters.Add("@GAT_VlCpc", SqlDbType.Money);

                    cmd.Prepare();

                    foreach (var categoria in categorias)
                    {
                        if (categoria.Type == "UNKNOWN")
                            continue;

                        cmd.Parameters["@GAT_Id"].Value = categoria.GoogleId;
                        cmd.Parameters["@GAT_GAG_Id"].Value = categoria.GrupoId;
                        cmd.Parameters["@GAT_GAT_Id"].Value = categoria.ParentGoogleId;
                        cmd.Parameters["@GAT_Descricao"].Value = categoria.Categoria ?? "Desconhecido";
                        cmd.Parameters["@GAT_Type"].Value = categoria.Type;
                        cmd.Parameters["@GAT_GAD_Id"].Value = categoria.Dimensao;
                        cmd.Parameters["@GAT_StsAtivo"].Value = !categoria.IsExcluded;
                        cmd.Parameters["@GAT_VlCpc"].Value = Convert.ToDecimal(categoria.Cpc) / Convert.ToDecimal(1000000);

                        cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro incluir/alterar grupo: {0}", ex.Message);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        private void AtualizarProduto(long grupoId, List<CategoriaProdutoResponse> produtos)
        {
            if (produtos.Count == 0)
                return;

            SqlCommand cmd = null;
            try
            {
                using (var conn = new SqlConnection(DBINTERNET))
                {
                    conn.Open();

                    cmd = new SqlCommand(@"DELETE GoogleAdwordsProduto 
                                        WHERE GAP_GAG_Id = @GrupoId AND GAP_Id NOT IN (SELECT GoogleId FROM @SkuList)

                                        INSERT INTO GoogleAdwordsProduto (GAP_Id, GAP_GAG_Id, GAP_GAT_Id, GAP_Pro_id, GAP_VlCpc, GAP_StsAtivo, GAP_DtCadastro)
                                        SELECT	GoogleId, GrupoId, CategoriaId, Sku, Cpc, Ativo, getdate()
                                        FROM	@SkuList 
                                        WHERE GoogleId NOT IN (SELECT GAP_Id FROM GoogleAdwordsProduto) and CategoriaId != 0

                                        UPDATE T
                                        SET GAP_GAG_Id = GrupoId,
	                                        GAP_GAT_Id = CategoriaId,
	                                        GAP_PRO_Id = Sku,
	                                        GAP_VlCpc = Cpc,
                                            GAP_StsAtivo = Ativo,
	                                        GAP_DtUltAlteracao = getdate()
                                        FROM GoogleAdwordsProduto T 
                                            JOIN @SkuList ON GAP_Id = GoogleId and GAP_GAG_Id = GrupoId
                                        WHERE GAP_VlCpc != Cpc OR GAP_StsAtivo != Ativo OR GAP_GAT_Id != CategoriaId", conn);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Parameters.AddWithValue("@GrupoId", grupoId);
                    var paramTable = cmd.Parameters.AddWithValue("@SkuList", CreateSkuList(produtos));
                    paramTable.SqlDbType = SqlDbType.Structured;
                    paramTable.TypeName = "dbo.SkuList";

                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro incluir/alterar grupo: {0}", ex.Message);
                return;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        private void BaixarCampanhasAutomaticas()
        {
            var response = GetCampanhasAutomaticas();

            PersistirCampanhas(response);

            foreach (var campanha in response)
            { 
                BaixarProdutoCategorias(campanha.Id);
            }

        }

        private IEnumerable<InsertUpdateDTO> ObterCampanhasAlvo(IEnumerable<InsertUpdateDTO> dtos)
        {

            using (SqlConnection conn = new SqlConnection(DBINTERNET))
            {
                string sql = @" declare @saida table
                                (
	                                Sku int, 
	                                CampanhaId bigint,
	                                GrupoId bigint,
	                                flativo bit default 1
                                )

                                insert into @saida (sku, CampanhaId, GrupoId)
                                SELECT Sku, GAC_Id CampanhaId, GAG_Id GrupoId
                                FROM @SkuList
	                                JOIN ProdutoEstruturado ON (Sku = PES_PRO_Id) 
	                                JOIN BuscadorRegraEstruturado ON(LEFT(PES_EST_CdEstruturado, LEN(BRE_EST_CdEstruturado)) = BRE_EST_CdEstruturado) 
	                                JOIN BuscadorDescrRegra ON (BRA_Id = BRE_BRA_Id) 
	                                JOIN GoogleAdwordsCampanha ON (GAC_Descricao = BRA_Descricao)
	                                JOIN GoogleAdwordsGrupo ON (GAG_GAC_Id = GAC_Id)
	                                JOIN Estruturado EST1 ON(EST1.EST_CdEstruturado = PES_CdEstruturadoNivel2) 
	                                JOIN Estruturado EST2 ON (EST2.EST_CdEstruturado = PES_CdEstruturadoNivel3)
	                                JOIN Estruturado EST3 ON(EST3.EST_CdEstruturado = PES_CdEstruturadoNivel4)
	                                JOIN disponibilidade ON(dsp_pro_id = Sku) 
	                                LEFT JOIN BuscadorDeParaCategorias ON (BDP_BCD_Id = 104 AND LEFT(PES_EST_CdEstruturado, LEN(BDP_CdEstruturado)) = BDP_CdEstruturado) 
	                                LEFT JOIN ProdutoCompl ON (Sku = PCL_PRO_Id AND PCL_CAR_CAG_Id = 230)
                                WHERE PES_CdEstruturadoNivel1 = '03'
	                                AND DSP_LgDisponibilidade = 1
	                                AND PCL_PRO_Id IS NULL
	                                AND((DSP_LgVendaDescoberta = 0 OR (DSP_LgVendaDescoberta = 1 AND DSP_QtdeDisponivelWeb >= 5)) OR DSP_LgPreVenda = 1)
	                                AND Sku NOT IN (select BEX_Pro_id from BuscadorProdutoExcecao where BEX_BCD_Id = 104)
	                                AND BRA_LgAtivo = 'S'

                                select Sku, CampanhaId, GrupoId, flativo from @Saida
                                UNION
                                select Sku, isnull(CampanhaId,0), isnull(GrupoId,0), 0 from @SkuList
                                where sku not in (select sku from @saida)";

                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.CommandTimeout = int.MaxValue;

                var paramTable = cmd.Parameters.AddWithValue("@SkuList", CreateSkuList(dtos));
                paramTable.SqlDbType = SqlDbType.Structured;
                paramTable.TypeName = "dbo.SkuList";

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    yield return new InsertUpdateDTO() { Sku = reader.GetInt32(0), CampanhaId = reader.GetInt64(1), GrupoId = reader.GetInt64(2), FlValido = reader.GetBoolean(3) };
                }
            }
        }

        private SqlMetaData[] CreateSkuStruct()
        {
            SqlMetaData[] metaData = new SqlMetaData[11];

            metaData[0] = new SqlMetaData("Sku", SqlDbType.Int);
            metaData[1] = new SqlMetaData("CompanhaId", SqlDbType.BigInt);
            metaData[2] = new SqlMetaData("GrupoId", SqlDbType.BigInt);
            metaData[3] = new SqlMetaData("CategoriaId", SqlDbType.BigInt);
            metaData[4] = new SqlMetaData("GoogleId", SqlDbType.BigInt);
            metaData[5] = new SqlMetaData("Cpc", SqlDbType.Money);
            metaData[6] = new SqlMetaData("Cpa", SqlDbType.Money);
            metaData[7] = new SqlMetaData("Roi", SqlDbType.Money);
            metaData[8] = new SqlMetaData("Itens", SqlDbType.Int);
            metaData[9] = new SqlMetaData("Qtde", SqlDbType.Int);
            metaData[10] = new SqlMetaData("Ativo", SqlDbType.Bit);

            return metaData;
        }

        /*private IEnumerable<SqlDataRecord> CreateSkuList(IEnumerable<string> skus, long campanhaId = 0, long grupoId = 0, long CategoriaId = 0)
        {

            SqlDataRecord record = new SqlDataRecord(CreateSkuStruct());

            foreach (var sku in skus)
            {
                record.SetInt32(0, Convert.ToInt32(sku));
                record.SetInt64(1, campanhaId);
                record.SetInt64(2, grupoId);
                record.SetInt64(3, CategoriaId);
                record.SetInt64(4, 0);
                record.SetDecimal(5, 0);
                record.SetDecimal(6, 0);
                record.SetDecimal(7, 0);
                record.SetBoolean(8, true);

                yield return record;
            }
        }*/

        private IEnumerable<SqlDataRecord> CreateSkuList(IEnumerable<InsertUpdateDTO> skus)
        {
            SqlDataRecord record = new SqlDataRecord(CreateSkuStruct());

            foreach (var sku in skus)
            {
                record.SetInt32(0, Convert.ToInt32(sku.Sku));
                record.SetInt64(1, sku.CampanhaId = sku.CampanhaId);
                record.SetInt64(2, sku.GrupoId);
                record.SetInt64(3, sku.CategoriaId);
                record.SetInt64(4, sku.GoogleId);
                record.SetDecimal(5, sku.VlCpc);
                record.SetDecimal(6, sku.VlCpa);
                record.SetDecimal(7, sku.VlRoi );
                record.SetInt32(8, sku.Itens);
                record.SetInt32(9, sku.Qtde);
                record.SetBoolean(10, sku.FlValido);

                yield return record;
            }
        }

        private IEnumerable<SqlDataRecord> CreateSkuList(IEnumerable<CategoriaProdutoResponse> skus)
        {
            SqlDataRecord record = new SqlDataRecord(CreateSkuStruct());

            foreach (var sku in skus)
            {
                record.SetInt32(0, Convert.ToInt32(sku.Sku));
                record.SetInt64(1, 0);
                record.SetInt64(2, sku.GrupoId);
                record.SetInt64(3, sku.ParentGoogleId);
                record.SetInt64(4, sku.GoogleId);
                record.SetDecimal(5, Convert.ToDecimal(sku.Cpc) / 1000000);
                record.SetDecimal(6, 0);
                record.SetDecimal(7, 0);
                record.SetInt32(8, 0);
                record.SetInt32(9, 0);
                record.SetBoolean(10, !sku.IsExcluded);

                yield return record;
            }
        }

        /*private IEnumerable<SqlDataRecord> CreateSkuList(IEnumerable<CategoriaProdutoResponse> skus)
        {
            SqlDataRecord record = new SqlDataRecord(CreateSkuStruct());

            foreach (var sku in skus)
            {
                record.SetInt32(0, Convert.ToInt32(sku.Sku));
                record.SetInt64(1, 0);
                record.SetInt64(2, sku.GrupoId);
                record.SetInt64(3, sku.ParentGoogleId);
                record.SetInt64(4, sku.GoogleId);
                record.SetDecimal(5, Convert.ToDecimal(sku.Cpc) / 1000000);
                record.SetBoolean(6, !sku.IsExcluded);

                yield return record;
            }
        }*/

        private void PersistirCampanhas(List<CampanhaResponse> campanhas)
        {
            SqlCommand cmd = null;
            try
            {
                using (var conn = new SqlConnection(DBINTERNET))
                {
                    conn.Open();

                    string sql = @" BEGIN
	                                    INSERT INTO GoogleAdwordsCampanha (GAC_Id, GAC_Descricao, GAC_DtInicio, GAC_DtFim, GAC_VlrOrcamento, GAC_Status)
	                                    SELECT	@GAC_Id, @GAC_Descricao, @GAC_DtInicio, @GAC_DtFim, @GAC_VlrOrcamento, @GAC_Status
	                                    WHERE	@GAC_Id NOT IN (SELECT GAC_Id FROM GoogleAdwordsCampanha)

	                                    IF @@ROWCOUNT = 0
	                                    BEGIN
		                                    UPDATE	GoogleAdwordsCampanha
		                                    SET		GAC_Descricao = @GAC_Descricao,
				                                    GAC_DtInicio = @GAC_DtInicio,
				                                    GAC_DtFim = @GAC_DtFim,
				                                    GAC_VlrOrcamento = @GAC_VlrOrcamento,
				                                    GAC_Status = @GAC_Status
		                                    WHERE	GAC_Id = @GAC_Id
                                                AND (   GAC_Descricao != @GAC_Descricao
                                                        OR GAC_DtInicio != @GAC_DtInicio
				                                        OR GAC_DtFim != @GAC_DtFim
				                                        OR GAC_VlrOrcamento != @GAC_VlrOrcamento				                                        
				                                        OR GAC_Status != @GAC_Status  
                                                    )
	                                    END
                                    END";

                    cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.Add("@GAC_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAC_Descricao", SqlDbType.VarChar, 255);
                    cmd.Parameters.Add("@GAC_DtInicio", SqlDbType.SmallDateTime);
                    cmd.Parameters.Add("@GAC_DtFim", SqlDbType.SmallDateTime);
                    cmd.Parameters.Add("@GAC_VlrOrcamento", SqlDbType.Money);
                    cmd.Parameters.Add("@GAC_Status", SqlDbType.VarChar, 30);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Prepare();

                    foreach (var campanha in campanhas)
                    {
                        cmd.Parameters["@GAC_Id"].Value = campanha.Id;
                        cmd.Parameters["@GAC_Descricao"].Value = campanha.Nome;
                        cmd.Parameters["@GAC_DtInicio"].Value = DateTime.ParseExact(campanha.DataInicio, "yyyyMMdd", CultureInfo.InvariantCulture);
                        cmd.Parameters["@GAC_DtFim"].Value = DateTime.ParseExact(campanha.DataFim, "yyyyMMdd", CultureInfo.InvariantCulture);
                        cmd.Parameters["@GAC_VlrOrcamento"].Value = Convert.ToDecimal(campanha.Orcamento) / Convert.ToDecimal(1000000);
                        cmd.Parameters["@GAC_Status"].Value = campanha.Status;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro incluir/alterar campanhas: {0}", ex.Message);
                throw;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

        }

        public List<CampanhaResponse> GetCampanhasAutomaticas()
        {
            try
            {
                var response = Task.Run(() => Service.ExecutaAcao<List<CampanhaResponse>>("Campanha/Automaticas"));

                response.Wait();

                return response.Result;
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao obter dados de campanhas automaticas", e);
            }
        }

    }

    public class DeleteBidRequest
    {
        public long AdGroupId { get; set; }
        public long GoogleId { get; set; }
    }
    public class GrupoResponse
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public long CampanhaId { get; set; }
        public long CpcBid { get; set; }
        public string Status { get; set; }
    }

    public class GrupoDTO
    {
        public long Id { get; set; }
        public long CampanhaId { get; set; }
    }

    public class CampanhaResponse
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public long Orcamento { get; set; }
        public string DataInicio { get; set; }
        public string DataFim { get; set; }
        public string Status { get; set; }
    }

    public interface ICrudDTO
    {
        long Id { get; set; }
        long GrupoId { get; set; }
        long GoogleId { get; set; }
        bool FlValido { get; set; }
        string MsgErro { get; set; }
    }

    public class ShoppingResponse
    {
        public string OfferId { get; set; }
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public long AdGroupId { get; set; }
        public string AdGroupName { get; set; }
        public string Date { get; set; }
        public long Clicks { get; set; }
        public long Impressions { get; set; }
        public decimal Cost { get; set; }
        public double Conversions { get; set; }
        public decimal CostPerConversion { get; set; }
        public double ValuePerConversion { get; set; }
        public double ValuePerAllConversion { get; set; }
        public double ConversionValue { get; set; }
        public double AllConversionValue { get; set; }
    }

    public class InsertUpdateDTO : ICrudDTO
    {
        public long Id { get; set; }
        public int Sku { get; set; }
        public long CampanhaId { get; set; }
        public long GrupoId { get; set; }
        public long CategoriaId { get; set; }
        public long GoogleId { get; set; }
        public bool Fltratamento { get; set; }
        public decimal VlCpc { get; set; }
        public decimal VlCpa { get; set; }
        public decimal VlRoi { get; set; }
        public int Itens { get; set; }
        public int Qtde { get; set; }
        public bool FlValido { get; set; }
        public string MsgErro { get; set; }
        public long GrupoRefId { get; set; }
        public long GoogleRefId { get; set; }
    }

    public class DeleteDTO : ICrudDTO
    {
        public long Id { get; set; }
        public long GrupoId { get; set; }
        public long GoogleId { get; set; }
        public bool FlValido { get; set; }
        public string MsgErro { get; set; }
    }

    public class ConfigCatRequest
    {
        public long GrupoId { get; set; }
        public long CategoriaId { get; set; }
    }

    public class DimensaoResponse
    {
        public long Id { get; set; }
        public string Descricao { get; set; }
        public long ParentId { get; set; }
    }

    public class InsertUpdateRequest
    {
        public string OfferId { get; set; }
        public long AdGroupId { get; set; }
        public long ParentId { get; set; }
        public long GoogleId { get; set; }
        public long CpcBid { get; set; }
    }

    public class CategoriaProdutoResponse
    {
        public long GoogleId { get; set; }
        public long GrupoId { get; set; }
        public string Sku { get; set; }
        public long Cpc { get; set; }
        public long NewCpc { get; set; }
        public decimal Cpa { get; set; }
        public decimal Custo { get; set; }
        public double Conversoes { get; set; }
        public long Clicks { get; set; }
        public string Type { get; set; }
        public bool IsMarca { get; set; }
        public bool IsCategoria { get; set; }
        public long ParentGoogleId { get; set; }
        public string Categoria { get; set; }
        public long Dimensao { get; set; }
        public bool IsExcluded { get; set; }
        public string Status { get; set; }
    }

    public class ConfigRequest
    {
        public long AdGroupId { get; set; }
        public long GoogleId { get; set; }
    }

    public class AdicionarBidRequest
    {
        public int sku { get; set; }
        public int qtde { get; set; }
        public int itens { get; set; }
    }

    public class PerformanceResponse
    {
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public long AdGroupId { get; set; }
        public string AdGroupName { get; set; }
        public string OfferId { get; set; }
        public long Id { get; set; }
        public long Cpc { get; set; }
        public List<EstatisticaResponse> Estatisticas { get; set; }
        public List<SimulacaoResponse> Simulacoes { get; set; }
    }

    public class PerformanceDTO
    {
        public List<PerformanceEstatisticaDTO> Estatisticas { get; set; }
        public List<PerformanceSimulacaoDTO> Simulacoes { get; set; }
    }

    public class PerformanceEstatisticaDTO
    {
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public long AdGroupId { get; set; }
        public string AdGroupName { get; set; }
        public string OfferId { get; set; }
        public long Id { get; set; }
        public string Date { get; set; }
        public long Clicks { get; set; }
        public long Impressions { get; set; }
        public decimal Cost { get; set; }
        public double Conversions { get; set; }
        public double ConversionValue { get; set; }
        public double AllConversionValue { get; set; }
        public decimal CostPerConversion { get; set; }
        public double ValuePerConversion { get; set; }
        public double ValuePerAllConversion { get; set; }
    }

    public class PerformanceSimulacaoDTO
    {
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public long AdGroupId { get; set; }
        public string AdGroupName { get; set; }
        public string OfferId { get; set; }
        public long Id { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public long Bid { get; set; }
        public long Clicks { get; set; }
        public long Cost { get; set; }
        public long Impressions { get; set; }
    }

    public class EstatisticaResponse
    {
        public string Date { get; set; }
        public long Clicks { get; set; }
        public long Impressions { get; set; }
        public decimal Cost { get; set; }
        public double Conversions { get; set; }
        public double ConversionValue { get; set; }
        public double AllConversionValue { get; set; }
        public decimal CostPerConversion { get; set; }
        public double ValuePerConversion { get; set; }
        public double ValuePerAllConversion { get; set; }
    }

    public class ShoppingRequest
    {
        public string Time { get; set; }
        public string Interval { get; set; }
        public int[] skus { get; set; }
    }

    public class SimulacaoRequest
    {
        public long AdGrupoId { get; set; }
        public long GoogleId { get; set; }
    }

    public class SimulacaoResponse
    {
        public long CriterionId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public long CampaignId { get; set; }
        public long AdGroupId { get; set; }
        public long Bid { get; set; }
        public long Clicks { get; set; }
        public long Cost { get; set; }
        public long Impressions { get; set; }
    }

}
