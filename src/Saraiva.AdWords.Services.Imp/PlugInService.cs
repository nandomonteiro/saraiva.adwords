﻿using Newtonsoft.Json;

namespace Saraiva.AdWords.Services.Plugins
{
    public abstract class PlugInService<T> : IPlugInService
    {
        public string Acao { get; set; }
        public string Mensagem { get; set; }
        public T request { get; set; }

        public PlugInService(string acao, string mensagem)
        {
            Acao = acao;
            Mensagem = mensagem;
            request = JsonConvert.DeserializeObject<T>(Mensagem);
        }

        public abstract void ExecutarServico();

    }
}
