﻿using System.Collections.Generic;

namespace ProductIntegration.Service.Adwords
{
    public class ResponseDto
    {
        public string Acao { get; set; }
        public bool FlErro { get; set; }
        public string MsgErro { get; set; }
        public IEnumerable<SkuDto> Skus { get; set; }
    }
}