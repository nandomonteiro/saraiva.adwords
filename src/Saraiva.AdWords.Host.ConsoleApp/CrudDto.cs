﻿using System.Collections.Generic;

namespace ProductIntegration.Service.Adwords
{
    public class CrudDto
    {
        public string Acao { get; set; }
        public long GrupoId { get; set; }
        public bool flNewBid { get; set; }
        public IEnumerable<SkuDto> Skus { get; set; }
    }
}