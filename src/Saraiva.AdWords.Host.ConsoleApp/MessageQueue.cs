﻿using Saraiva.AdWords.Host.Domain;
using System.Collections.Generic;

namespace ProductIntegration.Service.Adwords
{
    public class MessageQueue : IBaseRequest
    {
        public string Acao { get; set; }
        public string Mensagem { get; set; }
    }
}