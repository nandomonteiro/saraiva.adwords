﻿using System.Collections.Generic;

namespace Saraiva.AdWords.Host.Domain
{
    public class ClearingResponse
    {
        public string Acao { get; set; }
        public bool FlErro { get; set; }
        public string MsgErro { get; set; }
        public IEnumerable<ResponseSku> Skus { get; set; }


    }
}