﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductIntegration.Service.Adwords
{
    public class Configs
    {
        public Configs()
        {
            HostNames = new Hosts();
        }
        public string hubMktp { get; set; }

        public string awsSQSHostImagem { get; set; }

        public string awsAccessKeyId { get; set; }

        public string awsSecretAccessKey { get; set; }

        public string Proxy
        {
            get
            {
                return "http://" + ProxyHost + ":" + ProxyPort;
            }
        }

        public string ProxyHost { get; set; }

        public int ProxyPort { get; set; }

        public string ParceiroHubAnyMarketTokenKey { get; set; }

        public string ParceiroHubAnyMarketTokenValue { get; set; }

        public Hosts HostNames { get; set; }
    }

    public class Hosts
    {
        public string awsSQSHost { get; set; }

        public string awsRegion { get; set; }

        public Amazon.RegionEndpoint RegionEndpoint
        {
            get
            {
                if (string.IsNullOrEmpty(awsRegion))
                    throw new Exception("awsRegion não encontrado no arquivo de configuração.");
                else
                    return Amazon.RegionEndpoint.GetBySystemName(awsRegion);
            }
        }

        public int MaxNumberOfMessages { get; set; }

        public string awsSQSHostPedidoPago { get; set; }

        public string ClientHost { get; set; }
    }
}
