﻿namespace ProductIntegration.Service.Adwords
{
    public class SkuDto
    {
        public long Id { get; set; }
        public string Sku { get; set; }
        public long Cpc { get; set; }
        public long NewCpc { get; set; }
        public string Cat { get; set; }
    }
}