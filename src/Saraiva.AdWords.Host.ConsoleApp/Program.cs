﻿
using Amazon.SQS;
using Amazon.SQS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;
using Saraiva.AdWords.Host.Domain;
using System.Data.SqlClient;
using System.Globalization;
using System.Data;
using Microsoft.SqlServer.Server;
using System.Net.Http.Headers;

namespace ProductIntegration.Service.Adwords
{
    public class Program
    {
        public const long cte_grupo_google = 9310884460;
        public const string cte_action_clearing = "Clearing";
        public const string cte_action_consultar = "Consultar";
        public const string cte_action_cpa_bidmax = "CPABidMax";
        public const string cte_connection = @"Data Source = 10.249.48.19; Initial Catalog = dbInternet; Persist Security Info = True; User ID = inetanonimo; Password = postergar; Max Pool Size = 1000";

        static Dictionary<string, Action> _controllers =
            new Dictionary<string, Action>()
            {
                { "1", ObterMaisVendidos },
                { "2", ProcessarFila }
            };

        static Dictionary<string, Action<string>> _actions =
            new Dictionary<string, Action<string>>()
            {
                { cte_action_clearing, ActionClearing },
                { cte_action_cpa_bidmax, ActionObterCAPBidMax }
            };

        public struct Log
        {
            public int idTransaction;
            public int GAN_Id;
        }

        private static Log _log;

        //{"Acao":"Clearing","Mensagem":[9032983,8620427]}

        public static void Main(string[] args)
        {
            try
            {
                //if (args.Length > 0 && _controllers.ContainsKey(args[0]))
                //_controllers[args[0]]();

                /*var bidsAlvo = new List<InsertRequest>()
                {
                    new InsertRequest() { AdGroupId = 43371522366, OfferId = "9352208", CpcBid = 0, ParentId = 311960327262 },
                    new InsertRequest() { AdGroupId = 43371522366, OfferId = "9704478", CpcBid = 0, ParentId = 311960327262 }
                };

                UploadBids(bidsAlvo);*/

                ActionAtualizarProdutoCategorias("43371522366");

                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }

        public static async void ActionObterCAPBidMax(string message)
        {
            try
            { 
                var srv = new ServicoGoogle();

                var result = await srv.ExecutaAcao<CPABidMaxResponse>(cte_action_cpa_bidmax, message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao tentar executar a acao {0}. Messagem: {1}. Erro: {2}.", cte_action_cpa_bidmax, message, ex.Message);
            }
        }

        public static Log InsertTransaction(string type, string action)
        {
            using (SqlConnection conn = new SqlConnection(cte_connection))
            {
                string sql = @"	INSERT INTO ProductIntegrationTransaction  (startDate, endDate, type, status, partnerId)
	                            VALUES(getdate(), null, @type, null, @partnerId);

	                            print 'ProductIntegrationTransaction'

	                            SET @idTransaction = CAST(scope_identity() AS int)

	                            INSERT INTO GoogleAdwordsTransaction
				                            ([GAN_TRA_Id]
				                            ,[GAN_Action]
				                            ,[GAN_Error])
			                            VALUES
				                            (@idTransaction, @action,0)

	                            SET @GAN_Id = CAST(scope_identity() AS int)";
                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.Parameters.Add("@type", System.Data.SqlDbType.VarChar,10).Value = type;
                cmd.Parameters.Add("@partnerId", System.Data.SqlDbType.Int).Value = 104;
                cmd.Parameters.Add("@action", System.Data.SqlDbType.VarChar, 30).Value = action;
                cmd.Parameters.Add("@idTransaction", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("@GAN_Id", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                Log retorno;

                retorno.idTransaction = Convert.ToInt32(cmd.Parameters["@idTransaction"].Value);
                retorno.GAN_Id = Convert.ToInt32(cmd.Parameters["@GAN_Id"].Value);

                return retorno;
            }
        }

        private static IEnumerable<SqlDataRecord> CreateSkuList(IEnumerable<int> skus, long campanhaId = 0, long grupoId = 0, long CategoriaId = 0)
        {
            SqlMetaData[] metaData = new SqlMetaData[1];

            metaData[0] = new SqlMetaData("Sku", SqlDbType.Int);
            metaData[1] = new SqlMetaData("CompanhaId", SqlDbType.BigInt);
            metaData[2] = new SqlMetaData("GrupoId", SqlDbType.BigInt);
            metaData[3] = new SqlMetaData("CategoriaId", SqlDbType.BigInt);
            metaData[4] = new SqlMetaData("GoogleId", SqlDbType.BigInt);

            SqlDataRecord record = new SqlDataRecord(metaData);

            foreach (var sku in skus)
            {
                record.SetInt32(0, sku);
                record.SetInt64(1, campanhaId);
                record.SetInt64(2, grupoId);
                record.SetInt64(3, CategoriaId);
                record.SetInt64(4, 0);

                yield return record;
            }
        }

        private static IEnumerable<SqlDataRecord> CreateSkuList(IEnumerable<CategoriaProdutoResponse> skus)
        {
            SqlMetaData[] metaData = new SqlMetaData[1];

            metaData[0] = new SqlMetaData("Sku", SqlDbType.Int);
            metaData[1] = new SqlMetaData("CompanhaId", SqlDbType.BigInt);
            metaData[2] = new SqlMetaData("GrupoId", SqlDbType.BigInt);
            metaData[3] = new SqlMetaData("CategoriaId", SqlDbType.BigInt);
            metaData[4] = new SqlMetaData("GoogleId", SqlDbType.BigInt);

            SqlDataRecord record = new SqlDataRecord(metaData);

            foreach (var sku in skus)
            {
                record.SetInt32(0, Convert.ToInt32(sku.Sku));
                record.SetInt64(1, 0);
                record.SetInt64(2, sku.GrupoId);
                record.SetInt64(3, sku.ParentGoogleId);
                record.SetInt64(4, sku.GoogleId);

                yield return record;
            }
        }

        public static void ActionClearing(string message)
        {
            var skus = JsonConvert.DeserializeObject<int[]>(message);

            if (skus == null || skus.Count() == 0)
                return;

            BaixarCampanhasAutomaticas();

            var skusAlvo = new List<InsertUpdateDTO>();

            foreach (var campanhaId in ObterCampanhasAlvo(skus))
            {
                BaixarProdutoCategorias(campanhaId);
                skusAlvo.AddRange(ObterProdutosAlvo(campanhaId, skus));
            }

            foreach (var action in Clearing(skusAlvo))
                EnviarParaFila(action.Key, action.Value);
        }

        public static void ActionInsert(string message)
        {
            var skus = JsonConvert.DeserializeObject<List<InsertUpdateRequest>>(message);

            if (skus == null || skus.Count() == 0)
                return;

            var bidsAlvo = ObterBidsAlvo(skus);

            UploadBids(bidsAlvo);

            //BaixarPerformanceDeCampanha(skus);
        }

        private static IEnumerable<InsertRequest> ObterBidsAlvo(IEnumerable<InsertUpdateRequest> skus)
        {
            foreach (var sku in skus)
            {
                var bid = new InsertRequest() { AdGroupId = sku.GrupoId, OfferId = sku.Sku.ToString(), CpcBid = 0, ParentId =  sku.CategoriaId };
                yield return bid;
            }
        }

        private static async void UploadBids(IEnumerable<InsertRequest> bids)
        {
            try
            {
                var srv = new ServicoGoogle();

                var result = await srv.ExecutaAcao<string>("Produto/InsertBid", bids);

                Console.Write(result);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao tentar executar adicionar bids: {0}", ex.Message );
            }

        }

        public static Dictionary<string, List<InsertUpdateRequest>> Clearing(IEnumerable<InsertUpdateDTO> itens)
        {
            var result = new Dictionary<string, List<InsertUpdateRequest>>();

            foreach (var item in itens)
            {
                var request = new InsertUpdateRequest() {
                    Sku = item.Sku,
                    CampanhaId = item.CampanhaId,
                    GrupoId = item.GrupoId,
                    CategoriaId = item.CategoriaId,
                    GoogleId = item.GoogleId
                };

                var action = item.GoogleId == 0 ? "Insert" : "Update";

                if ( result.ContainsKey(action))
                    result[action].Add(request);
                else
                    result.Add(action, new List<InsertUpdateRequest>() { request });
            }

            return result; 
        }
        public static async Task<List<CampanhaResponse>> GetCampanhasAutomaticas()
        {
            var srv = new ServicoGoogle();

            List<CampanhaResponse> campanhas = null;

            try
            {
                campanhas = await srv.ExecutaAcao<List<CampanhaResponse>>("Campanha/Automaticas");
                return campanhas;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao obter campanha: {0}", ex.Message);
                return null;
            }
        }

        public static IEnumerable<InsertUpdateDTO> ObterProdutosAlvo(long campanhaId, int[] skus)
        {

                using (SqlConnection conn = new SqlConnection(cte_connection))
                {
                    string sql = @" SELECT Sku, CampanhaId, GAG_Id GrupoId, GAT_Id CategoriaId, isnull(GAP_Id,0) GoogleId
                                    FROM @SkuList
	                                    JOIN ProdutoEstruturado ON PES_PRO_Id = Sku
	                                    JOIN GoogleAdwordsGrupo ON GAG_GAC_Id = CampanhaId
	                                    JOIN GoogleAdwordsCategoria ON GAT_GAG_Id = GAG_Id
	                                    JOIN Estruturado ON (EST_CdEstruturado = PES_CdEstruturadoNivel2 AND EST_Descricao = GAT_Descricao) 
	                                    LEFT JOIN GoogleAdwordsProduto ON GAP_GAG_Id = GAG_Id";
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    var paramTable = cmd.Parameters.AddWithValue("@SkuList", CreateSkuList(skus, campanhaId: campanhaId));
                    paramTable.SqlDbType = SqlDbType.Structured;
                    paramTable.TypeName = "dbo.SkuList";

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var item = new InsertUpdateDTO() {
                                                            Sku = reader.GetInt32(0),
                                                            CampanhaId = reader.GetInt64(1),
                                                            GrupoId = reader.GetInt64(2),
                                                            CategoriaId = reader.GetInt64(3),
                                                            GoogleId = reader.GetInt64(4)
                                                         };
                        yield return item;
                    }
                }
        }

        public static IEnumerable<long> ObterCampanhasAlvo(int[] skus)
        {

            using (SqlConnection conn = new SqlConnection(cte_connection))
            {
                string sql = @" SELECT GAC_Id
                                FROM GoogleAdwordsCampanha
                                WHERE EXISTS (
	                                            SELECT NULL FROM BuscadorDescrRegra
	                                                JOIN BuscadorRegraEstruturado ON BRE_BRA_Id = BRA_Id
	                                                JOIN ProdutoEstruturado ON (LEFT(PES_EST_CdEstruturado, LEN(BRE_EST_CdEstruturado)) = BRE_EST_CdEstruturado) 
	                                                AND NOT EXISTS (
	                                                SELECT	NULL
	                                                FROM	[dbo].[BuscadorBlackList]
				                                                JOIN BuscadorRegra ON BCR_BCD_Id = BBL_BCD_Id
	                                                WHERE	BCR_BRA_Id = BRA_Id AND BBL_PRO_Id = PES_PRO_Id)
	                                            JOIN @SkuList ON PES_PRO_Id = Sku
	                            WHERE BRA_Descricao = GAC_Descricao AND BRA_LgAtivo = 'S')";

                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);

                var paramTable = cmd.Parameters.AddWithValue("@SkuList", CreateSkuList(skus));
                paramTable.SqlDbType = SqlDbType.Structured;
                paramTable.TypeName = "dbo.SkuList";

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    yield return reader.GetInt64(0);
                }
            }
        }

        public static async void BaixarPerformanceDeCampanha(List<InsertUpdateRequest> skus)
        {
            try
            {
                var performances = await GetPerformanceLastWeek(skus.Select(x => x.CampanhaId).Distinct());

                using (SqlConnection conn = new SqlConnection(cte_connection))
                {
                    string sql = @" INSERT INTO GoogleAdwordsPerformance (GAF_GAC_Id, GAF_Data, GAF_GAG_Id, GAF_PRO_Id, GAF_Clicks, GAF_Impressions, GAF_Cost ,GAF_Conversions ,GAF_CostPerConversion, GAF_ValuePerConversion)
                                    VALUES (@GAF_GAC_Id, @GAF_Data, @GAF_GAG_Id, @GAF_PRO_Id, @GAF_Clicks, @GAF_Impressions, @GAF_Cost, @GAF_Conversions, @GAF_CostPerConversion, @GAF_ValuePerConversion)";

                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.Add("@GAF_Data", SqlDbType.Date);
                    cmd.Parameters.Add("@GAF_GAC_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_GAG_Id", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_PRO_Id", SqlDbType.Int);
                    cmd.Parameters.Add("@GAF_Clicks", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_Impressions", SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_Cost", SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_Conversions", SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_CostPerConversion", SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_ValuePerConversion",SqlDbType.Money);

                    cmd.Prepare();

                    foreach (var item in performances)
                    {
                        cmd.Parameters["@GAF_Data"].Value = DateTime.ParseExact(item.Date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        cmd.Parameters["@GAF_GAC_Id"].Value = item.CampaignId;
                        cmd.Parameters["@GAF_GAG_Id"].Value = item.AdGroupId;
                        cmd.Parameters["@GAF_PRO_Id"].Value = Convert.ToInt32(item.OfferId);
                        cmd.Parameters["@GAF_Clicks"].Value = item.Clicks;
                        cmd.Parameters["@GAF_Impressions"].Value = item.Impressions;
                        cmd.Parameters["@GAF_Cost"].Value = item.Cost / 1000000;
                        cmd.Parameters["@GAF_Conversions"].Value = item.Conversions / 100;
                        cmd.Parameters["@GAF_CostPerConversion"].Value = item.CostPerConversion / 1000000;
                        cmd.Parameters["@GAF_ValuePerConversion"].Value = item.ValuePerConversion / 100;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exibicao temporaria. Registrar em log. Erro: {0}", e.Message);
            }
        }
        public static void AtualizarSimulacoesDeCampanha(List<long> campanhas, List<PerformanceResponse> performances)
        {
            try
            {
                //var performances = await GetPerformanceLastWeek(campanhas);

                using (SqlConnection conn = new SqlConnection(cte_connection))
                {
                    string sql = @" INSERT INTO GoogleAdwordsSimulacao (GAS_GAC_Id, GAF_Data, GAF_GAG_Id, GAF_PRO_Id, GAF_Clicks, GAF_Impressions, GAF_Cost ,GAF_Conversions ,GAF_CostPerConversion, GAF_ValuePerConversion)
                                    VALUES (@GAS_GAC_Id, @GAF_Data, @GAF_GAG_Id, @GAF_PRO_Id, @GAF_Clicks, @GAF_Impressions, @GAF_Cost, @GAF_Conversions, @GAF_CostPerConversion, @GAF_ValuePerConversion)";

                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.Add("@GAF_GAS_Id", System.Data.SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_GAS_Id", System.Data.SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_PRO_Id", System.Data.SqlDbType.Int);
                    cmd.Parameters.Add("@GAF_Clicks", System.Data.SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_Impressions", System.Data.SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAF_Cost", System.Data.SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_Conversions", System.Data.SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_CostPerConversion", System.Data.SqlDbType.Money);
                    cmd.Parameters.Add("@GAF_ValuePerConversion", System.Data.SqlDbType.Money);

                    cmd.Prepare();

                    foreach (var item in performances)
                    {
                        cmd.Parameters["@GAF_Data"].Value = item.Date == null ? DateTime.Now : DateTime.ParseExact(item.Date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        cmd.Parameters["@GAF_GAC_Id"].Value = item.CampaignId;
                        cmd.Parameters["@GAF_GAG_Id"].Value = item.AdGroupId;
                        cmd.Parameters["@GAF_PRO_Id"].Value = Convert.ToInt32(item.OfferId);
                        cmd.Parameters["@GAF_Clicks"].Value = item.Clicks;
                        cmd.Parameters["@GAF_Impressions"].Value = item.Impressions;
                        cmd.Parameters["@GAF_Cost"].Value = item.Cost / 1000000;
                        cmd.Parameters["@GAF_Conversions"].Value = item.Conversions / 100;
                        cmd.Parameters["@GAF_CostPerConversion"].Value = item.CostPerConversion / 1000000;
                        cmd.Parameters["@GAF_ValuePerConversion"].Value = item.ValuePerConversion / 100;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exibicao temporaria. Registrar em log. Erro: {0}", e.Message);
            }
        }

        private static async Task<List<PerformanceResponse>> GetPerformanceLastWeek(IEnumerable<long> campanhaIds)
        {
            var result = new List<PerformanceResponse>();

            try
            {
                var srv = new ServicoGoogle();

                foreach (var campanhaId in campanhaIds)
                {
                    await srv.ExecutaAcao<List<PerformanceResponse>>(String.Format("Relatorio/ShoppingLastWeek/{0}", campanhaId));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao tentar obter performance de campanhas. Erro: {0}", ex.Message);
            }

            return result;
        }

        public static void AtualizarCampanhas()
        {
            ActionAtualizarCampanhas(null);
        }

        public static void PersistirCampanha(long campanhaId)
        {
            ActionAtualizarCampanhas(campanhaId.ToString());
        }

        public static async void ActionAtualizarCampanhas(string message)
        {
            var srv = new ServicoGoogle();

            List<CampanhaResponse> campanhas;

            try
            {
                string chamada = String.Empty;

                if (String.IsNullOrEmpty(message))
                    chamada = "Campanha";
                else
                    chamada = "Campanha/" + message;

                campanhas = await srv.ExecutaAcao<List<CampanhaResponse>>(chamada);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao atualizar campanhas: {0}",ex.Message);
                return;
            }

            SqlCommand cmd = null;
            try
            {
                using (var conn = new SqlConnection(@"Data Source = 10.249.48.19; Initial Catalog = dbInternet; Persist Security Info = True; User ID = inetanonimo; Password = postergar; Max Pool Size = 1000"))
                {
                    conn.Open();

                    string sql = @" BEGIN
	                                    INSERT INTO GoogleAdwordsCampanha (GAC_Id, GAC_Descricao, GAC_DtInicio, GAC_DtFim, GAC_VlrOrcamento, GAC_Status)
	                                    SELECT	@GAC_Id, @GAC_Descricao, @GAC_DtInicio, @GAC_DtFim, @GAC_VlrOrcamento, @GAC_Status
	                                    WHERE	@GAC_Id NOT IN (SELECT GAC_Id FROM GoogleAdwordsCampanha)

	                                    IF @@ROWCOUNT = 0
	                                    BEGIN
		                                    UPDATE	GoogleAdwordsCampanha
		                                    SET		GAC_Descricao = @GAC_Descricao,
				                                    GAC_DtInicio = @GAC_DtInicio,
				                                    GAC_DtFim = @GAC_DtFim,
				                                    GAC_VlrOrcamento = @GAC_VlrOrcamento,
				                                    GAC_Status = @GAC_Status
		                                    WHERE	GAC_Id = @GAC_Id
                                                AND (   GAC_Descricao != @GAC_Descricao
                                                        OR GAC_DtInicio != @GAC_DtInicio
				                                        OR GAC_DtFim != @GAC_DtFim
				                                        OR GAC_VlrOrcamento != @GAC_VlrOrcamento				                                        
				                                        OR GAC_Status != @GAC_Status  
                                                    )
	                                    END
                                    END";

                    cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.Add("@GAC_Id", System.Data.SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAC_Descricao", System.Data.SqlDbType.VarChar, 255);
                    cmd.Parameters.Add("@GAC_DtInicio", System.Data.SqlDbType.SmallDateTime);
                    cmd.Parameters.Add("@GAC_DtFim", System.Data.SqlDbType.SmallDateTime);
                    cmd.Parameters.Add("@GAC_VlrOrcamento", System.Data.SqlDbType.Money);
                    cmd.Parameters.Add("@GAC_Status", System.Data.SqlDbType.VarChar, 30);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Prepare();

                    foreach (var campanha in campanhas)
                    {
                        cmd.Parameters["@GAC_Id"].Value = campanha.Id;
                        cmd.Parameters["@GAC_Descricao"].Value = campanha.Nome;
                        cmd.Parameters["@GAC_DtInicio"].Value = DateTime.ParseExact(campanha.DataInicio, "yyyyMMdd", CultureInfo.InvariantCulture);
                        cmd.Parameters["@GAC_DtFim"].Value = DateTime.ParseExact(campanha.DataFim, "yyyyMMdd", CultureInfo.InvariantCulture);
                        cmd.Parameters["@GAC_VlrOrcamento"].Value = Convert.ToDecimal(campanha.Orcamento) / Convert.ToDecimal(1000000);
                        cmd.Parameters["@GAC_Status"].Value = campanha.Status;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro incluir/alterar campanhas: {0}", ex.Message);
                throw;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

        }

        public static async void AtualizarGrupos(long CampanhaId, List<GrupoResponse> grupos)
        {
            var srv = new ServicoGoogle();

            grupos = await GetGrupos(CampanhaId);

            SqlCommand cmd = null;
            try
            {
                using (var conn = new SqlConnection(@"Data Source = 10.249.48.19; Initial Catalog = dbInternet; Persist Security Info = True; User ID = inetanonimo; Password = postergar; Max Pool Size = 1000"))
                {
                    conn.Open();

                    string sql = @" BEGIN
	                                    INSERT INTO GoogleAdwordsGrupo (GAG_Id, GAG_GAC_Id, GAG_Descricao, GAG_VlBidMaxPadrao, GAG_Status)
	                                    SELECT	@GAG_Id, @GAG_GAC_Id, @GAG_Descricao, @GAG_VlBidMaxPadrao, @GAG_Status
	                                    WHERE	@GAG_Id NOT IN (SELECT GAG_Id FROM GoogleAdwordsGrupo)

	                                    IF @@ROWCOUNT = 0
	                                    BEGIN
		                                    UPDATE	GoogleAdwordsGrupo
		                                    SET		GAG_GAC_Id = @GAG_GAC_Id,
				                                    GAG_Descricao = @GAG_Descricao,
				                                    GAG_VlBidMaxPadrao = @GAG_VlBidMaxPadrao,
				                                    GAG_Status = @GAG_Status
		                                    WHERE	GAG_Id = @GAG_Id
                                            AND (       GAG_Descricao != @GAG_Descricao
                                                    OR  GAG_GAC_Id != @GAG_GAC_Id        
                                                    OR  GAG_VlBidMaxPadrao != @GAG_VlBidMaxPadrao
                                                    OR  GAG_Status != @GAG_Status
                                                )
	                                    END
                                    END";

                    cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.Add("@GAG_Id", System.Data.SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAG_GAC_Id", System.Data.SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAG_Descricao", System.Data.SqlDbType.VarChar, 255);
                    cmd.Parameters.Add("@GAG_VlBidMaxPadrao", System.Data.SqlDbType.Money);
                    cmd.Parameters.Add("@GAG_Status", System.Data.SqlDbType.VarChar, 30);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.Prepare();

                    foreach (var grupo in grupos)
                    {
                        cmd.Parameters["@GAG_Id"].Value = grupo.Id;
                        cmd.Parameters["@GAG_GAC_Id"].Value = grupo.CampanhaId;
                        cmd.Parameters["@GAG_Descricao"].Value = grupo.Nome;
                        cmd.Parameters["@GAG_VlBidMaxPadrao"].Value = Convert.ToDecimal(grupo.CpcBid) / Convert.ToDecimal(1000000);
                        cmd.Parameters["@GAG_Status"].Value = grupo.Status;
                        
                        cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro incluir/alterar grupo: {0}", ex.Message);
                throw;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

        }

        public static async void BaixarCampanhasAutomaticas()
        {
            var srv = new ServicoGoogle();

            List<CampanhaResponse> campanhas;

            campanhas = await GetCampanhasAutomaticas();

            foreach (var campanha in campanhas)
               PersistirCampanha(campanha.Id);
        }

        public static async Task<List<GrupoResponse>> GetGrupos(long CampanhaId)
        {
            var srv = new ServicoGoogle();

            List<GrupoResponse> grupos = null;

            try
            {
                grupos = await srv.ExecutaAcao<List<GrupoResponse>>(string.Format("Grupo/Campanha/{0}", CampanhaId));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao atualizar grupo: {0}", ex.Message);
            }

            return grupos;
        }

        public static async Task<List<CampanhaResponse>> GetCampanhas()
        {
            var srv = new ServicoGoogle();

            List<CampanhaResponse> campanhas = null;

            try
            {
                campanhas = await srv.ExecutaAcao<List<CampanhaResponse>>("Campanha");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao obter dados de campanha: {0}", ex.Message);
            }

            return campanhas;
        }

        private static void AtualizarProduto(long grupoId, List<CategoriaProdutoResponse> produtos)
        {
            SqlCommand cmd = null;
            try
            {
                using (var conn = new SqlConnection(@"Data Source = 10.249.48.19; Initial Catalog = dbInternet; Persist Security Info = True; User ID = inetanonimo; Password = postergar; Max Pool Size = 1000"))
                {
                    conn.Open();

                    cmd.CommandText = @"DELETE GoogleAdwordsProduto 
                                        WHERE GAP_GAG_Id = @GrupoId AND GAP_Id NOT IN (SELECT GoogleId FROM @SkuList)

                                        INSERT INTO GoogleAdwordsProduto (GAP_Id, GAP_GAG_Id, GAP_GAT_Id, GAP_Pro_id, GAP_VlCpc, GAP_StsAtivo, GAP_DtCadastro)
                                        SELECT	GoogleId, GrupoId, CategoriaId, Sku, CpcBid, GAP_StsAtivo, getdate()
                                        FROM	@SkuList 
                                        WHERE GoogleId NOT IN (SELECT GAP_Id FROM GoogleAdwordsProduto)

                                        UPDATE T
                                        SET GAP_GAG_Id = GrupoId,
	                                        GAP_GAT_Id = CategoriaId,
	                                        GAP_PRO_Id = Sku,
	                                        GAP_VlCpc = CpcBid,
	                                        GAP_DtUltAlteracao = getdate()
                                        FROM GoogleAdwordsProduto T JOIN @SkuList ON GAP_Id = GoogleId";

                    cmd.CommandTimeout = int.MaxValue;

                    var paramTable = cmd.Parameters.AddWithValue("@SkuList", CreateSkuList(produtos));
                    paramTable.SqlDbType = SqlDbType.Structured;
                    paramTable.TypeName = "dbo.SkuList";

                    cmd.ExecuteNonQuery();
                }

                Console.WriteLine("Produtos atualizadas com sucesso.");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro incluir/alterar grupo: {0}", ex.Message);
                return;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        private static void AtualizarCategoria(long grupoId, List<CategoriaProdutoResponse> categorias)
        {
            SqlCommand cmd = null;
            try
            {
                using (var conn = new SqlConnection(@"Data Source = 10.249.48.19; Initial Catalog = dbInternet; Persist Security Info = True; User ID = inetanonimo; Password = postergar; Max Pool Size = 1000"))
                {
                    conn.Open();

                    cmd = new SqlCommand(string.Format(" DELETE FROM GoogleAdwordsCategoria WHERE GAT_GAG_Id = {0}", grupoId), conn);

                    cmd.CommandTimeout = int.MaxValue;

                    cmd.ExecuteNonQuery();

                    cmd.CommandText = @"INSERT INTO GoogleAdwordsCategoria (GAT_Id, GAT_GAG_Id, GAT_GAT_Id, GAT_Descricao, GAT_Type, GAT_VlCpc)
	                                    VALUES (@GAT_Id, @GAT_GAG_Id, @GAT_GAT_Id, @GAT_Descricao, @GAT_Type, @GAT_VlCpc)";

                    cmd.Parameters.Add("@GAT_Id", System.Data.SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAT_GAG_Id", System.Data.SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAT_GAT_Id", System.Data.SqlDbType.BigInt);
                    cmd.Parameters.Add("@GAT_Descricao", System.Data.SqlDbType.VarChar, 255);
                    cmd.Parameters.Add("@GAT_Type", System.Data.SqlDbType.VarChar, 255);
                    cmd.Parameters.Add("@GAT_VlCpc", System.Data.SqlDbType.Money);

                    cmd.Prepare();

                    foreach (var categoria in categorias)
                    {
                        cmd.Parameters["@GAT_Id"].Value = categoria.GoogleId;
                        cmd.Parameters["@GAT_GAG_Id"].Value = categoria.GrupoId;
                        cmd.Parameters["@GAT_GAT_Id"].Value = categoria.ParentGoogleId;
                        cmd.Parameters["@GAT_Descricao"].Value = categoria.Categoria ?? "Desconhecido";
                        cmd.Parameters["@GAT_Type"].Value = categoria.Type;
                        cmd.Parameters["@GAT_VlCpc"].Value = Convert.ToDecimal(categoria.Cpc) / Convert.ToDecimal(1000000);

                        cmd.ExecuteNonQuery();
                    }
                }

                Console.WriteLine("Categorias atualizadas com sucesso.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro incluir/alterar grupo: {0}", ex.Message);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static async void ActionAtualizarProdutoCategorias(string message)
        {
            long idGrupo;

            long.TryParse(message, out idGrupo);

            var srv = new ServicoGoogle();

            List<CategoriaProdutoResponse> produtosCategorias;

            try
            {
                produtosCategorias = await srv.ExecutaAcao<List<CategoriaProdutoResponse>>(String.Format("Produto/Grupo/{0}", idGrupo));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erro ao atualizar grupo: {0}", ex.Message);
                return;
            }

            if (produtosCategorias == null)
            {
                Console.WriteLine("Nao houve retorno de categorias ou produtos");
                return;
            }

            AtualizarCategoria(idGrupo, produtosCategorias.Where(x => x.IsCategoria).ToList());

            AtualizarProduto(idGrupo, produtosCategorias.Where(x => !x.IsCategoria).ToList());

        }

        public static async void BaixarProdutoCategorias(long campanhaId)
        {
            var srv = new ServicoGoogle();

            List<GrupoResponse> grupos = await GetGrupos(campanhaId);

                if (grupos == null)
                    return;

            AtualizarGrupos(campanhaId, grupos);

            List<CategoriaProdutoResponse> produtosCategorias;

            foreach (var grupo in grupos)
            {
                try
                {
                    if (grupo.Status != "ENABLED")
                        continue;

                    produtosCategorias = await srv.ExecutaAcao<List<CategoriaProdutoResponse>>(String.Format("Produto/Grupo/{0}", grupo.Id));

                    if (produtosCategorias == null)
                        continue;

                    AtualizarCategoria(grupo.Id, produtosCategorias.Where(x => x.IsCategoria).ToList());

                    AtualizarProduto(grupo.Id, produtosCategorias.Where(x => !x.IsCategoria).ToList());

                    Console.WriteLine("Atualizados {0} itens do grupo {1} - {2}.", produtosCategorias.Count, grupo.Id, grupo.Nome);

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Erro ao atualizar grupo {0} - {1}: {2}", grupo.Id, grupo.Nome, ex.Message);
                }
            }
        }

        public static void ObterMaisVendidos()
        {
            EnviarParaFila("Clearing", new String[] { "123456", "782495" });
        }

        public static void SendMessage(MessageQueue messenge)
        {
            var srv = new ServicoFila();

            try
            {
                EnviaParaFila<MessageQueue>(messenge);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Erro ao tentar inserir na fila. Acao {0} Mensagem {1} Erro {2}", messenge.Acao, messenge.Mensagem, e.Message));
            }
        }

        public static void EnviarParaFila(string action, object messenge)
        {
            var msg = JsonConvert.SerializeObject(messenge);
            var request = new MessageQueue { Acao = action, Mensagem = msg };

            var srv = new ServicoFila();

            try
            {
                EnviaParaFila<MessageQueue>(request);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Erro ao tentar inserir na fila. Acao {0} Mensagem {1} Erro {2}", action, msg, e.Message ));
            }
        }

        public static void SendMessage(string action, string messenge)
        {
            var request = new MessageQueue { Acao = action, Mensagem = messenge };

            var srv = new ServicoFila();

            try
            {
                EnviaParaFila<MessageQueue>(request);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Erro ao tentar inserir na fila. Acao {0} Mensagem {1} Erro {2}", action, messenge, e.Message));
            }
        }

        public static async void ProcessarFila()
        {
            var messages = await RecebeDaFila<MessageQueue>();

            foreach (var msg in messages)
            {
                if (_actions.ContainsKey(msg.Acao))
                    _actions[msg.Acao](msg.Mensagem);
                else
                    SendMessage(msg);
            }
        }

        public static async void EnviaParaFila<T>(T request)
        {
            var srv = new ServicoFila();

            if (await srv.InserirFilaSQS<T>(request))
                Console.WriteLine("Inserido com sucesso em fila");
            else
                Console.WriteLine("Fila não adicionado");
        }

        public static async Task<List<T>> RecebeDaFila<T>() where T : new()
        {
            var srv = new ServicoFila();

            var posts = await srv.ProcessarFilaSQS();

            var result = new List<T>();

            foreach (var post in posts)
            {
                try
                {
                    result.Add(JsonConvert.DeserializeObject<T>(post));
                }
                catch
                {
                    EnviarErroFila(post);
                }
                
            }

            return result;
        }

        public static void EnviarErroFila(string mensagem)
        {
            SendMessage("ERRO", mensagem);
        }

    }


    public class ClearingRequest
    {
        public List<string> Skus { get; set; }
    }

    public class CPABidMaxRequest
    {
        public long GrupoId { get; set; }
        public Dictionary<long, string> Skus { get; set; }
    }

    public class CPABidMaxResponse
    {
        public bool Flerro { get; set; }
        public string MsgErro { get; set; }
        public Dictionary<long, string> Skus { get; set; }
    }

    public class InsertUpdateRequest
    {
        public int Sku { get; set; }
        public long CampanhaId { get; set; }
        public long GrupoId { get; set; }
        public long CategoriaId { get; set; }
        public long GoogleId { get; set; }
    }

    public class InsertRequest
    {
        public string OfferId { get; set; }
        public long AdGroupId { get; set; }
        public long ParentId { get; set; }
        public long CpcBid { get; set; }
    }

    public class InsertUpdateDTO
    {
        public int Sku { get; set; }
        public long CampanhaId { get; set; }
        public long GrupoId { get; set; }
        public long CategoriaId { get; set; }
        public long GoogleId { get; set; }
    }

    public class ProductResponse
    {
        public long Id { get; set; }
        public long Cpa { get; set; }
        public long Cpc { get; set; }
        public long CpcMax { get; set; }
    }

    public class CampanhaResponse
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public long Orcamento { get; set; }
        public string DataInicio { get; set; }
        public string DataFim { get; set; }
        public string Status { get; set; }
    }

    public class GrupoResponse
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public long CampanhaId { get; set; }
        public long CpcBid { get; set; }
        public string Status { get; set; }
    }

    public class PerformanceResponse
    {
        public string OfferId { get; set; }
        public string Date { get; set; }
        public long CampaignId { get; set; }
        public long AdGroupId { get; set; }
        public long Clicks { get; set; }
        public long Impressions { get; set; }
        public decimal Cost { get; set; }
        public double Conversions { get; set; }
        public decimal CostPerConversion { get; set; }
        public double ValuePerConversion { get; set; }
        public double ValuePerAllConversion { get; set; }
    }

    public class CategoriaProdutoResponse
    {
        public long GoogleId { get; set; }
        public long GrupoId { get; set; }
        public string Sku { get; set; }
        public long Cpc { get; set; }
        public long NewCpc { get; set; }
        public decimal Cpa { get; set; }
        public decimal Custo { get; set; }
        public double Conversoes { get; set; }
        public long Clicks { get; set; }
        public string Type { get; set; }
        public bool IsMarca { get; set; }
        public bool IsCategoria { get; set; }
        public long ParentGoogleId { get; set; }
        public string Categoria { get; set; }
        public bool IsExcluded { get; set; }
        public string Status { get; set; }
    }


    public class ServicoFila
    {
        private Configs configs = new Configs();

        public ServicoFila()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");
            var configuration = builder.Build();

            configuration.GetSection("Configs").Bind(configs);
            configuration.GetSection("Configs:GoogleAdWords").Bind(configs.HostNames);
        }

        public async Task<bool> InserirFilaSQS<T>(T obj)
        {
            AmazonSQSConfig sqsConfig = new AmazonSQSConfig();
            sqsConfig.ProxyHost = configs.ProxyHost;
            sqsConfig.ProxyPort = configs.ProxyPort;
            sqsConfig.RegionEndpoint = configs.HostNames.RegionEndpoint;

            var client = new AmazonSQSClient(
              awsAccessKeyId: configs.awsAccessKeyId,
              awsSecretAccessKey: configs.awsSecretAccessKey,
              clientConfig: sqsConfig);

            var request = new SendMessageRequest
            {
                MessageBody = JsonConvert.SerializeObject(obj),
                QueueUrl = configs.HostNames.awsSQSHost
            };

            var response = await client.SendMessageAsync(request);
            return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }

        public async Task<List<String>> ProcessarFilaSQS()
        {
            var posts = new List<String>();

            AmazonSQSConfig sqsConfig = new AmazonSQSConfig();
            sqsConfig.ProxyHost = configs.ProxyHost;
            sqsConfig.ProxyPort = configs.ProxyPort;
            sqsConfig.RegionEndpoint = configs.HostNames.RegionEndpoint;

            var client = new AmazonSQSClient(
              awsAccessKeyId: configs.awsAccessKeyId,
              awsSecretAccessKey: configs.awsSecretAccessKey,
              clientConfig: sqsConfig);

            var response = await client.ReceiveMessageAsync(new ReceiveMessageRequest()
            {
                QueueUrl = configs.HostNames.awsSQSHost,
                MaxNumberOfMessages = configs.HostNames.MaxNumberOfMessages
            });
            while (response.Messages.Count > 0)
            {
                foreach (var mensagem in response.Messages)
                {
                    try
                    {
                        posts.Add(mensagem.Body);

                        await client.DeleteMessageAsync(new DeleteMessageRequest()
                        {
                            QueueUrl = configs.HostNames.awsSQSHost,
                            ReceiptHandle = mensagem.ReceiptHandle
                        });

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return posts;
                    }
                }

                response = await client.ReceiveMessageAsync(new ReceiveMessageRequest()
                {
                    QueueUrl = configs.HostNames.awsSQSHost,
                    MaxNumberOfMessages = configs.HostNames.MaxNumberOfMessages
                });
            }

            return posts;
        }

    }

    public class ServicoGoogle
    {
        private Configs configs = new Configs();
        private HttpClient client = new HttpClient();

        public ServicoGoogle()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");
            var configuration = builder.Build();

            configuration.GetSection("Configs").Bind(configs);
            configuration.GetSection("Configs:GoogleAdWords").Bind(configs.HostNames);

            client.BaseAddress = new Uri(configs.HostNames.ClientHost);

        }

        public async Task<T> ExecutaAcao<T>(string action, object request)
        {
            var requestSerializado = JsonConvert.SerializeObject(request);

            StringContent content = new StringContent(requestSerializado);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = await client.PostAsync(action, content);

            if (response.IsSuccessStatusCode)
            {
                var obj = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<T>(obj);
            }
            else
            {
                throw new Exception(String.Format("Erro detectado. Codigo: {0} Mensagem: {1}", response.StatusCode, response.RequestMessage));
            }
        }

        public async Task<T> ExecutaAcao<T>(string action)
        {
            HttpResponseMessage response = await client.GetAsync(action);

            if (response.IsSuccessStatusCode)
            {
                var obj = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<T>(obj);
            }
            else
            {
                throw new Exception(String.Format("Erro detectado. Codigo: {0} Mensagem: {1}", response.StatusCode, response.RequestMessage));
            }
        }

        public async Task<T> ExecutaAcao<T>(string action, string messenge)
        {
            StringContent content = new StringContent(messenge);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = await client.PostAsync(action, content);

            if (response.IsSuccessStatusCode)
            {
                var obj = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<T>(obj);
            }
            else
            {
                throw new Exception(String.Format("Erro detectado. Codigo: {0} Mensagem: {1}", response.StatusCode, response.RequestMessage));
            }
        }


    }


}
