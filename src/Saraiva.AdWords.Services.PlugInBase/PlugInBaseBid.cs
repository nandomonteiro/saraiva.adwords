﻿using Saraiva.AdWords.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace Saraiva.AdWords.Services.Plugins
{
    public abstract class PlugInBaseBid : PlugInBase, IPlugInService
    {
        protected string Acao { get; set; }
        protected string Mensagem { get; set; }

        public class StatusTransaction
        {
            public const string EM_EXECUCAO = "EE";
            public const string CONCLUIDO_COM_SUCESSO = "CS";
            public const string CONCLUIDO_COM_ERROS = "CE";
            public const string CONCLUIDO_SEM_ATIVIDADE = "CA";
            public const string CONCLUIDO_COM_RESSALVAS = "CR";
            public const string FINALIZACAO_FORCADA = "FF";
            public const string AGUARDANDO_EXECUCAO = "AE";
            public const string MENSAGEM_FORA_PADRAO = "MP";
        }

        public class StatusTransactionProgress
        {
            public const string EmProcesso = "P";
            public const string FinalizadoComSucesso = "S";
            public const string FinalizadoSemSucesso = "N";
        };

        protected int IdTransaction { get; set; }

        protected IAPIService Service { get; set; }
        protected IJsonService Json { get; set; }

        protected const string DBINTERNET = @"Data Source = 10.249.48.19; Initial Catalog = dbInternet; Persist Security Info = True; User ID = inetanonimo; Password = postergar; Max Pool Size = 1000";

        private Dictionary<string, Action<string>> _actions = new Dictionary<string, Action<string>>();

        public PlugInBaseBid(string acao, string mensagem)
        {
            Acao = acao;
            Mensagem = mensagem;
        }

        public PlugInBaseBid(string acao, string mensagem, IAPIService service, IJsonService json) : this(acao, mensagem)
        {
            Service = service;
            Json = json;
        }

        public PlugInBaseBid(string acao, string mensagem, IAPIService service, IJsonService json, int idtransaction) : this(acao, mensagem, service, json)
        {
            IdTransaction = idtransaction;
        }

        protected Dictionary<string, Action<string>> Acoes { get { return _actions; } }

        public override void ExecutarServico()
        {
            if (!Acoes.ContainsKey(Acao))
            { 
                Console.WriteLine("Acao [{0}] nao encontrada para o plugin [{1}]", Acao, this.GetType().Name);
                return;
            }

            try
            { 
                IdTransaction = InsertTransaction(Acao, Mensagem);

                if (IdTransaction == 0)
                { 
                    Console.WriteLine("Nao foi possivel concluir operacao. Verifique se ja existe processo em andamento.");
                    return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Erro ao registrar nova operacao: {0} - {1}", Acao, e.Message);
            }

            Acoes[Acao](Mensagem);
        }

        protected void UpdateTransactionError(string msgErro)
        {
            UpdateTransaction(true, msgErro, StatusTransaction.CONCLUIDO_COM_ERROS);
        }

        protected void UpdateTransactionStatus(string msg)
        {
            UpdateTransaction(false, msg, StatusTransaction.EM_EXECUCAO);
        }

        protected void UpdateTransaction(bool stsErro, string msgErro, string status)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(DBINTERNET))
                {
                    string sql = @" UPDATE  GoogleAdwordsTransaction 
                                    SET		GAN_DtFim   = GETDATE(),
		                                    GAN_StsErro = @StsErro,
		                                    GAN_MsgErro = @MsgErro,
                                            GAN_Status  = @Status
                                    WHERE	GAN_Id      = @idTransacao";
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("@idTransacao", IdTransaction);
                    cmd.Parameters.AddWithValue("@StsErro", stsErro);
                    cmd.Parameters.AddWithValue("@MsgErro", msgErro);
                    cmd.Parameters.AddWithValue("@Status", status);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("UpdateTransaction: {0}", e.Message);
            }
        }

        private int InsertTransaction(string acao, string mensagem)
        {
            if (IdTransaction != 0)
                return IdTransaction;

            try
            {
                using (SqlConnection conn = new SqlConnection(DBINTERNET))
                {
                    string sql = @"INSERT INTO GoogleAdwordsTransaction (GAN_Acao, GAN_DtInicio, GAN_Response) VALUES (@acao, GETDATE(), @mensagem)
                                   SELECT ISNULL(CAST(scope_identity() AS int),0)";
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("@acao", acao);
                    cmd.Parameters.AddWithValue("@mensagem", mensagem);

                    return (int)cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("InsertTransaction: {0}", e.Message);
                return 0;
            }
        }

        protected bool ExistsStatus(string acao, string status)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(DBINTERNET))
                {
                    string sql = @" SELECT	COUNT(1)
	                                FROM	GoogleAdwordsTransaction 
	                                WHERE	GAN_Acao IN (@acao, REPLACE(@acao, '_',''))
	                                    AND GAN_Status = @status";
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("@acao", acao);
                    cmd.Parameters.AddWithValue("@status", status);

                    return ((int)cmd.ExecuteScalar() > 0);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("InsertTransaction: {0}", e.Message);
                return false;
            }
        }

    }
}
