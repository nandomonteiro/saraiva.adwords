﻿using Saraiva.AdWords.Services.Interfaces;
using System;

namespace Saraiva.AdWords.Services.Plugins
{
    public abstract class PlugInBase : IPlugInService
    {
        public abstract void ExecutarServico();

    }
}
