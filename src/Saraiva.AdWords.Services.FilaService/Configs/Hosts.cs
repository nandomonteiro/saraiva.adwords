﻿using System;

namespace Saraiva.AdWords.Services.FilaService
{
    public class Hosts
    {
        public string SQSHost { get; set; }

        public string Region { get; set; }

        public Amazon.RegionEndpoint RegionEndpoint
        {
            get
            {
                if (string.IsNullOrEmpty(Region))
                    throw new Exception("awsRegion não encontrado no arquivo de configuração.");
                else
                    return Amazon.RegionEndpoint.GetBySystemName(Region);
            }
        }

        public int MaxNumberOfMessages { get; set; }

    }
}
