﻿using Amazon.SQS;
using Amazon.SQS.Model;
using Saraiva.AdWords.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Services.FilaService
{
    public class FilaService : IFilaService
    {
        private Configs configs = new Configs();

        public bool InserirFilaSQS(string mensagem)
        {
            AmazonSQSConfig sqsConfig = new AmazonSQSConfig();
            sqsConfig.ProxyHost = configs.ProxyHost;
            sqsConfig.ProxyPort = configs.ProxyPort;
            sqsConfig.RegionEndpoint = configs.HostNames.RegionEndpoint;

            var client = new AmazonSQSClient(
              awsAccessKeyId: configs.AccessKeyId,
              awsSecretAccessKey: configs.SecretAccessKey,
              clientConfig: sqsConfig);

            var request = new SendMessageRequest
            {
                MessageBody = mensagem,
                QueueUrl = configs.HostNames.SQSHost
            };

            var response = client.SendMessage(request);
            return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }

        public List<string> ProcessarFilaSQS()
        {
            var posts = new List<string>();

            AmazonSQSConfig sqsConfig = new AmazonSQSConfig();
            sqsConfig.ProxyHost = configs.ProxyHost;
            sqsConfig.ProxyPort = configs.ProxyPort;
            sqsConfig.RegionEndpoint = configs.HostNames.RegionEndpoint;

            var client = new AmazonSQSClient(
              awsAccessKeyId: configs.AccessKeyId,
              awsSecretAccessKey: configs.SecretAccessKey,
              clientConfig: sqsConfig);

            try
            {
                //var response = await client.ReceiveMessageAsync(new ReceiveMessageRequest()
                
                var messageRequest = new ReceiveMessageRequest()
                {
                    QueueUrl = configs.HostNames.SQSHost,
                    MaxNumberOfMessages = configs.HostNames.MaxNumberOfMessages
                };

                var response = client.ReceiveMessage(messageRequest);

                while (response.Messages.Count > 0)
                {
                    foreach (var mensagem in response.Messages)
                    {
                        try
                        {
                            posts.Add(mensagem.Body);

                            client.DeleteMessage(new DeleteMessageRequest()
                            {
                                QueueUrl = configs.HostNames.SQSHost,
                                ReceiptHandle = mensagem.ReceiptHandle
                            });

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            return posts;
                        }
                    }

                    response = client.ReceiveMessage(new ReceiveMessageRequest()
                    {
                        QueueUrl = configs.HostNames.SQSHost,
                        MaxNumberOfMessages = configs.HostNames.MaxNumberOfMessages
                    });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return posts;
        }

    }
}
