﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saraiva.AdWords.Host.Services.Dtos
{
    public class InsertUpdateDTO
    {
        public int Sku { get; set; }
        public long CampanhaId { get; set; }
        public long GrupoId { get; set; }
        public long CategoriaId { get; set; }
        public long GoogleId { get; set; }
    }
}
